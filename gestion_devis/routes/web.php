<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', function () {
    return view('home');
});

Auth::routes();

Route::get('/logout', [App\Http\Controllers\HomeController::class, 'logout'])->name('logout'); 





Route::prefix('admin')->group(function () {
    Route::middleware('can:accessAdminpanel')->group(function (){
Route::view('chat','admin.users.messages')->name('admin.chat');


Route::get('/Profile', [App\Http\Controllers\Admin\AdminController::class, 'profile'])->name('admin.profile');
Route::post('change-password', [App\Http\Controllers\Admin\AdminController::class, 'changePassword'])->name('change.password');

Route::get('/dashboard', [App\Http\Controllers\Admin\AdminController::class, 'index'])->name('admin.dashboard');
Route::get('/recherche/devis', [App\Http\Controllers\Admin\DevisController::class, 'autocompleteSearch'])->name('admin.devis.search');
//devis
Route::get('/devis', [App\Http\Controllers\Admin\DevisController::class, 'index'])->name('admin.devis');
Route::get('/devis/pdf/{id}', [App\Http\Controllers\Admin\DevisController::class, 'pdf'])->name('admin.devis.pdf');
Route::get('/devis/apercu/{id}', [App\Http\Controllers\Admin\DevisController::class, 'apercu'])->name('admin.devis.apercu');
Route::get('/devis/ajout', [App\Http\Controllers\Admin\DevisController::class, 'create'])->name('creer.devis');
Route::post('/devis/store', [App\Http\Controllers\Admin\DevisController::class, 'store'])->name('devis.store');
Route::post('/devis/update/{id}', [App\Http\Controllers\Admin\DevisController::class, 'update'])->name('devis.update');
Route::get('/devis/edit/{id}', [App\Http\Controllers\Admin\DevisController::class, 'edit'])->name('devis.edit');
Route::get('/devis/delete/{id}', [App\Http\Controllers\Admin\DevisController::class, 'destroy'])->name('devis.delete');

// clients
Route::get('/clients', [App\Http\Controllers\Admin\ClientController::class, 'index'])->name('admin.clients');
Route::get('/clients/search/clients', [App\Http\Controllers\Admin\ClientController::class, 'Searchclient'])->name('admin.clients.search');
Route::get('/clients/ajout', [App\Http\Controllers\Admin\ClientController::class, 'create'])->name('clients.ajout');
Route::post('/clients/store', [App\Http\Controllers\Admin\ClientController::class, 'store'])->name('clients.store');
Route::post('/clients/update/{id}', [App\Http\Controllers\Admin\ClientController::class, 'update'])->name('clients.update');
Route::get('/clients/edit/{id}', [App\Http\Controllers\Admin\ClientController::class, 'edit'])->name('clients.edit');
Route::get('/clients/delete/{id}', [App\Http\Controllers\Admin\ClientController::class, 'destroy'])->name('clients.delete');

// employee
Route::get('/employee', [App\Http\Controllers\Admin\EmployeeController::class, 'index'])->name('admin.employee');
Route::get('/Employee/search/', [App\Http\Controllers\Admin\EmployeeController::class, 'employeeSearch'])->name('admin.employee.search');
Route::get('/employee/ajout', [App\Http\Controllers\Admin\EmployeeController::class, 'create'])->name('employee.ajouter');
Route::post('/employee/store', [App\Http\Controllers\Admin\EmployeeController::class, 'store'])->name('employee.store');
Route::post('/employee/update/{id}', [App\Http\Controllers\Admin\EmployeeController::class, 'update'])->name('employee.update');
Route::get('/employee/edit/{id}', [App\Http\Controllers\Admin\EmployeeController::class, 'edit'])->name('employee.edit');
Route::get('/employee/delete/{id}', [App\Http\Controllers\Admin\EmployeeController::class, 'destroy'])->name('employee.delete');

// biblio

Route::get('/bibliotheque/', [App\Http\Controllers\Admin\biblioController::class, 'index'])->name('admin.biblio');
Route::get('/bibliotheque/search/bibliotheque', [App\Http\Controllers\Admin\biblioController::class, 'biblioSearch'])->name('admin.biblio.search');
Route::get('/bibliotheque/ajout/{id}', [App\Http\Controllers\Admin\biblioController::class, 'edit'])->name('admin.biblio.ajouter');
Route::post('/bibliotheque/store/{id}', [App\Http\Controllers\Admin\biblioController::class, 'update'])->name('admin.biblio.store');


});
});



Route::prefix('employee')->group(function () {
    Route::middleware('can:accessProfile')->group(function (){
        Route::view('chat','employee.users.messages')->name('user.chat');


        Route::get('/Profile', [App\Http\Controllers\Employee\EmployeeController::class, 'profile'])->name('employee.profile');
Route::post('change-password', [App\Http\Controllers\Employee\EmployeeController::class, 'changePassword'])->name('employee.change.password');
        Route::get('/dashboard', [App\Http\Controllers\Employee\EmployeeController::class, 'index'])->name('employee.dashboard');

Route::get('/recherche/devis', [App\Http\Controllers\Employee\DevisController::class, 'autocompleteSearch'])->name('employee.devis.search');
Route::get('/devis/pdf/{id}', [App\Http\Controllers\Employee\DevisController::class, 'pdf'])->name('employee.devis.pdf');
Route::get('/devis/apercu/{id}', [App\Http\Controllers\Employee\DevisController::class, 'apercu'])->name('employee.devis.apercu');
        //devis
        Route::get('/devis', [App\Http\Controllers\Employee\DevisController::class, 'index'])->name('employee.devis');
        Route::get('/devis/ajout', [App\Http\Controllers\Employee\DevisController::class, 'create'])->name('employee.creer.devis');
        Route::post('/devis/store', [App\Http\Controllers\Employee\DevisController::class, 'store'])->name('employee.devis.store');
        Route::post('/devis/update/{id}', [App\Http\Controllers\Employee\DevisController::class, 'update'])->name('employee.devis.update');
        Route::get('/devis/edit/{id}', [App\Http\Controllers\Employee\DevisController::class, 'edit'])->name('employee.devis.edit');
        Route::get('/devis/delete/{id}', [App\Http\Controllers\Employee\DevisController::class, 'destroy'])->name('employee.devis.delete');
        
        // clients
        Route::get('/clients', [App\Http\Controllers\Employee\ClientController::class, 'index'])->name('employee.clients');
        Route::get('/clients/search/clients', [App\Http\Controllers\Employee\ClientController::class, 'Searchclient'])->name('employee.clients.search');
        Route::get('/clients/ajout', [App\Http\Controllers\Employee\ClientController::class, 'create'])->name('employee.clients.ajout');
        Route::post('/clients/store', [App\Http\Controllers\Employee\ClientController::class, 'store'])->name('employee.clients.store');
        Route::post('/clients/update/{id}', [App\Http\Controllers\Employee\ClientController::class, 'update'])->name('employee.clients.update');
        Route::get('/clients/edit/{id}', [App\Http\Controllers\Employee\ClientController::class, 'edit'])->name('employee.clients.edit');
        Route::get('/clients/delete/{id}', [App\Http\Controllers\Employee\ClientController::class, 'destroy'])->name('employee.clients.delete');


        Route::get('/bibliotheque/', [App\Http\Controllers\Employee\biblioController::class, 'index'])->name('employee.biblio');
        Route::get('/bibliotheque/search/bibliotheque', [App\Http\Controllers\Employee\biblioController::class, 'biblioSearch'])->name('employee.biblio.search');
        Route::get('/bibliotheque/ajout/{id}', [App\Http\Controllers\Employee\biblioController::class, 'edit'])->name('employee.biblio.ajouter');
        Route::post('/bibliotheque/store/{id}', [App\Http\Controllers\Employee\biblioController::class, 'update'])->name('employee.biblio.store');


    });
});