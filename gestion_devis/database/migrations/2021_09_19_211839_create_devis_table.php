<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devis', function (Blueprint $table) {
            $table->id();
            $table->longText('description');
            $table->string('numero');
            $table->string('date');
            $table->string('compagne');
            $table->string('clients_contact');
            $table->string('clients_entreprise');
            $table->string('clients_phone');
            $table->string('quantite');
            $table->string('prix');
            $table->string('total');
            $table->string('sous_total');
            $table->string('remise');
            $table->string('sous_remise_total');
            $table->string('tva');
            $table->string('totalttc');
            $table->timestamps();
        });
    }

    

    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devis');
    }
}
