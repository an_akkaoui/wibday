<?php

namespace App\Http\Controllers\Employee;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Client;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        $personel = Client::all();
        return view('employee.clients.clients',['personel'=> $personel]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employee.clients.ajouter');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // "entreprise","contact","ville","email","phone"
        $request->validate([
            'entreprise' => 'required',
            'contact' => 'required',
            'ville' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ]);

        $st = client::create([
            'entreprise' => $request->entreprise,
            'contact' => $request->contact,
            'ville' => $request->ville,
            'email' => $request->email,
            'phone' => $request->phone,
        ]);
        $st->save();
        return redirect()->route('clients')->with('success','Client bien ajouter!');
    }

    public function Searchclient(REQUEST $request){

        if (isset($_GET['query'])){
  
          $search_text = $_GET['query'];
          $personel = DB::table('clients')->where('contact','LIKE','%'.$search_text.'%')->paginate(3);
          $personel->appends($request->all());
          return view('employee.clients.search',['personel'=>$personel]);
        }
           else{
            return view('employee.clients.search');
           }
           return view('employee.clients.search');
      }


      
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $st = Client::find($id);

        return view('employee.clients.edit')->with('stock', $st);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'contact' => 'required',
            'entreprise' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'ville' => 'required',
        ]);

        $st = Client::find($id);

        $st->contact = $request->contact;
        $st->entreprise = $request->entreprise;
        $st->email = $request->email;
        $st->phone = $request->phone;
        $st->ville = $request->ville;

        $st->save();

        return redirect()->route('clients')->with('success','bien Modifier!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $st = Client::find($id);
        $st->delete();
        return redirect()->route('clients')->with('success','Client bien supprimer!');
    }
}
