<?php

namespace App\Http\Controllers\Employee;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Devis;
use App\Models\Client;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use Dompdf\Dompdf;

class DevisController extends Controller
{
    
    public function index()
    {
        $personel = Devis::all();
        return view('employee.devis.devis',['personel'=> $personel]);
        
    }

    public function create()
    {
        $client = Client::all();
        return view('employee.devis.creer',['client'=>$client]);
    }

 
    public function store(Request $request)
    {
        $request->validate([
          'description' => 'required',
          'quantite' => 'required',
          'total' => 'required',
          'prix' => 'required',
          'numero' => 'required',
          'date' => 'required',
          'compagne' => 'required',
          'clients_contact' => 'required',
          'clients_entreprise' => 'required',
          'clients_phone' => 'required',
          'sous_total' => 'required',
          'remise' => 'required',
          'sous_remise_total' => 'required',
          'tva' => 'required',
          'totalttc' => 'required',
          'status' => 'required',
        ]);

        $st = Devis::create([
          'description' => $request->description,
          'quantite' => $request->quantite,
          'total' => $request->total,
          'clients_contact' => $request->clients_contact,
          'prix' => $request->prix,
          'numero' => $request->numero,
          'date' => $request->date,
          'compagne' => $request->compagne,
          'clients_entreprise' => $request->clients_entreprise,
          'clients_phone' => $request->clients_phone,
          'sous_total' => $request->sous_total,
          'remise' => $request->remise,
          'sous_remise_total' => $request->sous_remise_total,
          'tva' => $request->tva,
          'totalttc' => $request->totalttc,
          'status' => $request->status,
        ]);
        $st->save();
        return redirect()->route('employee.devis')->with('success','Devis bien Ajouter!');
    }


    public function autocompleteSearch(REQUEST $request){

        if (isset($_GET['query'])){
          $search_text = $_GET['query'];
  
          $devis = DB::table('devis')->where('clients_contact','LIKE','%'.$search_text.'%')->paginate(3);
          $devis->appends($request->all());
          return view('employee.devis.search',['devis'=>$devis]);
        }
           else{
            return view('employee.devis.search');
           }
           return view('employee.devis.search');
      }



      public function apercu($id){

        $data = DB::table('devis')->where('id',$id)
        ->orderBy('clients_contact')
        ->get();

        return view('employee.devis.pdf',['data'=>$data]);
    }





    public function edit($id)
    {
        $st = Devis::find($id);

        return view('employee.devis.edit')->with('stock', $st);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'description' => 'required',
            'quantite' => 'required',
            'total' => 'required',
            'prix' => 'required',
            'status' => 'required',
        ]);

        $st = Devis::find($id);

        $st->description = $request->description;
        $st->quantite = $request->quantite;
        $st->prix = $request->prix;
        $st->total = $request->total;
        $st->status = $request->status;

        $st->save();

        return redirect()->route('employee.devis')->with('success','Devis bien Modifier!');
    }


    public function destroy($id)
    {
        $st = Devis::find($id);
        $st->delete();
        return redirect()->route('employee.devis')->with('success','Devis est supprimé!');
    }

    public function pdf($id)
    {
        $devis = Devis::find($id);

        $image = url('image/logowib.png');

        $dompdf = new Dompdf();
        $devisdetails = Devis::all()->where('id',$id)->first()->toArray();

        $output = '
        <!DOCTYPE html>
        <html lang="en">
          <head>
            <meta charset="utf-8">
            <title>PDF</title>
            <link rel="stylesheet" href="style.css" media="all" />
          </head>
          <body>

          <style>
          @font-face {
            font-family: SourceSansPro;
            src: url(SourceSansPro-Regular.ttf);
          }
          
          .clearfix:after {
            content: "";
            display: table;
            clear: both;
          }
          
          a {
            color: #0087C3;
            text-decoration: none;
          }
          
          body {
            position: relative;
            width: 21cm;  
            height: 29.7cm; 
            margin: 0 auto; 
            color: #555555;
            background: #FFFFFF; 
            font-family: Arial, sans-serif; 
            font-size: 14px; 
            font-family: SourceSansPro;
          }
          
          header {
            padding: 10px 0;
            margin-bottom: 20px;
            border-bottom: 1px solid #AAAAAA;
          }
          
          #logo {
            float: left;
            margin-top: 8px;
          }
          
          #logo img {
            height: 70px;
          }
          
          #company {
            float: right;
            text-align: right;
          }
          
          
          #details {
            margin-bottom: 50px;
          }
          
          #client {
            padding-left: 6px;
            border-left: 6px solid #0087C3;
            float: left;
          }
          
          #client .to {
            color: #777777;
          }
          
          h2.name {
            font-size: 1.4em;
            font-weight: normal;
            margin: 0;
          }
          
          #invoice {
            float: right;
            text-align: right;
          }
          
          #invoice h1 {
            color: #0087C3;
            font-size: 2.4em;
            line-height: 1em;
            font-weight: normal;
            margin: 0  0 10px 0;
          }
          
          #invoice .date {
            font-size: 1.1em;
            color: #777777;
          }
          
          table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 20px;
          }
          
          table th,
          table td {
            padding: 20px;
            background: #EEEEEE;
            text-align: center;
            border-bottom: 1px solid #FFFFFF;
          }
          
          table th {
            white-space: nowrap;        
            font-weight: normal;
          }
          
          table td {
            text-align: right;
          }
          
          table td h3{
            color: #57B223;
            font-size: 1.2em;
            font-weight: normal;
            margin: 0 0 0.2em 0;
          }
          
          table .no {
            color: #FFFFFF;
            font-size: 1.6em;
            background: #0e70b1;
          }
          
          table .desc {
            text-align: left;
          }
          
          table .unit {
            background: #DDDDDD;
          }
          
          table .total {
            background: #0e70b1;
            color: #FFFFFF;
          }
          
          table td.unit,
          table td.qty,
          table td.total {
            font-size: 1.2em;
          }
         
          
          table tbody tr:last-child td {
            border: none;
          }
          
          table tfoot td {
            padding: 10px 20px;
            background: #FFFFFF;
            border-bottom: none;
            font-size: 1.2em;
            white-space: nowrap; 
            border-top: 1px solid #AAAAAA; 
          }
          
          table tfoot tr:first-child td {
            border-top: none; 
          }
          
          table tfoot tr:last-child td {
            color: #0e70b1;
            font-size: 1.4em;
            border-top: 1px solid #0e70b1; 
          
          }
          
          table tfoot tr td:first-child {
            border: none;
          }
          
          #thanks{
            font-size: 2em;
            margin-bottom: 50px;
          }
          
          #notices{
            padding-left: 6px;
            border-left: 6px solid #0087C3;  
          }
          
          #notices .notice {
            font-size: 1.2em;
          }
          
          footer {
            color: #777777;
            width: 100%;
            height: 30px;
            position: absolute;
            bottom: 0;
            border-top: 1px solid #AAAAAA;
            padding: 8px 0;
            text-align: center;
          }
          </style>
            <header class="clearfix">
              <div id="logo">
              
                <img src="./devis-app/public/image/logowib.png" style="width: 200px; max-width: 200px" alt="logo" />
              </div>
              <div style="float: right;">
                <div>Devis n° : '.$devisdetails['numero'].'</div>
                <div>Date emission : '.$devisdetails['date'].' </div>
              </div>
              </div>
            </header>
            <main>
              <div id="details" class="clearfix">
                <div id="client">
                  <div class="to">Contact: '.$devisdetails['clients_contact'].' </div>
                  <h2 class="name">Entreprise: '.$devisdetails['clients_entreprise'].'</h2>
                  <div class="address">Telehone : '.$devisdetails['clients_phone'].'</div>
                </div>
                <div style="float: right;">
                  <h5>Titre de Compagne : '.$devisdetails['compagne'].'</h5>
                </div>
              </div>
              <table border="0" cellspacing="0" cellpadding="0">
                
                  <tr>
                  <td class="no">#</td>
                    <th class="desc">DESCRIPTION</th>
                    <th class="unit">UNIT PRICE</th>
                    <th class="qty">QUANTITY</th>
                    <th class="total">TOTAL</th>
                  </tr>
               
                <tbody>
                  <tr>
                  <td class="no">'.$devisdetails['id'].'</td>
                    <td class="desc">'.$devisdetails['description'].'</td>
                    <td class="unit">'.$devisdetails['prix'].'</td>
                    <td class="qty">'.$devisdetails['quantite'].'</td>
                    <td class="total">'.$devisdetails['total'].'</td>
                  </tr>
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="2"></td>
                    <td colspan="2">Sous TOTAL</td>
                    <td>'.$devisdetails['sous_total'].'</td>
                  </tr>
                  <tr>
                    <td colspan="2"></td>
                    <td colspan="2">Remise</td>
                    <td>'.$devisdetails['remise'].'</td>
                  </tr>
                  <tr>
                    <td colspan="2"></td>
                    <td colspan="2">sous total moins Total</td>
                    <td>'.$devisdetails['sous_remise_total'].'</td>
                  </tr>
                  <tr>
                    <td colspan="2"></td>
                    <td colspan="2">TVA 20%</td>
                    <td>'.$devisdetails['tva'].'</td>
                  </tr>
                  <tr>
                    <td colspan="2"></td>
                    <td colspan="2">TOTAL TTC</td>
                    <td>'.$devisdetails['totalttc'].'</td>
                  </tr>
                </tfoot>
              </table>
              
            </main>
            <footer>
                WIB DAY SARL <br>
                203 Boulevard Zerktouni, Casablanca, 20100 <br>
                RC : 302523 – IF : 15163323 – Patente : 35891715 – Tél : 0522 39 77 55
            </footer>
          </body>
        </html>
        ';

        

        $dompdf->loadHtml($output);


        return PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadHtml($output)->setPaper('a3', 'landscape')->stream();
    
    }

}
