<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Client;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        $personel = Client::all();
        return view('admin.clients.clients',['personel'=> $personel]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.clients.ajouter');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // "entreprise","contact","ville","email","phone"
        $request->validate([
            'entreprise' => 'required',
            'contact' => 'required',
            'ville' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ]);

        $st = client::create([
            'entreprise' => $request->entreprise,
            'contact' => $request->contact,
            'ville' => $request->ville,
            'email' => $request->email,
            'phone' => $request->phone,
        ]);
        $st->save();
        return redirect()->route('admin.clients')->with('success','Client bien ajouter!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Searchclient(REQUEST $request){

        if (isset($_GET['query'])){
  
          $search_text = $_GET['query'];
          $personel = DB::table('clients')->where('contact','LIKE','%'.$search_text.'%')->paginate(1);
          $personel->appends($request->all());
          return view('admin.clients.search',['personel'=>$personel]);
        }
           else{
            return view('admin.clients.search');
           }
           return view('admin.clients.search');
      }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $st = Client::find($id);

        return view('admin.clients.edit')->with('stock', $st);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'contact' => 'required',
            'entreprise' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'ville' => 'required',
        ]);

        $st = Client::find($id);

        $st->contact = $request->contact;
        $st->entreprise = $request->entreprise;
        $st->email = $request->email;
        $st->phone = $request->phone;
        $st->ville = $request->ville;

        $st->save();

        return redirect()->route('admin.clients')->with('success','bien Modifier!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $st = Client::find($id);
        $st->delete();
        return redirect()->route('admin.clients')->with('success','Client bien supprimer!');
    }
}
