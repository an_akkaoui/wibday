<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Devis;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
  
    public function index()
    {
        $personel = Devis::all();
        $completed = $personel->where('status', 'valide');
        $encours = $personel->where('status', 'encours');
        $total = Devis::where('status', 'valide')->sum('total');
        return view('admin.dash',compact('personel','completed','encours','total'));
      
    }

   

    public function profile()
    {
        $id = Auth::user()->id;
        $data = User::all()->where('id',$id);
        return view('admin.profile',['data'=>$data]);
    }

    public function changePassword(Request $request)
    {
        
        $request->validate([
          'current_password' => 'required',
          'password' => 'required|string|min:6|confirmed',
          'password_confirmation' => 'required',
        ]);

        $user = Auth::user();

        if (!Hash::check($request->current_password, $user->password)) {
            return back()->with('error', 'Mot de passe incorrect!');
        }

        $user->password = Hash::make($request->password);

        $user->save();

        return back()->with('success', 'Mot de passe bien Changer!');
    }
}
