<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\biblios;
use App\Models\Client;
use App\Models\Devis;
use phpDocumentor\Reflection\Types\Null_;

class biblioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $devis = Devis::all();
        return view('admin.biblio.show',['devis'=> $devis]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $devis  = Devis::find($id);
      
     return view('admin.biblio.ajout',['devis'=> $devis]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[  
            'bon' => 'required|mimes:png,jpg,jpeg,csv,txt,pdf|max:2048',
        ]);
        
        $biblio  = Devis::find($id);
        
       
            $file = $request->bon;
            $file_new_name = time().$file->getClientOriginalName();
            $file->move('uploads/bon', $file_new_name);

        

        $biblio->status = 'valide';

        $biblio->bon = 'uploads/produit/'. $file_new_name;
        $biblio->save();

      return redirect()->route('admin.devis')->with('success','Bon de commande bien Ajouter!');

     
    }

    public function biblioSearch(REQUEST $request){

        if (isset($_GET['query'])){
          $search_text = $_GET['query'];
  
          $devis = DB::table('devis')->where('clients_contact','LIKE','%'.$search_text.'%')->paginate(3);
          $devis->appends($request->all());
          return view('admin.biblio.search',['devis'=>$devis]);
        }
           else{
            return view('admin.biblio.search');
           }
           return view('admin.biblio.search');
      }

    public function destroy($id)
    {
        //
    }
}
