<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller 
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personel = User::all()->where('role' ,'=', 'employe');
        return view('admin.employee.employee',['personel'=> $personel]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        return view('admin.employee.ajouter');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'role' => 'required',

        ]);
        // dd($request->all());

        $user = new User;
        $user->email = $request->email;
        $user->name = $request->name;
        $user->password = Hash::make($request->password);
        $user->role = $request->role;
        $user->save();
        
        return redirect()->route('admin.employee')->with('success','Employee bien Ajouter!');
    }

  
    public function edit($id)
    {
        $st = User::find($id);

        return view('admin.employee.edit')->with('stock', $st);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $user->email = $request->email;
        $user->name = $request->name;
        $user->password = Hash::make($request->password);

        
        $user->save();
        
        return redirect()->route('admin.employee')->with('success','Employee bien Modifier!');
    }

    public function employeeSearch(REQUEST $request){

        if (isset($_GET['query'])){
  
            $search_text = $_GET['query'];
            $personel = DB::table('users')->where('name','LIKE','%'.$search_text.'%')->paginate(3);
            $personel->appends($request->all());
            return view('admin.employee.search',['personel'=>$personel]);
          }
             else{
              return view('admin.employee.search');
             }
             return view('admin.employee.search');
      }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->route('admin.employee')->with('success','Employee est supprimé!');
    }
}
