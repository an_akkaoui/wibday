<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Devis extends Model
{
    use HasFactory;
    protected $table = 'devis';

    protected $fillable =[
        "id","description","quantite","total","prix","numero",
        "date","compagne","clients_contact","clients_entreprise","clients_phone","sous_total",
        "remise","sous_remise_total","tva","totalttc","status"
    ];

    public function clients(){  
        return $this->hasMany('App\client');
    }
    public function biblio(){
        return $this->hasOne('App\biblio');
    }
}
