<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class biblios extends Model
{
    use HasFactory;
   
    protected $fillable =[
        "clients_entreprise","devis_numero","clients_contact","devis_total","bon"
    ];

    public function clients(){  
        return $this->belongsTo('App\client');
    }
    public function devis(){
        return $this->belongsTo('App\devis');
    }
}
