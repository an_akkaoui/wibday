<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $fillable =[
        "entreprise","contact","ville","email","phone"
    ];


    public function devis(){
        return $this->belongsToMany('App\devis');
    }
    public function biblio(){
        return $this->hasOne('App\biblio');
    }
}
