@extends('employee.dashboard')

@section('name')

<div class="p-3">

  <h4 class="text-info">Ajouter un client: </h4>
  <div class="panel panel-default">
    <div class="panel-body">
      <form action="{{ route('employee.clients.store') }}" method="POST">
        @csrf
        {{ csrf_field() }}
        <div class="d-flex flex-column">
          <label for="" >Nom de contact :</label>
          <input name='contact'  type="text">
        </div>
        <div class="d-flex flex-column">
          <label for="">Nom d'Entreprise :</label>
          <input name='entreprise'  type="text">
        </div> 
        <div class="d-flex flex-column">
          <label for="">Email :</label>
          <input name='email'  type="text">
        </div>  
        <div class="d-flex flex-column">
          <label for="">Phone :</label>
          <input name='phone' type="text">
        </div> 
        <div class="d-flex flex-column">
          <label for="">Ville :</label>
          <input name='ville' type="text">
        </div> 
          
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-info" >Ajouter</button>
          <a type="button" class="btn btn-default" href="{{ route('employee.clients') }}">Close</a>
        </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </form>
</div>
@endsection


