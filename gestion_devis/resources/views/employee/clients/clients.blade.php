@extends('employee.dashboard')
@section('name')

<script type="text/javascript">
    var route = "{{ url('autocomplete-search') }}";

    $('#search').typeahead({
        source: function (query, process) {
            return $.get(route, {
                query: query
            }, function (data) {
                return process(data);
            });
        }
    });
</script>

<section class="p-2 mt-3">
  
<div class="container bg-light mt-4" style="height: 100px">
    <h6 class="text-primary p-2">Clients :</h6>
    <div class="d-flex justify-content-around">
      <a href="{{ route('employee.clients.ajout') }}"><p>Nouveaux Client</p></a> 
      <a href=""><p>Existant Client</p></a>
    </div>

</div>
<section class="p-2 mt-3">
    <section class="search" style="text-align: center;
    margin: auto;">
        <img src="/image/audit.png" width="50px" height="50px" alt="">
          <h5>Recherche Client :</h5>
          <form action="{{ route('employee.clients.search') }}" method="GET">
            <div class="d-flex justify-content-center p-1">
                <a class="btn btn-info ml-2" href="{{ route('admin.devis') }}"><< devis </a>
                <input type="search" class="form-control col-2 ml-2" id="search" name="query" placeholder="Search" value="">
                <button type="submit" class="btn btn-outline-info ml-2" >Rechercher</button>
            </div>
        </form>
    </section>
<div class="panel panel-default">
    <div class="panel-body">
        <table class="table table-striped mt-3">
            <thead class="bg-info text-center">
                <th>
                    Nom de Contact 
                </th>
               <th>
                Nom de l'entreprise
               </th>
               <th>
                   Email
               </th>
               <th>
                   Phone
               </th>
               <th>
                ville
            </th>
               <th>
                <a class="ml-3" href="{{ route('employee.clients.ajout') }}"><img src="/image/add.svg" width="20px" height="20px" alt=""></a>
            </th>
            
            </thead>
            <tbody class="text-center"> 

                @foreach ($personel as $personel)
                <td>
                        {{ $personel->contact }}
                    </td>
                    <td>
                        {{ $personel->entreprise }}
                     </td>
                     <td>
                        {{ $personel->email }}
                     </td>
                     <td>
                        {{ $personel->phone }}
                 </td>
                 <td>
                    {{ $personel->ville }}
             </td>
                
             <td class="d-flex">
                <a class="ml-3" href="{{ route('employee.clients.edit', ['id' => $personel->id ]) }}"><img src="/image/edit.svg" width="20px" height="20px" alt=""></a>
              <a class="ml-3" href="{{ route('employee.clients.delete', ['id' => $personel->id ]) }}"><img src="/image/delete.svg" alt="" width="20px" height="20px"></a>  
            </td>
                </tr>
                    
                @endforeach
            </tbody>
           
           
           </table>
           
    </div>
</div>   
</section>



    
@endsection
