@extends('employee.dashboard')

@section('name')
    

 <section class="p-2 col-lg-12">
   <h6 class="mt-2 p-2 text-primary">Page de bord:</h6>

   <div class="container d-flex justify-content-around mt-2 p-2">
        <div class="card" style="border-radius:12px;width: 35%;">
          <div class="card-body">
            <h5 class="card-title">Clients</h5>
            <div class="text-center"> <img class="ml-3" src="/image/target.svg" width="50px" height="50px" alt=""></div>
            <div class="text-center mt-2"><a href="{{ route('employee.clients.ajout') }}" class="btn btn-info">Ajouter</a></div>  
          </div>
        </div>
        
        <div class="card" style="border-radius:12px;width: 35%;">
          <div class="card-body">
            <h5 class="card-title">Devis</h5>
            <div class="text-center"> <img class="ml-3" src="/image/estimation.svg" width="50px" height="50px" alt=""></div>
          <div class="text-center mt-2"><a href="{{ route('employee.creer.devis') }}" class="btn btn-info">Ajouter</a></div>  
          </div>
      </div>
    </div>
   <div class="col-md-12 container d-flex justify-content-between mt-2 p-2">
      <div class="">
        <div class="card" style="border-radius:12px;width: 15rem;height:6rem">
          <div class="card-body">
            <h5 class="card-title" style="border-bottom: 1px solid rgb(65, 119, 201)">Total Devis</h5>
          <p>{{ $personel->count() }}</p>
          </div>
        </div>
      </div>
        <div class="card" style="border-radius:12px;width: 15rem;height:6rem">
          <div class="card-body">
            <h5 class="card-title" style="border-bottom: 1px solid rgb(65, 119, 201)">Total Devis Validé</h5>
            <p>{{ $completed->count() }}</p>
          </div>
      </div>
        <div class="card" style="border-radius:12px;width: 15rem;height:6rem">
          <div class="card-body">
            <h5 class="card-title" style="border-bottom: 1px solid rgb(65, 119, 201)">Total Devis en cours</h5>
          <p>{{ $encours->count() }}</p>

          </div>
        </div>
    </div>
    <div class="col-md-12 container d-flex justify-content-around mt-2 p-2">
          <div class="card" style="border-radius:12px;width: 22rem;height:6rem">
            <div class="card-body">
              <h5 class="card-title" style="border-bottom: 1px solid rgb(65, 119, 201)">Chiffre d'affaire TTC (Devis Validé)</h5>
              <p>{{ $total }}</p>
            </div>
          </div>

          <div class="card" style="border-radius:12px;width: 22rem;height:6rem">
            <div class="card-body">
              <h5 class="card-title" style="border-bottom: 1px solid rgb(65, 119, 201)">Chiffre d'affaire HT (Devis Validé) </h5>
              <p>100997865 DHS HT</p>
            </div>
          </div>
        </div>
      </div>




     <div class="card text-center mt-4">
     <div class="card-header" style="background-color: #0daee4">
      <h5 class="card-title">Dernières operations :</h5>
     </div>
     <div class="card-body">
       <table class="table table-striped">
         <thead>
           <tr>
            <th scope="col">N°devis</th>
             <th scope="col">Description</th>
             <th scope="col">Quantité</th>
             <th scope="col">Prix Unitaire</th>
             <th scope="col">Total</th>
           </tr>
         </thead>
         @foreach ($personel as $personel)
         <tbody>
           <tr>
             <td>
              {{ $personel->numero }}
             </td>
            <td>
              {{ $personel->description }}
          </td>
          <td>
              {{ $personel->quantite }}
           </td>
           <td>
              {{ $personel->prix }}
           </td>
           <td>
              {{ $personel->total }}
       </td>
           </tr>
          
         </tbody>
         @endforeach
       </table>
     </div>
    </div>



  </section>
@endsection