<style>

    .pagination{
        display: inline-block;
     }
     
</style>
@if($paginator->hasPages())
<ul class="pagination">
  
  @if($paginator->onFirstPage()) 
    <a ><span>&laquo;</span></a>
  @else
    <a href="{{ $paginator->previousPageUrl() }}"  rel="prev">&laquo;</a>
  @endif

  
  @foreach($elements as $element)
       
       @if(is_string($element))
          <a><span>{{$element}}</span></a>
       @endif

      
       @if(is_array($element))
           @foreach($element as $page=>$url)

            @if($page == $paginator->currentPage())
                <a><span>{{ $page }}</span></a>
            @else
                  <a href="{{ $url }}">{{ $page }}</a>
            @endif

           @endforeach
       @endif

  @endforeach

 
  @if($paginator->hasMorePages())
     <a href="{{ $paginator->nextPageUrl() }}"><span>&raquo;</span></a>
  @else
  <a ><span>&raquo;</span></a>
  @endif
</ul>

@endif
