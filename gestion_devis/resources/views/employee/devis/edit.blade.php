@extends('employee.dashboard')

@section('name')
<div class="panel-heading d-flex flex-row mt-4">
   <h6 class="text-primary p-3">Modifier Devis:</h6> 
</div>
<div class="panel panel-default">
  <div class="panel-body">
    <form action="{{ route('employee.devis.update',['id' => $stock->id])}}" method="POST"  enctype="multipart/form-data">
     {{ csrf_field() }}
     <div>
  <div class="d-flex flex-column">
    <label for="">Description</label>
    <input name='description' value="{{ $stock->description }}" type="text">
  </div> 
  <div class="d-flex flex-column">
    <label for="">Quantité</label>
    <input name='quantite' value="{{ $stock->quantite }}" type="text">
  </div>  
  <div class="d-flex flex-column">
    <label for="">Prix Unitaire</label>
    <input name='prix' value="{{ $stock->prix }}" type="text">
  </div> 
  <div class="d-flex flex-column">
    <label for="">Total</label>
    <input name='total' value="{{ $stock->total }}" type="text">
  </div>     
     </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info" >Modifier</button>
        <a type="button" class="btn btn-default" href="{{ route('employee.devis') }}">Close</a>
      </div>
  </div>
  
        </div>
      </div>
    </div>
  </div>
</div>
</form>
@endsection