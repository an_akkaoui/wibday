@extends('employee.dashboard')
@section('name')

<section class="p-3 col-lg-12">
  <h4 class="text-info p-3">Ajouter un bon :</h4>
  <form action="{{ route('employee.biblio.store',['id' => $devis->id]) }}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}
     <div class="container p-3 d-flex flex-column">
      
       <div class="card">
        @if ($devis->status =='encours')
        <div class="bg-warning p-1">
        <h5 class="bg-warning text-white"> {{ $devis->status }} </h5>
        </div>
            
        @endif
        @if ($devis->status == 'valide')
        <div class="bg-success p-1">
        <h5 class="text-white" > {{ $devis->status }} </h5>
        </div>
            
        @endif
        @if ($devis->status == 'brouillant')
        <div class="bg-secondary p-1">
          <h5 class="text-white" > {{ $devis->status }} </h5>
        </div>
        @endif 
       </div>
       <div class="d-flex justify-content-around p-3">

         <div class="col-5 d-flex flex-column p-3">
           <h6>{{ $devis->clients_contact }}</h6>
           <h6>{{ $devis->clients_entreprise }}</h6>
           <h6>{{ $devis->clients_phone }}</h6>
         </div>
  
         <div class="col-5 d-flex flex-column p-3">
           <div class="d-flex">
            <label for="">Devis n° :</label>
            <input class="col-6 ml-2 inputfield" name="numero" value="{{ $devis->numero }}"  style="height: 20px" disabled>
          </div>
          <div class="d-flex">
            <label for="">Date d'emission :</label>
            <input class="col-6 ml-2 form-control" name="date" value="{{ $devis->date }}"  style="height: 20px" disabled>
          </div>
         </div>
       </div>

       <div class="d-flex justify-content-center p-3">
        <table class="table table-bordred col-lg-10 text-center">
          <thead class="bg-info">
            <tr>
              <th scope="col">Description</th>
              <th class="col-2">Quantité</th>
              <th class="col-2">Prix Unitaire</th>
              <th class="col-2">Total</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th ><textarea name="description" rows="1" cols="50" disabled>{{ $devis->description }}</textarea></th>
              <td><input class="col-12" type="text" value="{{ $devis->quantite }}"  disabled></td>
              <td><input class="col-12" type="text" value="{{ $devis->prix }}" disabled></td>
              <td><input class="col-12" type="text" value="{{ $devis->total }}"  disabled></td>
            </tr>
          </tbody>
        </table>  
        </div>
      
        <div class="col-11 mt-2 d-flex align-items-end flex-column">
        <div class="d-flex">
          <label for="">sous-total :</label>
          <input class=" ml-2 form-control"  value="{{ $devis->sous_total }}" type="text" style="height: 20px;width:70px" disabled>
        </div>
        <div class="d-flex">
          <label for="">Remise :</label>
          <input class="ml-2 form-control"  type="text" value="{{ $devis->remise }}" style="height: 20px;width:70px" disabled>
        </div>
        <div class="d-flex">
          <label for="">sous-total moins Remise :</label>
          <input class=" ml-2 form-control"  value="{{ $devis->sous_remise_total }}" type="text" style="height: 20px;width:70px" disabled>
        </div>
        <div class="d-flex">
          <label for="">TVA 20% :</label>
          <input class="ml-2 form-control"  type="text" value="{{ $devis->tva }}" style="height: 20px;width:70px" disabled>
        </div>
        <div class="d-flex">
          <label for="">Total TTC :</label>
          <input class="ml-2 form-control"  type="text" value="{{ $devis->totalttc }}" style="height: 20px;width:70px" disabled>
        </div>
        </div>



      <div class="p-3 d-flex justify-content-center col-12 text-center">
        <label for="avatar">Inserer le bon:</label>
          <input class="p-1 ml-3" type="file" name="bon" multiple>
      </div>
      <div class="text-center">

        <button  type="submit" class="btn btn-info col-2 " style="height: 40px">AJouter</button>
      </div>
      
      
      
      
    </div>
  </form>

</section>
@endsection