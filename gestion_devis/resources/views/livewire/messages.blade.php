<div class="p-3" style="min-height: 95vh">
    <style>
        .avatar {
    height: 30px;
    width: 30px;
}
.list-group-item:hover, .list-group-item:focus {
    background: rgba(24,32,23,0.37);
    cursor: pointer;
}
.chatbox {
    height: 50vh !important;
    overflow-y: scroll;
}
.message-box {
    height: 70vh !important;
    overflow-y: scroll;display:flex; flex-direction:column-reverse;
}
.single-message {
    background: #f1f0f0;
    border-radius: 12px;
    padding: 10px;
    margin-bottom: 10px;
    width: fit-content;
}
.received {
    margin-right: auto !important;
}
.sent {
    margin-left: auto !important;
    background :#3490dc;
    color: white!important;
}
.sent small {
    color: white !important;
}
.link:hover {
    list-style: none !important;
    text-decoration: none;
}
.online-icon {
    font-size: 11px !important;
}

    </style>
    
	<div class="container">
    <div class="row justify-content-center">
      
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                       <h4 class="text-info">Utilisateurs</h4> 
                    </div>
                    <div class="card-body chatbox p-0">
                        <ul class="list-group list-group-flush">
                          @foreach($users as $user)

                          @if($user->id !== auth()->id())
                          @php
                              $not_seen= App\Models\Message::where('user_id',$user->id)->where('receiver_id',auth()->id())->where('is_seen',false)->get() ?? null

                          @endphp
                                <a wire:click="getUser({{$user->id}})"  class="text-dark link">
                                    <li class="list-group-item">
                                        <img class="img-fluid avatar" src="https://cdn.pixabay.com/photo/2017/06/13/12/53/profile-2398782_1280.png">
                                        @if($user->is_online==true)
                                          
                                         <i class="fa fa-circle text-success online-icon">
                                            @endif
                                             
                                         </i> {{$user->name}}
                                       @if(filled($not_seen))
                                            <div class="badge badge-success rounded"> {{ $not_seen->count()}} </div>
                                            @endif
                                    </li>
                                </a>
                                @endif
                          @endforeach
                        </ul>
                    </div>
                </div>
            </div>
  
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                     @if(isset($sender)) {{$sender->name}}   @endif
                </div>
                <div class="card-body message-box" wire:poll="mountdata">
                   @if(filled($allmessages))
                     @foreach($allmessages as $mgs)
                            <div class="single-message @if($mgs->user_id == auth()->id()) sent @else received @endif">
                                <p class="font-weight-bolder my-0">{{$mgs->user->name}}</p>
                              {{ $mgs->message}}
                                <br><small class="text-muted w-100">Sent <em>{{$mgs->created_at}}</em></small>
                            </div>

                            @endforeach
                            @endif
                        
                </div>
                 
                <div class="card-footer">
                    <form wire:submit.prevent="SendMessage">
                        <div class="row">
                            <div class="col-md-8">
                                <input wire:model="message" class="form-control input shadow-none w-100 d-inline-block" placeholder="Type a message" required>
                            </div>

                            <div class="col-md-4">
                                <button type="submit" class="btn btn-primary d-inline-block w-100"><i class="far fa-paper-plane"></i> Send</button>
                            </div>
                        </div>
                    </form>
                </div>
             
            </div>
        </div>
    </div>
</div>
</div>
