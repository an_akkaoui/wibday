<!doctype html>
<html lang="en">
  <head>
    <title>Gestion Devis</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 <link rel="shortcut icon" type="image/x-icon" href="/image/w.png" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <style>
      html {
  height:100%;
}

body {
  margin:0;
}

.bg {
  animation:slide 3s ease-in-out infinite alternate;
  background-image: linear-gradient(-35deg,  rgba(252, 249, 249, 0.699) 50%,#afdbd6 50%);
  bottom:0;
  left:-50%;
  opacity:.5;
  position:fixed;
  right:-50%;
  top:0;
  z-index:-1;
}

.bg2 {
  animation-direction:alternate-reverse;
  animation-duration:10s;
}

.bg3 {
  animation-duration:10s;
}

@keyframes slide {
  0% {
    transform:translateX(-25%);
  }
  100% {
    transform:translateX(25%);
  }
}
  </style>
  <body style="align-items:center;min-height:100vh;display:flex;justify-content:center">

    <div class="bg"></div>
<div class="bg bg2"></div>
<div class="bg bg3"></div>

    <div class="col-md-6 p-0 d-flex justify-content-beetwen" style="background-color: rgb(250, 250, 250,0.7);border-radius:16px;max-height:60vh;"> 
        <div class="col-md-6" style="padding: 0px; border: 10px solid;border-width: 3px;border-radius:16px;border-color:rgb(22, 141, 161)">
           <img src="/image/logo.png" width="100%" height="100%" alt="" style="border-radius:12px;">
        </div>

        <div class="col-md-6 p-4">
            @if (Route::has('login'))
            @auth
             <div class="col-md-12 p-4 mt-4 d-flex flex-column text-center" style="height: 200px;">
                <div class="p-2 ">
                    <p style="color:#31aa9c ">page de bord :</p>
                    <a href="{{ url('admin/dashboard') }}" class="btn btn-info">Dashboard -></a>
                </div>
                 <div class="mt-auto p-2 bd-highlight">
                     <a href="{{ url('logout') }}" class=" btn btn-warning">Deconnexion</a>
                 </div>
             </div>

            @else
                <div class="container p-2">
                    <h5 class="text-center text-info">Log in</h5>

                                  <form method="POST" action="{{ route('login') }}">
                                      @csrf
                
                                      <div class="form-group row">
                                          <label for="email" class="col-form-label ml-2" style="font-size: 14px">EMail :</label>
                
                                          <div class="col-md-12" >
                                              <input style="max-height:28px;" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                
                                              @error('email')
                                                  <span class="invalid-feedback" role="alert" style="font-size: 10px">
                                                      <strong>{{ $message }}</strong>
                                                  </span>
                                              @enderror
                                          </div>
                                      </div>
                
                                      <div class="form-group row">
                                          <label for="password" class="col-form-label ml-2" style="font-size: 14px">Mot de passe :</label>
                
                                          <div class="col-md-12">
                                              <input style="max-height:28px;" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                
                                              @error('password')
                                                  <span class="invalid-feedback" role="alert" style="font-size: 10px">
                                                      <strong>{{ $message }}</strong>
                                                  </span>
                                              @enderror
                                          </div>
                                      </div>
                
                                      <div class="form-group row" >
                                              <div class="form-check col-md-12 text-center">
                                                  <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                
                                                  <label class="col-md-8 form-check-label" for="remember" style="font-size: 13px">
                                                      {{ __('Remember Me') }}
                                                  </label>
                                              </div>
                                          
                                      </div>
                
                                      <div class="col-md-12">
                                          <div class="col-md-12 text-center">
                                              <div>
                                                  <button type="submit" class="btn btn-info">
                                                      {{ __('Log in') }}
                                                  </button>
                                              </div>
                                              
                                              @if (Route::has('password.request'))
                                              <div class="col-md-12">
                                                      <a class="btn btn-link text-info" href="{{ route('password.request') }}" style="font-size: 12px">
                                                        Forgot Your Password
                                                      </a>
                                                    </div>
                                                  @endif
                
                                          </div>
                                      </div>
                                  </form>
                              
                          
                </div>

               
            @endauth
            @endif

        </div>



      
        
    </div>
    


    
    






       
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>