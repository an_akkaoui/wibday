@extends('admin.dashboard')

@section('name')

<section class="p-3 col-lg-12">
<div class="panel-heading d-flex flex-row mt-4">
   <h4 class="text-info p-3">Modifier Devis:</h4> 
</div>
<div class="panel panel-default">
  <div class="panel-body">
    <form action="{{ route('employee.update',['id' => $stock->id])}}" method="POST"  enctype="multipart/form-data">
     {{ csrf_field() }}
     <div>
  <div class="d-flex flex-column">
    <label for="">Description</label>
    <input name='name' value="{{ $stock->name }}" type="text">
  </div> 
  <div class="d-flex flex-column">
    <label for="">Quantité</label>
    <input name='email' value="{{ $stock->email }}" type="text">
  </div>  
  <div class="d-flex flex-column">
    <label for="">Prix Unitaire</label>
    <input name='password' value="{{ $stock->password }}" type="text">
  </div>     
     </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info" >Modifier</button>
        <a type="button" class="btn btn-default" href="{{ route('admin.devis') }}">Close</a>
      </div>
  </div>
  
        </div>
      </div>
    </div>
  </div>
</div>
</form>

</section>
@endsection