@extends('admin.dashboard')
@section('name')

<script>
    $(function () {
  $('[data-toggle="tooltip"]').tooltip();
})
var route = "{{ url('autocomplete-search') }}";
      
              $('#search').typeahead({
                  source: function (query, process) {
                      return $.get(route, {
                          query: query
                      }, function (data) {
                          return process(data);
                      });
                  }
              });
</script>

<style>
    .paginate{
   width: 100%;
    display: inline-block;
    margin: auto;
    width: 100%;
    margin-top: 34px;
    text-align: center;
    
 }
 
 .pas{
   width: 50%; 
   margin-top: 40px;
  margin: auto;
  text-align: center;

}
</style>
 
<section class="p-3">
    <h4 class="text-info p-3">Employees :</h4>

    <section class="p-2 mt-3">
        <section class="p-3 col-lg-12"> 
            <section class="search" style="text-align: center;
            margin: auto;">
                <img src="/image/audit.png" width="50px" height="50px" alt="">
                  <h5>Recherche Employee :</h5>
                  <form action="{{ route('admin.employee.search') }}" method="GET">
                    <div class="d-flex justify-content-center p-1">
                        <a class="btn btn-info ml-2" href="{{ route('admin.employee') }}"><< employee </a>
                        <input type="search" class="form-control col-2 ml-2" id="search" name="query" placeholder="Search" value="">
                        <button type="submit" class="btn btn-outline-info ml-2" >Rechercher</button>
                    </div>
                </form>
            </section>
    <div class="panel panel-default">

        <div class="panel-body">
            <table class="table table-striped mt-3">
                <thead class="bg-info text-center">
                    
                   <th>
                    Nom
                   </th>
                   <th>
                       Email
                   </th>
                   <th>
                    Mot de passe
                </th>
    
                   <th>
                    <a class="ml-3" href="{{ route('employee.ajouter') }}"><img src="/image/add.svg" width="20px" height="20px" alt=""></a>
                </th>
                </thead>
                <tbody class="text-center"> 
    
                    @foreach ($personel as $personel)
                    <tr>
                        <td>
                            {{ $personel->name }}
                        </td>
                         <td>
                            {{ $personel->email }}
                         </td>
                         <td>
                            {{ $personel->password }}
                         </td>
                         <td class="d-flex">
                            <a class="ml-3" href="{{ route('employee.edit', ['id' => $personel->id ]) }}"><img src="/image/edit.svg" width="20px" height="20px" alt=""></a>
                          <a class="ml-3" href="{{ route('employee.delete', ['id' => $personel->id ]) }}"><img src="/image/delete.svg" alt="" width="20px" height="20px"></a>  
                        </td>
                    </tr>
                        
                    @endforeach
                </tbody>
               
               
               </table>
               
        </div>
    </div>
</section>


    
@endsection
