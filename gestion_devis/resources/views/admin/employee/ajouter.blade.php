@extends('admin.dashboard')

@section('name')
<section class="p-3 col-lg-12">
<h4 class="text-info p-3">ajouter Compte:</h4>
<div class="panel panel-default">
  <div class="panel-body mb-3">
    <form action="{{ route('employee.store') }}" method="POST">
      @csrf
      {{ csrf_field() }}
      <div class="d-flex justify-content-around mb-2">
        <div class="d-flex flex-column col-4">
          <label for="">Nom :</label>
          <input name='name'  type="text">
        </div>  
        <div class="d-flex flex-column col-4">
          <label for="" >Email :</label>
          <input name='email'  type="text">
        </div>
      </div>
       <div class="d-flex justify-content-around mb-2">
         <div class="d-flex flex-column col-4">
           <label for="">password :</label>
           <input name='password' type="password">
         </div>
         <div class="d-flex flex-column col-4">
           <label for="">Role :</label>
           <select class="form-select" name='role'>
             <option name='role' selected >choisir ..</option>
             <option value="admin">Admin</option>
             <option value="employe">Employe</option>
           </select>
         </div>
        </div>
       </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info" >Ajouter</button>
        <a type="button" class="btn btn-default" href="{{ route('admin.employee') }}">Close</a>
      </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>
</section>
@endsection


