@extends('admin.dashboard')
@section('name')
<section class="p-3 col-lg-12" >
  

    <div class="container bg-light mt-4 d-flex justify-content-between" style="height: 100px">
      <div class="p-2 d-flex flex-column">
        <label for="">Clients :</label>
        <select id="select" name="" class="custom-select" id="">
          <option value="" selected>selectionner ou ajouter un client</option>
          @foreach ($client as $client)
          <option value="">{{ $client->contact }}</option>
          @endforeach
        </select>
      </div>

      <div class="bg bg-info col-2">
        <a class="ml-3" href="{{ route('clients.ajout') }}"><img src="/image/add.svg" width="20px" height="20px" alt=""></a>

      </div>

      
      
      <div class="mt-2 p-3 col-md-5 d-flex justify-content-around">
        <div>
          <h6 class="text-dark">Devis :</h6>
        </div>
        
        <div>
          <a  class="btn btn-info text-lignt" href="{{ route('admin.devis.apercu', ['id' => $client->id ]) }}">Apercu</a>   
        </div>
        
      </div>
      
      
      
      
      
    </div>
    <h4 class="p-3 text-info">Creer un Devis :</h4>
    <form action="{{ route('devis.store') }}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}

    
    <div class="col-lg-12">
    <div class="card">
    <div class="bg-info" style="height: 30px"> </div>
    <div class="p-2 bg-light" >
      <div class="d-flex flex-row justify-content-between">
        
        
        <img src="/image/logowib.png" width="150px" height="40px" alt="">
        <div class="d-flex flex-column">
        
              
          <div class="d-flex panel-body">
            <label for="">Devis n° :</label>
            <input class="col-6 ml-2 inputfield" name="numero" type="text" style="height: 20px">
          </div>
          <div class="d-flex">
            <label for="">Date d'emission :</label>
            <input class="col-6 ml-2 form-control" name="date" type="date" style="height: 20px">
          </div>
        </div>
      </div>
    </div>
   </div>
  
   <div class="container p-2 d-flex flex-row justify-content-around">
    <div class="p-2 mr-3 d-flex flex-column">
      <label style="color: #188FB8">destinatire :</label>
      <input class="mt-1 inputfield" type="text" value="{{ $client->contact }}" name="clients_contact" placeholder="Nom et Prenom" style="height: 20px">
      <input class="mt-1 inputfield" type="text" value="{{ $client->entreprise }}" name="clients_entreprise" placeholder="Entreprise" style="height: 20px">
      <input class="mt-1 inputfield" type="text" value="{{ $client->phone }}" name="clients_phone" placeholder="Telephone" style="height: 20px">
    </div>
    <div class="d-flex flex-column">
      <label style="color: #188FB8">Compagne :</label>
      <input class="ml-2 inputfield" name="compagne" type="text" placeholder="Titre de compagne " style="height: 20px">
    </div>
    
    
   </div>
   <div class="d-flex justify-content-center">
    <table class="table table-bordred col-lg-10 text-center">
      <thead class="bg-info">
        <tr>
          <th scope="col">Description</th>
          <th class="col-2">Quantité</th>
          <th class="col-2">Prix Unitaire</th>
          <th class="col-2">Total</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th ><textarea name="description" rows="1" cols="50"></textarea></th>
          <td><input class="col-8" type="text" name="quantite"></td>
          <td><input class="col-8" type="text" name="prix"></td>
          <td><input class="col-8" type="text" name="total"></td>
        </tr>
      </tbody>
    </table>  
    </div>
  
    <div class="col-11 mt-2 d-flex align-items-end flex-column">
    <div class="d-flex">
      <label for="">sous-total :</label>
      <input class=" ml-2 form-control" name="sous_total" type="text" style="height: 20px;width:70px">
    </div>
    <div class="d-flex">
      <label for="">Remise :</label>
      <input class="ml-2 form-control" name="remise" type="text" style="height: 20px;width:70px">
    </div>
    <div class="d-flex">
      <label for="">sous-total moins Remise :</label>
      <input class=" ml-2 form-control" name="sous_remise_total" type="text" style="height: 20px;width:70px">
    </div>
    <div class="d-flex">
      <label for="">TVA 20% :</label>
      <input class="ml-2 form-control" name="tva" type="text" style="height: 20px;width:70px">
    </div>
    <div class="d-flex">
      <label for="">Total TTC :</label>
      <input class="ml-2 form-control" name="totalttc" type="text" style="height: 20px;width:70px">
    </div>
    </div>
  
      <div class="mt-2 col-12 card text-center">
     </div>
    
     <div class="p-2 col-5 text-center d-flex flex-column">
      <label for="">enregistrer comme :</label>
      <select id="select" name="status" class="custom-select" id="">
        <option name="status" selected>selectionner</option>
        <option value="encours">encours</option>
        <option value="valide">valide</option>
        <option value="brouillant">brouillon</option>
      </select>
    </div>
  
    
    
    
    
    <div class="col-11 p-2 d-flex justify-content-end">
      
      <button class="btn btn-primary" name="submit" type="submit">Ajouter</button>
      <a class="ml-2 btn btn-secondary" href="{{ route('admin.devis') }}">Annuler</a>
    </div>
      
      <div class="card-body border border-info">
     <p class="card-text text-center">WIB DAY SARL <br>
       203 Boulevard Zerktouni, Casablanca, 20100 <br>
       RC : 302523 – IF : 15163323 – Patente : 35891715 – Tél : 0522 39 77 55</p>
     </div>
     <div class="card-footer bg-info">
    </div>
  </div>
      
  </form>  

</section>



@endsection