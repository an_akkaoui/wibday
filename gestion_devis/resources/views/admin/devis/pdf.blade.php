    <!DOCTYPE html>
        <html lang="en">
          <head>
            <meta charset="utf-8">
            <title>PDFs</title>
            <link rel="stylesheet"  media="all" />
          </head>
          <body>
            <script type="text/javascript">
              var route = "{{ url('autocomplete-search') }}";
      
              $('#search').typeahead({
                  source: function (query, process) {
                      return $.get(route, {
                          query: query
                      }, function (data) {
                          return process(data);
                      });
                  }
              });
          </script>
          <style>
          @font-face {
            font-family: SourceSansPro;
            src: url(SourceSansPro-Regular.ttf);
          }
          
          .clearfix:after {
            content: "";
            display: table;
            clear: both;
          }
          
          a {
            color: #0087C3;
            text-decoration: none;
          }
          
          body {
            position: relative;
            color: #000000;
            background-color: #77777742;
            font-family: Arial, sans-serif; 
            font-size: 14px; 
            font-family: SourceSansPro;
          }
          
          header {
            display: flex;
            justify-content: space-between;
            width: 100%;
            padding: 10px 0;
            margin-bottom: 20px;
            border-bottom: 1px solid #AAAAAA;
          }
          
          #logo {
            /* float: left; */
            margin-top: 8px;
          }
          
          #logo img {
            height: 70px;
          }
          
          #company {
            float: right;
            text-align: right;
          }
          
          
          #details {
            margin-bottom: 50px;
          }
          
          #client {
            padding-left: 6px;
            border-left: 6px solid #0087C3;
            float: left;
          }
          
          #client .to {
            color: #777777;
          }
          
          h2.name {
            font-size: 1.4em;
            font-weight: normal;
            margin: 0;
          }
          
          #invoice {
            float: right;
            text-align: right;
          }
          
          #invoice h1 {
            color: #0087C3;
            font-size: 2.4em;
            line-height: 1em;
            font-weight: normal;
            margin: 0  0 10px 0;
          }
          
          #invoice .date {
            font-size: 1.1em;
            color: #777777;
          }
          
          table {
           width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 20px;
          }
          
          table th{
            padding: 6px;
            background: #EEEEEE;
            text-align: center;
            border-bottom: 1px solid #FFFFFF;
          }
          table td {
            padding: 10px;
            background: #EEEEEE;
            text-align: center;
            border-bottom: 1px solid #FFFFFF;
          }
          
          table th {
            white-space: nowrap;        
            font-weight: normal;
          }
          
          table td {
            text-align: right;
          }
          
          table td h3{
            color: #57B223;
            font-size: 1.2em;
            font-weight: normal;
            margin: 0 0 0.2em 0;
          }
          
          table .no {
            color: #FFFFFF;
            font-size: 1.6em;
            background: #0e70b1;
          }
          
          table .desc {
            text-align: left;
          }
          
          table .unit {
            background: #DDDDDD;
          }
          
          table .total {
            background: #0e70b1;
            color: #FFFFFF;
          }
          
          table td.unit,
          table td.qty,
          table td.total {
            font-size: 1.2em;
          }
          
          table tbody tr:last-child td {
            border: none;
          }
          
          table tfoot td {
            padding: 10px 20px;
            background: #FFFFFF;
            border-bottom: none;
            font-size: 1.2em;
            white-space: nowrap; 
            border-top: 1px solid #AAAAAA; 
          }
          
          table tfoot tr:first-child td {
            border-top: none; 
          }
          
          table tfoot tr:last-child td {
            color: #0e70b1;
            font-size: 1.4em;
            border-top: 1px solid #0e70b1; 
          
          }
          
          table tfoot tr td:first-child {
            border: none;
          }
          
          #thanks{
            font-size: 2em;
            margin-bottom: 50px;
          }
          
          #notices{
            padding-left: 6px;
            border-left: 6px solid #0087C3;  
          }
          
          #notices .notice {
            font-size: 1.2em;
          }
          
          footer {
            color: #777777;
            width: 100%;
            height: 30px;

            border-top: 1px solid #AAAAAA;
            padding: 8px 0;
            text-align: center;
          }
          
          .numero{
            margin-top: 18px;
            
          }
          section{
             margin: auto;
             width: 100%;
             padding: 20px;
          }
          .pdf{
            width: 90%;
            margin: auto;
             background-color: #ffffff;
             border-radius: 15px;
             padding: 20px;
             margin-top: 12px;
             
          }

          .tous{
            width:80%;
            align-items: center;
          }
          .search{
            text-align: center;
                margin: auto;
               

          }
         
          
          </style>
          <section class="tous">
            <section class="search">
              <img src="/image/audit.png" width="50px" height="50px" alt="">
              <h5>Recherche PDF :</h5>
              <form action="{{ route('admin.devis.search') }}" method="GET">
                <div>
                <button><a href="{{ route('admin.devis') }}"><< devis </a></button>  
                  <input type="search" id="search" name="query" placeholder="Search" value="">
               <button type="submit">Rechercher</button>
              </div>
            </form>            
            </section>
            <section class="section">
              @foreach ($data as $data)
              <div class="pdf">
              <header>
               
                  <div id="logo">
                    <img src="/image/logo-Wib.png" style="width: 100%;
                    max-width: 159px;
                    height: 83px;" />
                  </div>
                  <div class="numero">
                    <div>Devis n° : {{ $data->numero }}</div>
                    <div>Date emission : {{ $data->date }}</div>
                  </div>
                 
                 </header>
                 <main>
                  <div id="details" class="clearfix">
                    <div id="client">
                      <div class="to">Contact: {{ $data->clients_contact }}</div>
                      <h2 class="name">Entreprise: {{ $data->clients_entreprise }}</h2>
                      <div class="address">Telehone : {{ $data->clients_phone }}</div>
                    </div>
                    <div style="float: right;">
                      <h1>Titre de Compagne : {{ $data->compagne }}</h1>
                    </div>
                  </div>
                  <table border="0" cellspacing="0" cellpadding="0">
                    <thead>
                      <tr>
                      <td class="no">#</td>
                        <th class="desc">DESCRIPTION</th>
                        <th class="unit">UNIT PRICE</th>
                        <th class="qty">QUANTITY</th>
                        <th class="total">TOTAL</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                      <td class="no">{{ $data->id }}</td>
                        <td class="desc">{{ $data->description }}</td>
                        <td class="unit">{{ $data->prix }}</td>
                        <td class="qty">{{ $data->quantite }}</td>
                        <td class="total">{{ $data->total }}</td>
                      </tr>
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colspan="2"></td>
                        <td colspan="2">Sous TOTAL</td>
                        <td>{{ $data->sous_total }}</td>
                      </tr>
                      <tr>
                        <td colspan="2"></td>
                        <td colspan="2">Remise</td>
                        <td>{{ $data->remise }}</td>
                      </tr>
                      <tr>
                        <td colspan="2"></td>
                        <td colspan="2">sous total moins Total</td>
                        <td>{{ $data->sous_remise_total }}</td>
                      </tr>
                      <tr>
                        <td colspan="2"></td>
                        <td colspan="2">TVA 20%</td>
                        <td>{{ $data->tva }}</td>
                      </tr>
                      <tr>
                        <td colspan="2"></td>
                        <td colspan="2">TOTAL TTC</td>
                        <td>{{ $data->totalttc }}</td>
                      </tr>
                    </tfoot>
                  </table>
                </main>
                <footer>
                  WIB DAY SARL <br>
                  203 Boulevard Zerktouni, Casablanca, 20100 <br>
                  RC : 302523 – IF : 15163323 – Patente : 35891715 – Tél : 0522 39 77 55
                </footer>
                
                
              </div>
              
              
            </div>
            <div style="text-align: center;width:100%">

              <button style="width: 100px;height:30px;margin-top:1rem"><a class="ml-3" href="{{ route('admin.devis.pdf', ['id' => $data->id ]) }}">Telecharger</a></button>
            </div>
            @endforeach
            
            
          </section>
            
          </section>
       
          
        </body>
        </html>
        