@extends('admin.dashboard')

@section('name')

<section class="p-3 col-lg-12">
<div class="panel-heading d-flex flex-row mt-4">
   <h4 class="text-info p-3">Modifier Devis:</h4> 
</div>
<div class="panel panel-default">
  <div class="panel-body">
    <form action="{{ route('devis.update',['id' => $stock->id])}}" method="POST"  enctype="multipart/form-data">
     {{ csrf_field() }}
     <div>
  <div class="d-flex flex-column">
    <label for="">Description :</label>
    <textarea name="description" rows="5" cols="50">{{ $stock->description }}</textarea>
  </div> 
  <div class="d-flex flex-column">
    <label for="">Quantité :</label>
    <input name='quantite' value="{{ $stock->quantite }}" type="text">
  </div>  
  <div class="d-flex flex-column">
    <label for="">Prix Unitaire :</label>
    <input name='prix' value="{{ $stock->prix }}" type="text">
  </div> 
  <div class="d-flex flex-column">
    <label for="">Total :</label>
    <input name='total' value="{{ $stock->total }}" type="text">
  </div>     
  <div class="p-2 col-5 text-center d-flex flex-column">
    <label for="">Modifier comme :</label>
    <select id="select" name="status" class="custom-select" id="">
      <option name="status" selected>selectionner</option>
      <option class="p-1"  style="background-color: rgba(33, 224, 119, 0.493)" value="valide">valide</option>
      <option class="p-1" style="background-color: rgba(255, 251, 0, 0.534)" value="encours">encours</option>
      <option class="p-1" style="background-color: rgba(141, 141, 138, 0.589)" value="brouillant">brouillon</option>
    </select>
  </div>
     </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info" >Modifier</button>
        <a type="button" class="btn btn-default" href="{{ route('admin.devis') }}">Close</a>
      </div>
  </div>
  
        </div>
      </div>
    </div>
  </div>
</div>
</form>

</section>
@endsection