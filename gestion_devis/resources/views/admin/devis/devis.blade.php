@extends('admin.dashboard')

@section('name')
<section class="p-3 col-lg-12">
<h4 class="text-info p-3">Devis :</h4>
         
        <table class="table table-bordered table-striped">
        <thead style="background-color: #68b1c5;height: 2px;
        line-height: 2px;">
          <tr class="col-12" style="height: 1px;
          line-height: 1px;">
            <th class="col-6"><p class="text-center text-dark">Description</p></th>
            <th class="col-1"><p class="text-center text-dark">Quantité</p></th>
            <th class="col-2"><p class="text-center text-dark">Prix Unitaire</p></th>
            <th class="col-1"><p class="text-center text-dark">Total</p></th>
            <th class="text-center text-dark">
                <a class="text-center text-dark" href="{{ route('creer.devis') }}"> <p class="text-center text-dark">Ajout</p><img src="/image/add.svg" width="20px" height="20px" alt=""></a>             
              </th>
              <th>
               <p class="text-center text-dark">Apercu</p> 
              </th>
              <th>
                <p class="text-center text-dark">status</p> 
               </th>
               <th class="col-2">
                <p class="text-center text-dark">bon de commande</p> 
               </th>
               

          </tr>
        </thead>
        @foreach ($personel as $personel)
        <tbody>
          <td>
                  {{ $personel->description }}
              </td>
              <td>
                  {{ $personel->quantite }}
               </td>
               <td>
                  {{ $personel->prix }}
               </td>
               <td>
                  {{ $personel->total }}
           </td>
           <td class="d-flex">
                    <a class="ml-3" href="{{ route('devis.edit', ['id' => $personel->id ]) }}"><img src="/image/edit.svg" width="20px" height="20px" alt=""></a>
                  <a class="ml-3" href="{{ route('devis.delete', ['id' => $personel->id ]) }}"><img src="/image/delete.svg" alt="" width="20px" height="20px"></a> 
                  <a class="ml-3" href="{{ route('admin.devis.pdf', ['id' => $personel->id ]) }}"><img src="/image/pdf-file.png" alt="" width="20px" height="20px"></a> 
                </td>
                
                <td>
                  <a class="ml-3" href="{{ route('admin.devis.apercu', ['id' => $personel->id ]) }}"><img src="/image/preview.png" width="20px" height="20px" alt=""></a>
                </td>
                <td>
                  @if ($personel->status == 'encours')
                  <div class="bg-warning p-1">
                  <p class="bg-warning text-white"> {{ $personel->status }} </p>
                  </div>
                      
                  @endif
                  @if ($personel->status == 'valide')
                  <div class="bg-success p-1">

                  <p class="text-white"> {{ $personel->status }} </p>
                  </div>
                      
                  @endif
                  @if ($personel->status == 'brouillant')
                  <div class="bg-secondary p-1">
                    <p class="text-white"> {{ $personel->status }} </p>
                  </div>
                      
                  @endif

                </td>
                <td>
                   @if ($personel->bon == '0')
                   <a class="text-center text-dark" href="{{ route('admin.biblio.ajouter', ['id' => $personel->id ]) }}"><img src="/image/add-file.png" width="20px" height="20px" alt=""> Ajouter Bon commande</a>
                   
                   @else 
                   {{ $personel->bon }}            
                      
                 
                   @endif

                </td>
              
                
        </tbody>
        @endforeach
      </table>  
      </div>

      

      

</section>



@endsection