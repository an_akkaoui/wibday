@extends('admin.dashboard')
@section('name')

<script>
    $(function () {
  $('[data-toggle="tooltip"]').tooltip();
})
var route = "{{ url('autocomplete-search') }}";
      
              $('#search').typeahead({
                  source: function (query, process) {
                      return $.get(route, {
                          query: query
                      }, function (data) {
                          return process(data);
                      });
                  }
              });
</script>

<style>
    .paginate{
   width: 100%;
    display: inline-block;
    margin: auto;
    width: 100%;
    margin-top: 34px;
    text-align: center;
    
 }
 
 .pas{
   width: 50%; 
   margin-top: 40px;
  margin: auto;
  text-align: center;

}
</style>

<section class="p-3 col-lg-12">
  
<div class="container bg-light mt-4 p-4" style="height: 100px">
    <h4 class="text-info p-3">Clients :</h4>
    <div class="d-flex justify-content-around">
      <a href="{{ route('clients.ajout') }}"><p>Nouveaux Client</p></a> 
      <a href="{{ route('admin.clients') }}"><p>Existant Client</p></a>
    </div>

</div>
<section class="p-2 mt-3">
    <h5 class="text-info p-2">Recherche :</h5>
    <section class="p-3 col-lg-12"> 
        <section class="search" style="text-align: center;
        margin: auto;">
            <img src="/image/audit.png" width="50px" height="50px" alt="">
              <h5>Recherche Clients :</h5>
              <form action="{{ route('admin.clients.search') }}" method="GET">
                <div class="d-flex justify-content-center p-1">
                    <a class="btn btn-info ml-2" href="{{ route('admin.clients') }}"><< clients </a>
                    <input type="search" class="form-control col-2 ml-2" id="search" name="query" placeholder="Search" value="">
                    <button type="submit" class="btn btn-outline-info ml-2" >Rechercher</button>
                </div>
            </form>
        </section>
    @if ((isset($personel)))
    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table table-striped mt-3">
                <thead class="bg-info text-center">
                    <th>
                        Nom de Contact 
                    </th>
                    <th>
                        Nom de l'entreprise
                    </th>
                    <th>
                        Email
                    </th>
                    <th>
                        Phone
                    </th>
                    <th>
                        ville
                    </th>
                    <th>
                        <a class="ml-3" href="{{ route('clients.ajout') }}"><img src="/image/add.svg" width="20px" height="20px" alt=""></a>
                    </th>
                    
                </thead>
                @if (count($personel) > 0)
                <tbody class="text-center"> 
                    
                    @foreach ($personel as $personels)
                <td>
                        {{ $personels->contact }}
                    </td>
                    <td>
                        {{ $personels->entreprise }}
                     </td>
                     <td>
                        {{ $personels->email }}
                     </td>
                     <td>
                        {{ $personels->phone }}
                 </td>
                 <td>
                    {{ $personels->ville }}
             </td>
                
             <td class="d-flex">
                <a class="ml-3" href="{{ route('clients.edit', ['id' => $personels->id ]) }}"><img src="/image/edit.svg" width="20px" height="20px" alt=""></a>
              <a class="ml-3" href="{{ route('clients.delete', ['id' => $personels->id ]) }}"><img src="/image/delete.svg" alt="" width="20px" height="20px"></a>  
            </td>
                </tr>
                    
            </tbody>
            
            
           </table>
           
        </div>
    </div>   
    @endforeach
    @else
    <div class="pas p-3">
        
        <h3>pas de resultats</h3>
    </div>
    @endif
    
    <div class="paginate">
        {{ $personel->links('admin.pagination') }}
        
    </div>
</section>
@endif


</section>   
@endsection
