@extends('admin.dashboard')

@section('name')
<section class="p-3 col-lg-12">
<h4 class="text-info p-3">Ajouter un client:</h4>
<div class="panel panel-default">
  <div class="panel-body mb-2">
    <form action="{{ route('clients.store') }}" method="POST">
      @csrf
      {{ csrf_field() }}
      <div class="d-flex justify-content-around">
        <div class="d-flex flex-column col-4">
          <label for="" >Nom de contact :</label>
          <input name='contact'  type="text">
        </div> 
        <div class="d-flex flex-column col-4">
          <label for="">Nom d'Entreprise :</label>
          <input name='entreprise'  type="text">
        </div> 
      </div>
       <div class="d-flex justify-content-around">
         <div class="d-flex flex-column col-4">
           <label for="">Email :</label>
           <input name='email'  type="text">
         </div>  
         <div class="d-flex flex-column col-4">
           <label for="">Phone :</label>
           <input name='phone' type="text">
         </div>
       </div>

      <div class="d-flex flex-column col-4 offset-1">
        <label for="">Ville :</label>
        <input name='ville' type="text">
      </div> 
        
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info" >Ajouter</button>
        <a type="button" class="btn btn-default" href="{{ route('admin.clients') }}">Close</a>
      </div>
        </div>
    
</form>
</section>
@endsection


