@extends('admin.dashboard')

@section('name')
<section class="p-3 col-lg-12">
<div class="panel-heading d-flex flex-row mt-4">
   <h4 class="text-info p-3">Modifier Devis:</h4> 
</div>
<div class="panel panel-default">
  <div class="panel-body">
    <form action="{{ route('clients.update',['id' => $stock->id])}}" method="POST"  enctype="multipart/form-data">
     {{ csrf_field() }}
     <div>
  <div class="d-flex flex-column">
    <label for="">Nom de Contact :</label>
    <input name='contact' value="{{ $stock->contact }}" type="text">
  </div> 
  <div class="d-flex flex-column">
    <label for="">Nom de l'entreprise :</label>
    <input name='entreprise' value="{{ $stock->entreprise }}" type="text">
  </div>  
  <div class="d-flex flex-column">
    <label for="">Email :</label>
    <input name='email' value="{{ $stock->email }}" type="text">
  </div> 
  <div class="d-flex flex-column">
    <label for="">Phone :</label>
    <input name='phone' value="{{ $stock->phone }}" type="text">
  </div>  
  <div class="d-flex flex-column">
    <label for="">Ville :</label>
    <input name='ville' value="{{ $stock->ville }}" type="text">
  </div>   
     </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info" >Modifier</button>
        <a type="button" class="btn btn-default" href="{{ route('admin.clients') }}">Close</a>
      </div>
  </div>
  
        </div>
      </div>
    </div>
  </div>
</div>
</form>
</section>
@endsection