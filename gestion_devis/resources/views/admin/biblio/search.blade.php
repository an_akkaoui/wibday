@extends('admin.dashboard')
@section('name')

<script type="text/javascript">
  var route = "{{ url('autocomplete-search') }}";

  $('#search').typeahead({
      source: function (query, process) {
          return $.get(route, {
              query: query
          }, function (data) {
              return process(data);
          });
      }
  });
</script>
<style>

 .paginate{
   width: 100%;
    display: inline-block;
    margin: auto;
    width: 100%;
    margin-top: 34px;
    text-align: center;
    
 }
 .pas{
   width: 50%; 
  margin: auto;
  text-align: center;

}
</style>

<section class="p-3 col-lg-12">
  <h5 class="text-info p-2">Recherche :</h5>
  <section class="search" style="text-align: center;
  margin: auto;">
      <img src="/image/audit.png" width="50px" height="50px" alt="">
        <h5>Recherche Devis :</h5>
        <form action="{{ route('admin.biblio.search') }}" method="GET">
          <div class="d-flex justify-content-center p-1">
              <a class="btn btn-info ml-2" href="{{ route('admin.devis') }}"><< devis </a>
              <input type="search" class="form-control col-2 ml-2" id="search" name="query" placeholder="Search" value="">
              <button type="submit" class="btn btn-outline-info ml-2" >Rechercher</button>
          </div>
      </form>
  </section>
  @if ((isset($devis)))
      @if (count($devis) > 0)
  @foreach ($devis as $devi)
    <div class="container p-4 mt-2 d-flex flex-column" style="border:1px solid black">

       <div class="card">
        @if ($devi->status == 'encours')
        <div class="p-1" style="background-color: rgba(255, 255, 0, 0.329)">
        <h5 class="text-dark"> {{ $devi->status }} </h5>
        </div>
            
        @endif
        @if ($devi->status == 'valide')
        <div class="p-1" style="background-color: rgba(12, 190, 71, 0.329)">
        <h5 class="text-success"> {{ $devi->status }} </h5>
        </div>
            
        @endif
        @if ($devi->status == 'brouillant')
        <div class="p-1" style="background-color: rgba(116, 116, 111, 0.329)">
          <h5 class="text-secondary"> {{ $devi->status }} </h5>
        </div>
        @endif 
       </div>

       <div class="d-flex justify-content-around p-3">

         <div class="col-5 d-flex flex-column p-3">
           <h6>{{ $devi->clients_contact }}</h6>
           <h6>{{ $devi->clients_entreprise }}</h6>
           <h6>{{ $devi->clients_phone }}</h6>
         </div>
  
         <div class="col-5 d-flex flex-column p-3">
           <div class="d-flex">
            <label for="">Devi n° :</label>
            <input class="col-6 ml-2 inputfield" name="numero" value="{{ $devi->numero }}" type="text" style="height: 20px" disabled>
          </div>
          <div class="d-flex">
            <label for="">Date d'emission :</label>
            <input class="col-6 ml-2 form-control" name="date" value="{{ $devi->date }}" type="date" style="height: 20px" disabled>
          </div>
         </div>
       </div>

       <div class="d-flex justify-content-center p-3">
        <table class="table table-bordred col-lg-10 text-center">
          <thead class="bg-info">
            <tr>
              <th scope="col">Description</th>
              <th class="col-2">Quantité</th>
              <th class="col-2">Prix Unitaire</th>
              <th class="col-2">Total</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th ><textarea name="description" rows="1" cols="50" disabled>{{ $devi->description }}</textarea></th>
              <td><input class="col-12" type="text" value="{{ $devi->quantite }}" name="quantite" disabled></td>
              <td><input class="col-12" type="text" value="{{ $devi->prix }}" name="prix" disabled></td>
              <td><input class="col-12" type="text" value="{{ $devi->total }}" name="total" disabled></td>
            </tr>
          </tbody>
        </table>  
        </div>
      
        <div class="col-11 mt-2 d-flex align-items-end flex-column">
        <div class="d-flex">
          <label for="">sous-total :</label>
          <input class=" ml-2 form-control" name="sous_total" value="{{ $devi->sous_total }}" type="text" style="height: 20px;width:70px" disabled>
        </div>
        <div class="d-flex">
          <label for="">Remise :</label>
          <input class="ml-2 form-control" name="remise" type="text" value="{{ $devi->remise }}" style="height: 20px;width:70px" disabled>
        </div>
        <div class="d-flex">
          <label for="">sous-total moins Remise :</label>
          <input class=" ml-2 form-control" name="sous_remise_total" value="{{ $devi->sous_remise_total }}" type="text" style="height: 20px;width:70px" disabled>
        </div>
        <div class="d-flex">
          <label for="">TVA 20% :</label>
          <input class="ml-2 form-control" name="tva" type="text" value="{{ $devi->tva }}" style="height: 20px;width:70px" disabled>
        </div>
        <div class="d-flex">
          <label for="">Total TTC :</label>
          <input class="ml-2 form-control" name="totalttc" type="text" value="{{ $devi->totalttc }}" style="height: 20px;width:70px" disabled>
        </div>
        </div>
        
        
        @if ($devi->status == 'encours')
        <div class="text-center p-2" style="margin-bottom: 8px">
          <a class="text-center text-dark" href="{{ route('admin.biblio.ajouter', ['id' => $devi->id ]) }}"><img src="/image/add-file.png" width="20px" height="20px" alt=""> Ajouter Bon commande</a>
         
        </div>
        @endif
        @if ($devi->status == 'brouillant')
        <div class="text-center p-2" style="margin-bottom: 8px">      
          <a class="text-center text-dark" href="{{ route('admin.biblio.ajouter', ['id' => $devi->id ]) }}"><img src="/image/add-file.png" width="20px" height="20px" alt=""> Ajouter Bon commande</a>
        </div>
        @endif
        @if ($devi->status == 'valide')
        <div class="p-3 d-flex justify-content-center col-12 text-center">
          <label for="avatar">Modifier:</label>
          <a class="ml-3" href="{{ route('devis.edit', ['id' => $devi->id ]) }}"><img src="/image/edit.svg" width="20px" height="20px" alt=""></a>
        </div>
        @endif

   
      </div>
      @endforeach

      @else
    <div class="pas p-3">  
        <h3>pas de resultats</h3>
    </div>

    @endif
    
    <div class="paginate">
        {{ $devis->links('admin.pagination') }}
        
    </div>
    @endif
  </section>
@endsection