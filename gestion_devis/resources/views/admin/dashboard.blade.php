@extends('layouts.app')
@section('content')

<section>
  <style>
    nav{
      z-index: 1;
    }
    .navbar-nav.navbar-center {
    position: absolute;
    left: 50%;
    transform: translatex(-50%);
}
#li{
  position: absolute;
    left: 80%;
    transform: translatex(-80%);
}
li {
    list-style: none;
}
.w3-sidebar{
  height:100%;
  width:200px;
  background-color:#fff;
  position:fixed!important;
  z-index:1;
  overflow:auto
}
.w3-bar-item{
  width:100%;
  display:block;
  background-color: #17a3b8e0;
  padding:8px 16px;
  text-align:center;
  border:none;
  white-space:normal;
  float:none;
  outline:0
}
  </style>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <div class="navbar-header">
        <a class="navbar-brand" href="#"><img class="ml-3" src="/image/logowib.png" width="150px" height="65px" alt=""></a>
      </div>
            
            <div class="collapse navbar-collapse">
              <ul class="nav navbar-nav navbar-center mr-auto mt-2">
               
                <form class="form-inline my-2 my-lg-0 float-right">
                  <input class="form-control mr-sm-2" style="width:150px; height:25px" type="search" placeholder="Search" aria-label="Search">
                  <input class="btn btn-outline-primary p-0" style="width:60px;height:25px;font-size:14px;" type="submit" value="Search">
                </form>
              </ul>
                <li class="nav-item dropdown" id="li">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown">
                    <img src="/image/user.png" width="20px" height="20px" alt="">
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('admin.profile') }}">Profil</a>
                    <a class="dropdown-item" href="{{ url('logout') }}">Deconnexion</a>
                  </div>
                </li>
            </div>

   
</nav>


</section>
    
      <div class="w3-sidebar w3-light-grey w3-bar-block" style="width:10%">
        <h3 class="w3-bar-item text-white">Gestion devis</h3>
        <a href="{{ route('admin.dashboard') }}" class="w3-bar-item w3-button text-white"><img class="" src="/image/dashboard.svg" width="15px" height="15px" alt=""> Dashboard</a>
        <a href="{{ route('admin.clients') }}" class="w3-bar-item w3-button text-white"><img class="" src="/image/customer.svg" width="15px" height="15px" alt=""> Clients</a>
        <a href="{{ route('admin.devis') }}" class="w3-bar-item w3-button text-white"><img class="" src="/image/documents.png" width="20px" height="20px" alt=""> Devis</a>

        <a href="{{ route('admin.employee') }}" class="w3-bar-item w3-button text-white"><img class="" src="/image/employee.svg" width="20px" height="20px" alt=""> Employees</a>
        <a href="{{route('admin.chat')}} " class="w3-bar-item w3-button text-white"><img class="" src="/image/mail.png" width="15px" height="15px" alt=""> Inbox</a>
        <a href="{{ route('admin.biblio') }}" class="w3-bar-item w3-button text-white"><img class=""  src="/image/book.png"  width="20px" height="20px" alt=""> bibliothèque</a>
        <a href="{{ url('logout') }}" class="w3-bar-item w3-button text-white"><img class="" src="/image/logout.svg" width="17px" height="17px" alt=""> Deconnexion</a>
  
      </div>
        
        
       
      
       <main class="" style="background-color: #cfcfcf2c;margin-left:10%">
        <div class="p-3">
          @include('admin.flash-messages')
        </div>
         @yield('name')
       </main>
        

    

      
   
   

@endsection

