<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'libphonenumber' => array($vendorDir . '/giggsey/libphonenumber-for-php/src'),
    'Svg\\' => array($vendorDir . '/phenx/php-svg-lib/src'),
    'Snowplow\\RefererParser' => array($vendorDir . '/snowplow/referer-parser/php/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log'),
    'PayPal' => array($vendorDir . '/paypal/rest-api-sdk-php/lib'),
    'HTMLPurifier' => array($vendorDir . '/ezyang/htmlpurifier/library'),
    'FontLib\\' => array($vendorDir . '/phenx/php-font-lib/src'),
    'Diff' => array($vendorDir . '/phpspec/php-diff/lib'),
    'Cron' => array($vendorDir . '/mtdowling/cron-expression/src'),
);
