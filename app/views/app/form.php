<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\helpers\UrlHelper;

/* @var $this yii\web\View */
/* @var $formModel app\models\Form */
/* @var $formDataModel app\models\FormData */
/* @var $showTheme boolean */
/* @var $showBox boolean */
/* @var $customJS boolean */

// Home URL
$homeUrl = Url::home(true);

// Base URL without schema
$baseUrl = UrlHelper::removeScheme($homeUrl);

$this->title = $formModel->name;


// Add body background to show box design
/*if ($showBox) {
    $this->registerCss("body { background-color: #EFF3F6; }");
} else {
    // Add theme
    if ($showTheme && isset($formModel->theme, $formModel->theme->css) && !empty($formModel->theme->css)) {
        $this->registerCss($formModel->theme->css);
    }
}*/


// Code By Eysf

if (isset($formModel->theme->color)){
    $colorBody = $formModel->theme->color;
    $this->registerCss("body { background-color: $colorBody; }");
}

if (isset($formModel->theme->css))
    $this->registerCss($formModel->theme->css);
    

$background = '';
$backgroundd = '';
if (file_exists('/home/dayzenwib/public_html/app/uploads/'.$_GET['id'].'_photo_background.jpeg')) {   
    $background = 'style="background-image: url(\'https://app.wibday.com/app/uploads/'.$_GET['id'].'_photo_background.jpeg\');"';
    echo '<style>body { 
      background: url(https://app.wibday.com/app/uploads/'.$_GET['id'].'_photo_background.jpeg) no-repeat center center fixed; 
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;
    }</style>';
}

// End Code By Eysf

?>

<!-- MATERIAL DESIGN ICONIC FONT -->
<link rel="stylesheet" href="https://app.wibday.com/app/assets/form/fonts/material-design-iconic-font/css/material-design-iconic-font.min.css">

<!-- STYLE CSS -->
<link rel="stylesheet" href="https://app.wibday.com/app/assets/form/css/style.css">

<div class="wrapper" <?= $backgroundd; ?>>
    <div class="row">
    	<div class="inner">
    		<div class="image-holder">
    			<img src="https://wibday.com/app/uploads/<?= $_GET['id'];  ?>.jpeg" style="width:100%" />
    		</div>
    		
    		<?php if ($showBox) : ?>
    			<div class="form-view">
                    <div class="panel panel-default">
                        <div class="panel-body" style="padding: 20px">
                            <!-- Form App Code -->
                            <div id="c<?= $formModel->id ?>">
                                <?= Yii::t('app', 'Fill out my') ?> <a href="<?= Url::to(['app/form', 'id' => $formModel->id], true) ?>"><?= Yii::t('app', 'online form') ?></a>.
                            </div>
                            <script type="text/javascript">
                                (function(d, t) {
                                    var s = d.createElement(t), options = {
                                        'id': <?= $formModel->id ?>,
                                        'theme': <?= $showTheme ?>,
                                        'customJS': <?= $customJS ?>,
                                        'container': 'c<?= $formModel->id ?>',
                                        'height': '<?= $formDataModel->height ?>px',
                                        'form': '<?= UrlHelper::removeScheme(Url::to(['/app/embed'], true)) ?>'
                                    };
                                    s.type= 'text/javascript';
                                    s.src = '<?= Url::to('@web/static_files/js/form.widget.js', true) ?>';
                                    s.onload = s.onreadystatechange = function() {
                                        var rs = this.readyState; if (rs) if (rs != 'complete') if (rs != 'loaded') return;
                                        try { (new EasyForms()).initialize(options).display() } catch (e) { }
                                    };
                                    var scr = d.getElementsByTagName(t)[0], par = scr.parentNode; par.insertBefore(s, scr);
                                })(document, 'script');
                            </script>
                            <!-- End Form App Code -->
                        </div>
                    </div>
                </div>
            <?php else : ?>
            	<div id="c<?= $formModel->id ?>">
                    Fill out my <a href="#">online form</a>.
                </div>
                <script type="text/javascript">
                    (function(d, t) {
                        var s = d.createElement(t), options = {
                            'id': <?= $formModel->id ?>,
                            'theme': <?= $showTheme ?>,
                            'customJS': <?= $customJS ?>,
                            'container': 'c<?= $formModel->id ?>',
                            'height': '<?= $formDataModel->height ?>px',
                            'form': '<?= UrlHelper::removeScheme(Url::to(['/app/embed'], true)) ?>'
                        };
                        s.type= 'text/javascript';
                        s.src = '<?= Url::to('@web/static_files/js/form.widget.js', true) ?>';
                        s.onload = s.onreadystatechange = function() {
                            var rs = this.readyState; if (rs) if (rs != 'complete') if (rs != 'loaded') return;
                            try { (new EasyForms()).initialize(options).display() } catch (e) { }
                        };
                        var scr = d.getElementsByTagName(t)[0], par = scr.parentNode; par.insertBefore(s, scr);
                    })(document, 'script');
                </script>
                <!-- End Form App Code -->
            <?php endif; ?>
    	</div>
	</div>
    <div class="row">	
        <footer>
            <div class="container">
                <div class="row">
                </div>
                <div class="row">
                    <div class="col-sm-12 wow fadeIn">
                        <div class="footer-border"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-7 footer-copyright wow fadeIn" style="width: calc(100% - 30px);">
                        <p style="text-align: center;">© Copyright <a href="http://app.wibday.com">Wib Day</a> | Tous Les Droits Résérvés.</p>
                    </div>
                    
                </div>
            </div>
        </footer>
    </div>
</div>
<style>

    @media only screen and (max-width: 767px){
        
        .form-view {
            width: 100% !important;
            padding-right: 5px !important;
            padding-left: 5px !important;
        }
        .panel-body{
            padding: 0px !important;
        }
                
    }
   	p.line {
	    clear: both;
	    overflow: hidden;
	}
	        
	footer {
	    /*margin: 50px -20px -20px !important;*/
	    padding-bottom: 10px;
	    background: #333;
	    color: #fff;
	    padding-top: 14px;
	    display: contents;
	}
	
	.form-view {
        padding-top: 10px !important;
        padding-left: 25px;
        padding-right: 25px;
    }
    .panel{
        border-radius: 0px !important;
        -webkit-box-shadow: 0 0px 0px rgb(255, 255, 255) !important;
        box-shadow: 0 0px 0px rgb(255, 255, 255) !important;
    }
    
    .inner{
        max-width: 985px !important;
    }
    .form-view {
        width: 60%;
    }
</style>