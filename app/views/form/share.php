<?php

use yii\helpers\Html;
use kartik\sidenav\SideNav;

/* @var $this yii\web\View */
/* @var $formModel app\models\Form */
/* @var $formDataModel app\models\FormData */

$this->title = $formModel->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Forms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $formModel->name, 'url' => ['view', 'id' => $formModel->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Embed or Share');

?>
<div class="share-page">

    <div class="row">
        <div class="col-sm-3">
            <?php
            echo SideNav::widget([
                'type' => SideNav::TYPE_DEFAULT,
                'heading' => '<i class="glyphicon glyphicon-share-alt"></i> ' . Yii::t('app', 'Embed or Share'),
                'iconPrefix' => 'glyphicon glyphicon-',
                'items' => [
                    [
                        'url' => null,
                        'label' => Yii::t("app", "Embed Full Form"),
                        'icon' => 'embed',
                        'active' => true,
                        'options' => [
                            'id' => 'showEmbed',
                        ]
                    ],
                    [
                        'url' => null,
                        'label' => Yii::t("app", "Share Form Link"),
                        'icon' => 'paired',
                        'options' => [
                            'id' => 'showLink',
                        ]
                    ],
                ],
                'options' => [
                    'id' => 'sideNav',
                ]
            ]);
            ?>
        </div>
     
    </div>

	<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title"><?= Yii::t("app", "Link to your Form") ?></h3>
    </div>
    <div class="panel-body">
        <form id="showForm">
           
            <div class="input-group">
                <input type="url" id="formUrl" class="form-control" value="<?= \yii\helpers\Url::to([
                    'app/form',
                    'id' => $formModel->id
                ], true); ?>" onfocus="this.select();" onmouseup="return false;">
                <span class="input-group-btn">
                    <input type="submit" class="btn btn-info" value="<?= Yii::t('app', 'Go!') ?>">
                </span>
            </div><!-- /input-group -->
        </form>
    </div>
    <div class="panel-footer">
        <p class="hint-block">
        </p>
    </div>
</div>

</div>

<?php
$script = <<< JS
$( document ).ready(function(){
    $('#showEmbed').find('a').on('click', function(e) {
        e.preventDefault();
        $('#embed').show();
        $('#link').hide();
        $('#showEmbed').addClass('active');
        $('#showLink').removeClass('active');
    });
    $('#showLink').find('a').on('click', function(e) {
        e.preventDefault();
        $('#embed').hide();
        $('#link').show();
        $('#showEmbed').removeClass('active');
        $('#showLink').addClass('active');
    });
    $('#withoutDesign').change(function() {
        if($(this).is(":checked")) {
            $('#formUrl').val($('#formUrl').val() + '&t=0');
        } else {
            $('#formUrl').val($('#formUrl').val().replace('&t=0', ''));
        }
    });
    $('#withoutBox').change(function() {
        if($(this).is(":checked")) {
            $('#formUrl').val($('#formUrl').val() + '&b=0');
        } else {
            $('#formUrl').val($('#formUrl').val().replace('&b=0', ''));
        }
    });
    $('#withoutCustomJS').change(function() {
        if($(this).is(":checked")) {
            $('#formUrl').val($('#formUrl').val() + '&js=0');
        } else {
            $('#formUrl').val($('#formUrl').val().replace('&js=0', ''));
        }
    });
    $('#showForm').on('submit', function(e) {
        e.preventDefault();
        window.open($('#formUrl').val());
    });

    $('#withoutBoxAlt').change(function() {
        if($(this).is(":checked")) {
            $('#formUrlAlt').val($('#formUrlAlt').val() + '/0');
        } else {
            $('#formUrlAlt').val($('#formUrlAlt').val().slice(0,-2));
        }
    });
    $('#showFormAlt').on('submit', function(e) {
        e.preventDefault();
        window.open($('#formUrlAlt').val());
    });
})

JS;
$this->registerJs($script, $this::POS_END);
?>