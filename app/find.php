<html lang="en">
    <head>
        <title>
            Bootstrap Validation example using validator.js
        </title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"></link>
        <script src="https://code.jquery.com/jquery-1.12.4.js">
        </script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js">
        </script>
    </head>
    <body>
        <br/>
        <div class="container">
            <div class="panel panel-primary" style="width:100%;margin:0px auto">


              <div class="panel-heading"> Find word in folder </div>
              <div class="panel-body">


                <form data-toggle="validator" method="GET" role="form">


                  <div class="form-group">
                      <label class="control-label" for="inputName">Kalma</label>
                      <input class="form-control" name="word" data-error="Please enter name field." id="inputName" placeholder="Name"  type="text" value="<?php echo $_GET['word']; ?>" required />
                      <div class="help-block with-errors"></div>
                  </div>


                  <div class="form-group">
                    <label for="inputName" class="control-label">EXT</label>
                    <input type="text" name="ext" class="form-control" id="inputName" data-error="Please enter Extension field." placeholder="Extension" value="<?php echo $_GET['ext']; ?>" required>
                    <div class="help-block with-errors"></div>
                  </div>


                  <div class="form-group">
                    <label for="inputName" class="control-label">FOLDER</label>
                    <div class="form-group">
                      <input type="text" name="folder" class="form-control" id="inputName" data-error="Please enter folder field." placeholder="Name folder" value="<?php echo $_GET['folder']; ?>" required>
                      <div class="help-block with-errors"></div>
                    </div>
                  </div>


                  <div class="form-group">
                      <button class="btn btn-primary" type="submit">
                          Submit
                      </button>
                  </div>
				  

				<?php

					if(isset($_GET['ext']))
					{
						echo '<div class="form-group" style="height: 300px;background: #000;color: #fff;overflow: auto;">';
						$currentFile = __FILE__;
						$searchDir = str_replace('find.php','',$currentFile).$_GET['folder'];
						$searchExtList = array('.'.$_GET['ext']);
						$searchString = $_GET['word'];
						// echo $searchDir; die();
						$allFiles = everythingFrom($searchDir,$searchExtList,$searchString);
						echo '';
						foreach($allFiles as $name){
							$sRegExp = '/\b'.$searchString.'\b/i';
							foreach (glob($name) as $sFile){
								foreach (file($sFile) as $nLineNumber => $sLine){
									if (preg_match($sRegExp, $sLine) == 1){
										echo 'haya <b>'.$searchString.'</b> l9inaha f had chemin <span style="color: red">'.$name.'</span>, ster <b>'.$nLineNumber.'</b><br />';
									}
								}
							}
							echo '';
							// echo 'haya <b>'.$searchString.'</b> l9inaha hna '.$name.'<br />';
						}
						//var_dump($allFiles);
						echo '</div>';
					}
									  
				?>			  

					
                </form>


              </div>
            </div>
        </div>
    </body>
</html>

<!--<form method="GET" >
kalma :
<input type="text" name="word" value="<?php echo $_GET['word']; ?>" />
ext :
<input type="text" name="ext" value="<?php echo $_GET['ext']; ?>" />
folder :
<input type="text" name="folder" value="<?php echo $_GET['folder']; ?>" />
<input type="submit" />
</form>-->
<?php

/* if(isset($_GET['ext']))
{
	$currentFile = __FILE__;
	$searchDir = str_replace('find.php','',$currentFile).$_GET['folder'];
	$searchExtList = array('.'.$_GET['ext']);
	$searchString = $_GET['word'];
	// echo $searchDir; die();
	$allFiles = everythingFrom($searchDir,$searchExtList,$searchString);
	foreach($allFiles as $name){
		$sRegExp = '/\b'.$searchString.'\b/i';
		foreach (glob($name) as $sFile){
			foreach (file($sFile) as $nLineNumber => $sLine){
				if (preg_match($sRegExp, $sLine) == 1){

					printf('<br/>haya <b>%s</b> l9inaha f had chemin %s, ster <b>%d</b>', $searchString, $name, $nLineNumber);

				}
			}
		}
		// echo 'haya <b>'.$searchString.'</b> l9inaha hna '.$name.'<br />';
	}
	//var_dump($allFiles);
} */


function everythingFrom($baseDir,$extList,$searchStr) {
    $ob = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($baseDir), RecursiveIteratorIterator::SELF_FIRST);
    foreach($ob as $name => $object){
        if (is_file($name)) {
            foreach($extList as $k => $ext) {
                if (substr($name,(strlen($ext) * -1)) == $ext) {
                    $tmp = file_get_contents($name);
                    if (strpos($tmp,$searchStr) !== false) {
                        $files[] = $name;
                    }
                }
            }
        }
    }
    return $files;
}

?>