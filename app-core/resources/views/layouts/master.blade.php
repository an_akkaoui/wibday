<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="{{ setting('site.description') }}">
<meta name="keywords" content="emailing, e-mailing, email, e-mail, envoi, envoyer, routeur, routage, en masse, mailing, mailing-list, gratuit, pas cher, solution,marketing, e-marketing, webmarketing, wib day, Wib Day, cible, emailing maroc - mailing maroc, email marketing maroc, routage emailing, newsletter au maroc, solution emailing, Plateforme Emailing, plateforme emailing, Location bases de données, Location bases de données emails, newsletter, campagne emailing, booster, ciblage, email newsletter, bdd, email campagne, agence communication, agence digital, agence email, agence emailing, agence emailing maroc, email maroc, bdd maroc, collect de leads, géneration de leads, leads, lead">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<style>
.slider-wrap1 {
position: relative;
margin: 80px auto;
width: 70%;
background-color:#fff;
margin-top:-50px;}
.slider-wrap1 a{
color:#fff;
}
.slider1 {
position: relative;
width: 90%;
margin: auto;

}
.logoclt ul {
margin: 0;
padding: 0;

}
.logoclt ul li {
list-style: none;
text-align: center;
}
.logoclt img{
	padding-left:55px;
}
.logoclt ul li span {
display: inline-block;
vertical-align: middle;
width: 200px;
height: 200px;
background: black;
}
.slider-arrow1 {
position: absolute;
top: 40px;
width: 20px;
height: 20px;
background: #1d90b9;
color: #fff;
text-align: center;
text-decoration: none;
border-radius: 50%;
}
.sa-left1 {
left: 10px;
}
.sa-right1 {
right: 10px;
}

</style>






    <!-- Title -->
    <title>{{ setting('site.title') }}</title>

    <!-- Favicon -->
    <link rel="icon" href="{{url('/')}}/img/core-img/favicon.png">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="{{url('/')}}/style.css">

    <script src="https://www.google.com/recaptcha/api.js"></script>
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- /Preloader -->

    <!-- Header Area Start -->
    <header class="header-area">
        <!-- Top Header Area Start -->
        <div class="top-header-area">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-5">
<!--                        <div class="top-header-content">
                            <p>Welcome to hair salon!</p>
                        </div>-->
                    </div>
                    <div class="col-7">
                        <div class="top-header-content text-right">
                     
                            <p> Suivez-nous  :<span class="mx-2"></span>  <a href="https://www.facebook.com/WibDay.Maroc" target="_blank"><i class="fa fa-facebook"></i></a><span class="mx-2"></span>  
                            <a href="https://www.linkedin.com/company/wibday/?viewAsMember=true" target="_blank">
                            <i class="fa fa-linkedin"></i>
                            </a>
                            <span class="mx-2"></span> 
                            <a href="mailto:info@wibday.com" target="_blank">
							<i class="fa fa-envelope"></i>
                            </a>
                            <span class="mx-2"></span> | <span class="mx-2"></span>  
                            <i class="fa fa-phone" aria-hidden="true"></i> : 
                            <a href="tel:+212684101010">(+212) 684 10 10 10</a>
                            
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Top Header Area End -->

        <!-- Main Header Start -->
        <div class="main-header-area">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Classy Menu -->
                    <nav class="classy-navbar justify-content-between" id="akameNav">

                        <!-- Logo -->
                        <a class="nav-brand" href="{{ route('home.view') }}"><img src="{{url('/')}}/assets/img/logo.png" alt="Wib Day - Email Marketing"></a>

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">
                            <!-- Menu Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>
                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul id="nav">
                                    <li @if(Route::currentRouteName() === 'home.view') class="active" @endif><a href="{{ route('home.view') }}">Accueil</a></li>
                                    <li @if(Route::currentRouteName() === 'agence.view') class="active" @endif><a href="{{ route('agence.view') }}">Notre Agence</a></li>
                                    <li @if(Route::currentRouteName() === 'service.view') class="active" @endif><a href="{{ route('service.view') }}">Nos Services</a></li>
                                    <li @if(Route::currentRouteName() === 'references.view') class="active" @endif><a href="{{ route('references.view') }}">Nos Références</a></li>
                                    <li @if(Route::currentRouteName() === 'contact.view') class="active" @endif><a href="{{ route('contact.view') }}">Contact</a></li>   
                                    <li @if(Route::currentRouteName() === 'blog-index.view' || Route::currentRouteName() === 'blog-article.view') class="active" @endif><a href="{{ route('blog-index.view') }}">Blog</a></li>                                   
<!--                                    <li><a href="#">Page</a>
                                        <ul class="dropdown">
                                            <li><a href="./index.html">- Home</a></li>
                                            <li><a href="./about.html">- About Us</a></li>
                                            <li><a href="./service.html">- Services</a></li>
                                            <li><a href="./portfolio.html">- Portfolio</a></li>
                                            <li><a href="./blog.html">- Blog</a></li>
                                            <li><a href="./single-blog.html">- Blog Details</a></li>
                                            <li><a href="./contact.html">- Contact</a></li>
                                            <li><a href="#">- Dropdown</a>
                                                <ul class="dropdown">
                                                    <li><a href="#">- Dropdown Item</a></li>
                                                    <li><a href="#">- Dropdown Item</a></li>
                                                    <li><a href="#">- Dropdown Item</a></li>
                                                    <li><a href="#">- Dropdown Item</a></li>
                                                </ul>
                                            </li>-->
                                        </ul>
                                    </li>
                                </ul>

                                <!-- Cart Icon -->
<!--                                <div class="cart-icon ml-5 mt-4 mt-lg-0">
                                    <a href="#"><i class="icon_cart"></i></a>
                                </div>-->

                                <!-- Book Icon -->
                                <div class="book-now-btn ml-5 mt-4 mt-lg-0 ml-md-4">
                                    <a href="https://app.wibday.com/app/form?id=21" class="btn akame-btn" target="_blank">Inscrivez-vous</a>
                                </div>
                            </div>
                            <!-- Nav End -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header Area End -->
    @yield('content')
<!--
    <div id="jquery-script-menu">
<div class="jquery-script-center">

<div class="jquery-script-ads"><script type="text/javascript"><!--
google_ad_client = "ca-pub-2783044520727903";
/* jQuery_demo */
google_ad_slot = "2780937993";
google_ad_width = 728;
google_ad_height = 90;
//-->
<!--</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></div>
<div class="jquery-script-clear"></div>
</div>
</div>

<div class="slider-wrap1">
<div class="slider1">
<ul class="logoclt">
<li> <img src="img/core-img/mazagan.jpg"/> </li>
<li> <img src="img/core-img/adwebmaroc-1.jpg"/></li>
<li> <img src="img/core-img/avis-1.jpg"/></li>
<li> <img src="img/core-img/bmw.jpg"/> </li>
<li> <img src="img/core-img/carrefour-1.jpg"/> </li>
<li> <img src="img/core-img/chocodiva.jpg"/> </li>
<li> <img src="img/core-img/anfaplace-1.jpg"/></li>
<li> <img src="img/core-img/electroplanet-1.jpg"/> </li>
<li> <img src="img/core-img/ford-1.jpg"/> </li>
<li> <img src="img/core-img/hilton.jpg"/> </li>
<li> <img src="img/core-img/hitradio-1.jpg"/> </li>
<li> <img src="img/core-img/hyundai.jpg"/> </li>
<li> <img src="img/core-img/iga.jpg"/> </li>
<li> <img src="img/core-img/istikbal.jpg"/> </li>
<li> <img src="img/core-img/kasbahtamadout.jpg"/> </li>
<li> <img src="img/core-img/kia.jpg"/> </li>
<li> <img src="img/core-img/Logo-hrm-1.jpg"/> </li>
<li> <img src="img/core-img/london-academy-1.jpg"/> </li>
<li> <img src="img/core-img/macroisiere-1.jpg"/> </li>
<li> <img src="img/core-img/myschool-1.jpg"/> </li>
<li> <img src="img/core-img/palmeraie-1.jpg"/> </li>
<li> <img src="img/core-img/preventica-casa.jpg"/> </li>
<li> <img src="img/core-img/vizir center.png"/> </li>
<li> <img src="img/core-img/vtc.jpg"/> </li>
<li> <img src="img/core-img/widiane-1.jpg"/> </li>
</ul>
</div>
<a href="#" class="slider-arrow1 sa-left1">&lt;</a> <a href="#" class="slider-arrow1 sa-right1">&gt;</a> </div>
<script src="http://code.jquery.com/jquery-latest.min.js"></script> 
<script src="jquery.lbslider.js"></script> 
<script>
jQuery('.slider1').lbSlider({
    leftBtn: '.sa-left1',
    rightBtn: '.sa-right1',
    visible: 5,
    autoPlay: true,
    autoPlayDelay: 4
});
</script>






    <!-- Call To Action Area Start -->
    <section class="akame-cta-area bg-gray section-padding-80">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-10 col-xl-5">
                    <div class="cta-content">
                        <h2>Les spécialistes de l’Emailing</h2>
                        <p>Faites un tour d’horizon de l’ensemble des solutions proposées par Wib Day. </p>
                        <div class="akame-btn-group mt-30">
                            <a href="https://www.wibday.com/wibday.pdf" class="btn akame-btn active mr-3 mb-3 mb-sm-0">Kit Media</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>





        <!-- CTA Thumbnail -->
        <div class="cta-thumbnail bg-img" style="background-image: url(/img/bg-img/cta.png);"></div>
    </section>
    <!-- Call To Action Area End -->



          




    <!-- Footer Area Start -->
    <footer class="footer-area section-padding-80-0">
        <div class="container">
            <div class="row justify-content-between">

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-md-4">
                    <div class="single-footer-widget mb-80">
                        <!-- Footer Logo -->
                        <a href="#" class="footer-logo"><img src="{{url('/')}}/assets/img/logo.png" alt=""></a>

                        <p class="mb-30">Wib Day est une agence de conseil dans la réalisation des campagnes de marketing direct. L'agence vous accompagne dans la gestion de : collecte de données prospects et clients, gestion de bases de données, création d'opérations ciblées, analyse des retours et recommandations, optimisation de la vente</p>

                        <!-- Copywrite Text -->
                        <div class="copywrite-text">
                            <p>© Copyright Wib Day Tous Les Droits Résérvés </p>
                        </div>
                    </div>
                </div>

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-md-4 col-xl-3">
                    <div class="single-footer-widget mb-80">
                        <!-- Widget Title -->
                        <h4 class="widget-title">Rejoignez-nous</h4>

                        <!-- Open Times -->
                        <div class="open-time">
                            <p></p>

                        </div>

                        <!-- Social Info -->
                        <div class="social-info">
                            <a href="https://www.facebook.com/WibDay.Maroc" class="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="https://www.linkedin.com/company/wibday/?viewAsMember=true" class="twitter"><i class="fa fa-linkedin"></i></a>
                            <a href="mailto:info@wibday.com" class="google-plus"><i class="fa fa-envelope"></i></a>
                            <a href="tel:+212684101010" class="instagram"><i class="fa fa-phone"></i></a>
                        </div>
                    </div>
                </div>

                <!-- Single Footer Widget -->
                <div class="col-12 col-md-4 col-xl-3">
                    <div class="single-footer-widget mb-80">

                        <!-- Widget Title -->
                        <h4 class="widget-title">Wib Day</h4>

                        <!-- Contact Address -->
                        <div class="contact-address">
                            <p> <a href="https://wibday.com/note_legal.pdf">Note Légale</a> </p>
                            <p><a href="/contact">Rect. des données personnelles</a></p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </footer>
    <!-- Footer Area End -->

    <!-- All JS Files -->
    <!-- jQuery -->
    <script src="{{url('/')}}/js/jquery.min.js"></script>
    <!-- Popper -->
    <script src="{{url('/')}}/js/popper.min.js"></script>
    <!-- Bootstrap -->
    <script src="{{url('/')}}/js/bootstrap.min.js"></script>
    <!-- All Plugins -->
    <script src="{{url('/')}}/js/akame.bundle.js"></script>
    <!-- Active -->
    <script src="{{url('/')}}/js/default-assets/active.js"></script>

    @hasSection('scripts')
        @yield('scripts')
    @endif
</body>

</html>