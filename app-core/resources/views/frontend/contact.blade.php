@extends('layouts.master')
@section('content')
    
    <!-- Breadcrumb Area Start -->
    <section class="breadcrumb-area section-padding-80">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2>Contactez-nous</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i class="icon_house_alt"></i> Accueil</a></li>
                                <li class="breadcrumb-item active" aria-current="page">contact</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Area End -->

    <!-- Contact Information Area Start -->
    <section class="contact-information-area section-padding-80-0">
        <div class="container">
            <div class="row">
                <!-- Single Contact Information -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-contact-information mb-80">
                        <i class="icon_phone" style="color:#1d90b9;"></i>
                        <h4>Téléphone</h4>
                        <p>+212 684 10 10 10</p>
                    </div>
                </div>

                <!-- Single Contact Information -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-contact-information mb-80">
                        <i class="icon_pin" style="color:#1d90b9;"></i>
                        <h4>Address</h4>
                        <p>1, Angle rue Socrate et Abdou Taour 1er étage. Maarif Ext</p>
                    </div>
                </div>

                <!-- Single Contact Information -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-contact-information mb-80">
                        <i class="icon_clock" style="color:#1d90b9;"></i>
                        <h4>Horaire</h4>
                        <p>09:00 am to 17:00 pm</p>
                    </div>
                </div>

                <!-- Single Contact Information -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-contact-information mb-80">
                        <i class="icon_mail" style="color:#1d90b9;"></i>
                        <h4>Email</h4>
                        <p>info@wibday.com</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact Information Area End -->

    <!-- Contact Area Start -->
    <section class="akame-contact-area bg-gray section-padding-80">
        <div class="container">
            <div class="row">
                <!-- Section Heading -->
                <div class="col-12">
                    <div class="section-heading text-center">
                        <h2>Laissez un message</h2>
                        <p>Notre personnel vous rappellera plus tard et répondra à vos questions</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <!-- Form -->
                    <div class="screen-reader-response">
                              
                    </div>
                    <form action="contact.php" method="post" id="contact-form" class="contact-form akame-contact-form border-0 p-0" novalidate="novalidate">
                        <div class="row">
                            <div class="col-lg-6">
                                <input type="text" name="name" class="form-control mb-30" placeholder="Votre nom">
                            </div>
                            <div class="col-lg-6">
                                <input type="email" name="email" class="form-control mb-30" placeholder="Email">
                            </div>
                            
                            <div class="col-12">
                               <select name="subject" class="contact-subject form-control" id="contact-subject">
                               <option value="">Votre Demande</option>
                                <option value="Demande de devis">Demande de devis</option>
                                <option value="Modi.des données personnelles">Modifier Mon Email</option>
                                <option value="Se désabonner">Désabonnement</option>
                                <option value="autre">Autre</option>
	                        	</select>
                                  <div style="margin-top:15px;"></div>
                            </div>
                          
                            <div class="col-12">
                                <textarea name="message" class="form-control mb-30" placeholder="Votre message ..."></textarea>
                            </div>
                            
                            <div class="col-12">
                                <div class="g-recaptcha" data-sitekey="6LfkYKMZAAAAAM92zBbBracGGJZ7UMCohVbRyzFl"> </div>
                            </div>
                            <div class="col-12 text-center">
                                <button type="submit" class="btn akame-btn btn-3 mt-15 active">Envoyez</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact Area End -->
@endsection
   
    @section('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
        <style type="text/css">
        	.has-error input[type=password], .has-error input[type=text], .has-error input[type=tel], .has-error input[type=email], .has-error input[type=search], .has-error .error{
			    border-color: #e04f67;
			    color: #e04f67;
			}
			.required, .has-error{
			    float: right;
			    color: #c34343 !important;
			    border-color : #c34343 !important;
			    font-size: 10px;
			    font-weight: 600;
			}
        </style>
        <script type="text/javascript">
           


           // JavaScript Validation For Registration Page

           $('document').ready(function()
           {      
                 // name validation
                 var nameregex = /^[a-zA-Z ]+$/;
                  
                 $.validator.addMethod("validname", function( value, element ) {
                    return this.optional( element ) || nameregex.test( value );
                 }); 
                  
                 // valid email pattern
                 var eregex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                  
                 $.validator.addMethod("validemail", function( value, element ) {
                    return this.optional( element ) || eregex.test( value );
                 });
                  
                 $("#contact-form").validate({
                       
                   rules:
                   {
                       name: {
                          required: true,
                          validname: true,
                          minlength: 4
                       },
                       subject: {
                          required: true
                       },
                       message: {
                          required: true
                       },
                       email: {
                          required: true,
                          validemail: true
                       },
                       tel: {
                          required: true,
                          minlength: 10,
                          maxlength: 10
                       },
                    },
                    messages:
                    {
                       name: {
                          required: "S'il vous plaît entrez votre nom",
                          validname: "Le nom ne doit contenir que des alphabets et un espace",
                          minlength: "Votre nom est trop court"
                       },
                       subject: {
                          required: "S'il vous plaît entrez votre sujet"
                       },
                       tel: {
                          required: "Veuillez entrer votre numéro de téléphone",
                          //validname: "Veuillez ne pas saisir plus de 10 caractères.",
                          minlength: "Votre numéro de téléphone est trop court"
                       },
                       email: {
                            required: "Veuillez saisir votre adresse e-mail",
                            validemail: "Entrez une adresse e-mail valide"
                       }
                    },
                    errorPlacement : function(error, element) {
                      $(element).closest('.form-control').find('.help-block').html(error.html());
                    },
                    highlight : function(element) {
                      $(element).closest('.form-control').removeClass('has-success').addClass('has-error');
                    },
                    unhighlight: function(element, errorClass, validClass) {
                      $(element).closest('.form-control').removeClass('has-error').addClass('has-success');
                      $(element).closest('.form-control').find('.help-block').html('');
                    },
                    
                    submitHandler: function(form) {
                               //form.submit();
                       console.log($("#contact-form").serialize()); 
                       // var data = $("#kt_form_1").serialize();
                       $.ajax({
                          url: $( "#contact-form" ).attr("action"),
                          type: "POST",
                          data:  $("#contact-form").serialize(),
                          dataType: 'text',
                          processData: false,
                          success: function(response) { 
                             if(response=="error"){
                                // alert('error');
                                $( '.screen-reader-response' ).append('<div class="alert alert-danger" role="alert"> Votre message n\'a pas pu être envoyé <strong>Merci de remplir les champs obligatoires</strong> </div>').show();
                             }
                             else{
                                //alert('success');
                                $( '.screen-reader-response' ).append('<div class="alert alert-success" role="alert"><strong>Merci!</strong> Votre message a bien été envoyé</div>').show();
                                window.setInterval(function () {
                                   location.reload();
                                }, 2500);
                             
                             }
                          }            
                       });
                    }
                 }); 
              })

        </script>

        @endsection
        
