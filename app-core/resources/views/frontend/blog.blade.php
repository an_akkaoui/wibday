@extends('layouts.master')
@section('content')
    
    
    <!-- Breadcrumb Area Start -->
    <section class="breadcrumb-area section-padding-80">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2>Blog</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home.view') }}"><i class="icon_house_alt"></i> Accueil</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Blog</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Area End -->

    <!-- News Area Start -->
    <section class="akame-news-area section-padding-0-80">
        <div class="container">
            <div class="row mx-sm-n4">
                @foreach($posts as $post)
                <!-- Single Blog Item -->
                <div class="col-12 col-sm-6 col-lg-4 px-4 akame-blog-masonary-item mb-50 wow fadeInUp" data-wow-delay="800ms">
                    <!-- Single Post Area -->
                    <div class="single-post-area">
                        <div class="post-thumbnail">
                            <a href="{{ url('/').'/blog/'.$post->slug }}"><img src="{{Voyager::image($post->thumbnail('cropped'))}}" alt="{{$post->slug}}"></a>
                        </div>
                        <div class="post-content">
                            <a href="{{ url('/').'/blog/'.$post->slug }}" class="post-title">{{ $post->title }}</a>
                            <p>
                            {{ \Illuminate\Support\Str::limit($post->excerpt, 120) }}
                            @if (strlen(strip_tags($post->excerpt)) > 120)
                            <a href="{{ url('/').'/blog/'.$post->slug }}"> (suite)</a>
                            @endif
                                 
                            </p>
                        </div>
                    </div>
                </div>
                @endforeach



            </div>
<!--
            <div class="row">
                <div class="col-12 text-center">
                    <a href="#" class="btn akame-btn active mt-30">Load More</a>
                </div>
            </div>-->
        </div>
    </section>
    <!-- News Area End -->

    <!-- Border -->
    <div class="container">
        <div class="border-top"></div>
    </div>
@endsection