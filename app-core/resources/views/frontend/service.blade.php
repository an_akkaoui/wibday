@extends('layouts.master')
@section('content')

    <!-- Breadcrumb Area Start -->
    <section class="breadcrumb-area section-padding-80">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2>Service</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i class="icon_house_alt"></i> Accueil</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Nos services</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Area End -->


    <!-- Our Service Area Start -->
    <section class="akame-service-area section-padding-80-0">
        <div class="container">


            <div class="row">

                <!-- Single Service Area -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-service-area mb-80 wow fadeInUp" data-wow-delay="200ms">
                        <img src="img/core-img/s1.png" alt="">
                        <h5>Email Marketing</h5>
                        <p>Nos bases de données emails sont opt-in récents, Plus d'1 millions de contacts, segmenter par critère et par le comportemental.</p>
                    </div>
                </div>

                <!-- Single Service Area -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-service-area mb-80 wow fadeInUp" data-wow-delay="400ms">
                        <img src="img/core-img/s2.png" alt="">
                        <h5>Géneration de leads</h5>
                        <p>Vous souhaitez collecter des leads qualifiés en BtoB ou en BtoC ? Notre approche vous garantie des prospects chauds et qualifiés selon vos critères .</p>
                    </div>
                </div>

                <!-- Single Service Area -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-service-area mb-80 wow fadeInUp" data-wow-delay="600ms">
                        <img src="img/core-img/s3.png" alt="">
                        <h5>Plateforme de routage</h5>
                        <p>Notre plateforme vous offre tous les avantages pour vos campagnes emailing et analysez leurs performances en temps réel !</p>
                    </div>
                </div>

                <!-- Single Service Area -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-service-area mb-80 wow fadeInUp" data-wow-delay="800ms">
                        <img src="img/core-img/s4.png" alt="">
                        <h5>Insertions publicitaires</h5>
                        <p>Bénéficiez d’une exposition qui touche l’ensemble de nos abonnés, grâce à l’envoi quotidien de notre newsletter. </p>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- Our Service Area End -->
@endsection
