@extends('layouts.master')
@section('content')
    <!-- Blog Details Area Start -->
    <section class="akame-blog-details-area section-padding-80">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-10">
                    <div class="post-content">
<!--                        <div class="post-meta">
                            <a href="#" class="post-date"><i class="icon_clock_alt"></i> September 27, 2018</a>
                            <a href="#" class="post-comments"><i class="icon_chat_alt"></i> 10</a>
                        </div>-->
                        <h2 class="post-title">{{ $post->title }}</h2>
                    </div>
                </div>
            </div>

            <!-- <div class="row">
                <div class="col-12">
                    <div class="post-thumbnail mb-50" align="center">
                        <img src="img/bg-img/cible-wibday-2.png" alt="email marketing">
                    </div>
                </div>
            </div> -->

            <div class="row justify-content-center">
                <div class="col-2 col-md-2 col-lg-1">
                    <!-- Post Share -->
<!--                <div class="akame-post-share">
                        <a href="#" data-toggle="tooltip" data-placement="left" title="Facebook" class="facebook"><i class="fa fa-facebook"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="left" title="Twitter" class="twitter"><i class="fa fa-twitter"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="left" title="Google Plus" class="google-plus"><i class="fa fa-google-plus"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="left" title="Instagram" class="instagram"><i class="fa fa-instagram"></i></a>
                    </div>-->
                </div>
                <div class="col-12" style="box-sizing: border-box; margin: 0px; padding: 0px 15px; position: relative; width: 1140px; flex: 0 0 100%; max-width: 100%;">
                    <div class="post-thumbnail mb-50" style="box-sizing: border-box; margin: 0px 0px 50px; padding: 0px;" align="center"><img src="{{url('/')}}/storage/{!! $post->image !!}" style="max-width:800px!important;max-height:500px!important;"></div>
                </div>
                <div class="col-10 col-md-10 col-lg-9">
                    <div class="blog-details-text" style="box-sizing: border-box; margin: 0px; padding: 0px; position: relative; z-index: 1; color: #212529; font-family: 'Open Sans', sans-serif; font-size: 14px;">
                        {!! $post->body !!}
                    </div>

                    <!-- Post Author Area -->
                    <div class="post-author-area d-flex align-items-center justify-content-between">
                        <!-- Author Meta -->
                        <div class="author-meta d-flex align-items-center">
                            <div class="author-avatar">
                                <img src="{{url('/')}}/img/core-img/Wibday.png" alt="Wib Day">
                            </div>
                            <div class="author-text">
                                <p>Agence d'Email Marketing Direct</p>
                                <h5>Wib Day</h5>
                            </div>
                        </div>

                        <!-- Author Social Info -->
                        <div class="author-social-info">
                            <a href="https://www.facebook.com/WibDay.Maroc" class="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="https://www.linkedin.com/company/wibday/?viewAsMember=true" class="linkedin"><i class="fa fa-linkedin"></i></a>
                        </div>
                    </div>



    </section>
    <!-- Blog Details Area End -->
@endsection