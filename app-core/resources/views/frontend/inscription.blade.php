<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="/assets/form/style.css">
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <title>cimr</title>
  </head>
  <body style="background-color: #75b532; margin: 33px; " >
        <div class="container form p-3 " id="cont" style="border-radius: 5px ;">
            <div class="mb-3">
                <img width="100%" src="{{ asset('assets/img/1646058794323.jpg') }}" alt="" srcset="">
              </div>
              
              <form id='inscription-form' role='form' action='/assets/inscription.php' method='post' novalidate='true'>
                  @csrf
                  <div class='response'></div>
            <div class="mb-3">
                <label for="formGroupExampleInput" class="form-label " style="display: block;">Civilité</label>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="civilite" id="exampleRadios2" value="Mr" checked>
                    <label class="form-check-label" for="exampleRadios2">
                        Mr
                    </label>
                  </div>
                <div class="form-check-inline">
                    <input class="form-check-input" type="radio" name="civilite" id="exampleRadios1" value="Mme">
                    <label class="form-check-label" for="exampleRadios1">
                        Mme
                    </label>
                  </div>
                  
                  <div class="form-check-inline">
                    <input class="form-check-input" type="radio" name="civilite" id="exampleRadios3" value="Mlle">
                    <label class="form-check-label" for="exampleRadios3">
                        Mlle
                    </label>
                  </div>
              </div>
            <div class="mb-3">
                <label for="formGroupExampleInput" class="form-label">Nom</label>
                <input type="text" class="form-control" id="Nom" name="nom" required>
              </div>
              <div class="mb-3">
                <label for="formGroupExampleInput2" class="form-label">Prénom</label>
                <input type="text" class="form-control" id="Prénom" name="prenom" required>
              </div>
              <div class="mb-3">
                <label for="formGroupExampleInput2" class="form-label">Email</label>
                <input type="email" class="form-control" id="formGroupExampleInput2" name="email" required>
              </div>
              <div class="mb-3">
                <label for="formGroupExampleInput2" class="form-label">Téléphone</label>
                <input type="tel" class="form-control" id="formGroupExampleInput2" name="telephone" placeholder="0X XX XX XX XX" required>
              </div>
              <div class="mb-3">
                <label for="formGroupExampleInput2" class="form-label">Fonction</label>
                <input type="text" class="form-control" id="formGroupExampleInput2" name="fonction" required>
              </div>
              <p style="height: 0px;visibility: hidden"><input type="text" name="recaptcha" required value="" id="recaptchaValidator" pattern="1" data-error="Check `I am human` !" style="visibility: hidden"></p>

                        <div class="g-recaptcha" style="overflow: hidden" data-sitekey="6LfkYKMZAAAAAM92zBbBracGGJZ7UMCohVbRyzFl" data-callback="captcha_onclick"></div>

              <div class="mb-3 mt-3">
                <button class="btn btn-primary md-block" type="submit">ENVOYER</button>
              </div>
             
             </form>
              
        </div>
        <div style="color: #fff; text-align: center; padding-top: 11px; padding-bottom: 11px; text-decoration: none;" class="mb-3">
            <p>© Copyright <a style="text-decoration: none; color: #000000;"  target="_blank" href="https://www.wibday.com/">Wib Day</a>  | Tous Les Droits Résérvés.</p>
          </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->



<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js" integrity="sha256-dHf/YjH1A4tewEsKUSmNnV05DDbfGN3g7NMq86xgGh8=" crossorigin="anonymous"></script>

<script>

         $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

    

        
    

    // init the validator
    // validator files are included in the download package
    // otherwise download from http://1000hz.github.io/bootstrap-validator

    
$(function () {    
   
    $('#inscription-form').validator();
    
    // when the form is submitted
    $('#inscription-form').on('submit', function (e) {
     
    $('#inscription-form').find('.btn').addClass("disabled");
         
        // if the validator does not prevent form submit
        if (!e.isDefaultPrevented()) {

            var url = "{{route('insert_inscrit.view')}}"
            
            // POST values in the background the the script URL
            $.ajax({
                type: "POST",
                url: url,
                data: $(this).serialize(),
                error: function (xhr) {
                   $('.response').html('<div class="alert alert-danger"><ul></ul></div');
                   var pos = $('.response').offset();
                    $('body').animate({ scrollTop: pos.top });
                   $.each(xhr.responseJSON.errors, function(key,value) {
                     $('.response ul').append('<li>'+value+'</li>');
                 }); 
                },
                success: function (data)
                {
                    
                    $('#newsletter-form').find('.btn').removeClass("disabled");
                    // $('.response').html('<div class="alert alert-'+data.type+'">'+data.message+'</div');
                    
                    $("#cont").html('<div style="font-size: 19px;text-align: center;"> <p> Nous avons bien reçu vos informations. Notre conseiller en retraite vous appellera dans les plus brefs délais pour répondre à toutes vos interrogations. </p> <a href="https://www.cimr.ma/" target="_blank"><img width="110" src="http://admin.email.wibmailing.com/editor_images2/image_b81c709f//logo-cimr.png" alt="" <="" a=""> </a></div>')
                    
                        // empty the form

                        // if(data.type=="success")
                        
                        
                        
                    
                }

            });
            return false;
        }
    })
    
}); 

function captcha_onclick() {
            $('#recaptchaValidator').val(1);
            $('#inscription-form').validator('validate');
}

</script>
  </body>
</html>
