@extends('layouts.master')
@section('content')
    
    <!-- Breadcrumb Area Start -->
    <section class="breadcrumb-area section-padding-80">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2>Qui sommes-nous ?</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i class="icon_house_alt"></i> Accueil</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Notre Agence</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Area End -->

    <!-- Blog Details Area Start -->
    <section class="akame-blog-details-area section-padding-80">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-10">
                    <div class="post-content">

                        <h2 class="post-title">Leader de l’email marketing</h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="post-thumbnail mb-50" align="center">
                      <img src="img/bg-img/demarche-emailing.jpg" alt="démarche de l'emailing">
                    </div>
                </div>
            </div>
 <p>  <strong>  Email Marketing WIB DAY</strong></p>
            <div class="row justify-content-center">


                <div class="col-10 col-md-10 col-lg-9">
                    <div class="blog-details-text">
               
                        <p><span>W</span> <span>ib Day est une agence d’email Marketing direct et du conseil, elle se charge de l’envoi d’email à partir d’une plateforme performante qui gère toutes les campagnes emailing. Notre agence dispose d’1 millions d’emails ultra-qualifier et performant. Grâce à nos bases de données on vous permet de cibler et promouvoir votre business sur un panel de clients avec un fort taux de conversion. En tant que professionnel de l’emailing, nous possédons une connaissance approfondie et exhaustive des fichiers pouvant répondre à vos besoins. </span>  </p>
                

                        <p>Notre objectif est de vous aider à booster vos campagnes afin d’optimiser votre rentabilité, mais aussi d’offrir une meilleure visibilité aux personnes intéressées par vos services ou vos produits. Dans un contexte de plus en plus concurrentiel, l’entreprise doit mener des actions concrètes, fiables et efficaces pour développer, fidéliser et renouveler sa clientèle. </p>

                        <p>Que ce soit pour vous accompagner dans le design de vos emails, prendre en charge vos campagnes, analyser vos résultats et vous faire des recommandations, ou plus globalement, auditer votre stratégie Email, nos équipes d’experts sont à votre écoute pour étudier vos besoins et répondre à toutes vos questions pour que vos projets se transforment en résultats et vos objectifs en succès. </p>



                    <!-- Post Author Area -->
  
            </div>
        </div>
    </section>
    <!-- Blog Details Area End -->

    <!-- Testimonial Area Start -->
    <section class="testimonial-area section-padding-80">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-10">
                    <div class="testimonial-slides owl-carousel">

                        <!-- Single Testimonial Slide -->
                        <div class="single-testimonial-slide">
                            <img src="img/bg-img/r1.png" alt="">
                            <p>Savoir-Faire</p>
                            <div class="ratings-name d-flex align-items-center justify-content-center">
                                <div class="ratings mr-3">
                                    <i class="icon_star" style="color:#1d90b9;"></i>
                                    <i class="icon_star" style="color:#1d90b9;"></i>
                                    <i class="icon_star" style="color:#1d90b9;"></i>
                                    <i class="icon_star" style="color:#1d90b9;"></i>
                                    <i class="icon_star" style="color:#1d90b9;"></i>
                                </div>
                            </div>
                        </div>

                        <!-- Single Testimonial Slide -->
                        <div class="single-testimonial-slide">
                            <img src="img/bg-img/r2.png" alt="">
                            <p>La réactivité</p>
                            <div class="ratings-name d-flex align-items-center justify-content-center">
                                <div class="ratings mr-3">
                                    <i class="icon_star" style="color:#1d90b9;"></i>
                                    <i class="icon_star" style="color:#1d90b9;"></i>
                                    <i class="icon_star" style="color:#1d90b9;"></i>
                                    <i class="icon_star" style="color:#1d90b9;"></i>
                                    <i class="icon_star" style="color:#1d90b9;"></i>
                                </div>
                            </div>
                        </div>

                        <!-- Single Testimonial Slide -->
                        <div class="single-testimonial-slide">
                            <img src="img/bg-img/r3.png" alt="">
                            <p>Force de Proposition</p>
                            <div class="ratings-name d-flex align-items-center justify-content-center">
                                <div class="ratings mr-3">
                                    <i class="icon_star" style="color:#1d90b9;"></i>
                                    <i class="icon_star" style="color:#1d90b9;"></i>
                                    <i class="icon_star" style="color:#1d90b9;"></i>
                                    <i class="icon_star" style="color:#1d90b9;"></i>
                                    <i class="icon_star" style="color:#1d90b9;"></i>
                                </div>
                            </div>
                        </div>
                        
                         <!-- Single Testimonial Slide -->
                        <div class="single-testimonial-slide">
                            <img src="img/bg-img/r4.png" alt="">
                            <p>Suivi Régulier</p>
                            <div class="ratings-name d-flex align-items-center justify-content-center">
                                <div class="ratings mr-3">
                                    <i class="icon_star" style="color:#1d90b9;"></i>
                                    <i class="icon_star" style="color:#1d90b9;"></i>
                                    <i class="icon_star" style="color:#1d90b9;"></i>
                                    <i class="icon_star" style="color:#1d90b9;"></i>
                                    <i class="icon_star" style="color:#1d90b9;"></i>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Testimonial Area End -->
@endsection