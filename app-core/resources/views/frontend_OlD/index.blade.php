@extends('layouts.master')
@section('content')

    <!-- Welcome Area Start -->
    <section class="welcome-area">
        <div class="welcome-slides owl-carousel">
            <!-- Single Welcome Slide -->
            <div class="single-welcome-slide bg-img" style="background-image: url(img/bg-img/16.jpg);">
                <!-- Welcome Content -->
                <div class="welcome-content h-100">
                    <div class="container h-100">
                        <div class="row h-100 align-items-center">
                            <!-- Welcome Text -->
                            <div class="col-12 col-md-9 col-lg-6">
                                <div class="welcome-text">
                                    <h2 data-animation="fadeInUp" data-delay="100ms">Solution de Mailing<br/> Professionnel ...</h2>
                                    <p data-animation="fadeInUp" data-delay="400ms">Planifiez, Boostez vos campagnes et Générer plus de Lead <br/>pour Développez vos ventes !</p>
                                    <a href="{{ route('contact.view') }}" class="btn akame-btn" data-animation="fadeInUp" data-delay="700ms">Demande de devis</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Single Welcome Slide -->
            <div class="single-welcome-slide bg-img" style="background-image: url(img/bg-img/16-1.jpg);">
                <!-- Welcome Content -->
                <div class="welcome-content h-100">
                    <div class="container h-100">
                        <div class="row h-100 align-items-center">
                            <!-- Welcome Text -->
                            <div class="col-12 col-md-9 col-lg-6">
                                <div class="welcome-text">
                                    <h2 data-animation="fadeInUp" data-delay="100ms">Solution de Mailing<br/> Garantie ...</h2>
                                    <p data-animation="fadeInUp" data-delay="400ms">Trouvez la cible que vous cherchez, Grâce à la location de base de données!</p>
                                    <a href="{{ route('contact.view') }}" class="btn akame-btn active" data-animation="fadeInUp" data-delay="700ms">Demande de devis</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Welcome Area End -->

    <!-- About Area Start -->
    <section class="akame-about-area section-padding-80-0">
        <div class="container">
            <div class="row align-items-center">
                <!-- Section Heading -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="section-heading mb-80">
                        <h2>Email Marketing</h2>
                        <p>Agence d'Email Marketing Direct</p>
                        <span class="taille">Qui sommes-nous ?</span>
                    </div>
                </div>

                <!-- About Us Thumbnail -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="about-us-thumbnail mb-80">
                        <img src="img/bg-img/15.jpg" alt="Agence d'email marketing au Maroc">
                    </div>
                </div>

                <!-- About Us Content -->
                <div class="col-12 col-lg-4">
                    <div class="about-us-content mb-80 pl-4">
                        <h3>Agence spécialisée dans l'email Marketing</h3>
                        <p>Wib Day est une agence de conseil dans la réalisation des campagnes de marketing direct. L'agence vous accompagne dans la gestion de : collecte de données prospects et clients, gestion de bases de données, création d'opérations ciblées, analyse des retours et recommandations, optimisation de la vente </p>
                       
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About Area End -->

    <!-- Border -->
    <div class="container">
        <div class="border-top mt-3"></div>
    </div>

    <!-- Our Service Area Start -->
    <section class="akame-service-area section-padding-80-0">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Section Heading -->
                    <div class="section-heading text-center">
                        <h2>Nos Services</h2>
                        <p>Nous vous proposons des solutions d’Email Marketing évolutif</p>
                    </div>
                </div>
            </div>

            <div class="row">

                <!-- Single Service Area -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-service-area mb-80 wow fadeInUp" data-wow-delay="200ms">
                        <img src="img/core-img/s1.png" alt="">
                        <h5>Email Marketing</h5>
                        <p>Nos bases de données emails sont opt-in récents, Plus d'1 millions de contacts, segmenter par critère et par le comportemental.</p>
                    </div>
                </div>

                <!-- Single Service Area -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-service-area mb-80 wow fadeInUp" data-wow-delay="400ms">
                        <img src="img/core-img/s2.png" alt="">
                        <h5>Géneration de leads</h5>
                        <p>Vous souhaitez collecter des leads qualifiés en BtoB ou en BtoC ? Notre approche vous garantie des prospects chauds et qualifiés selon vos critères .</p>
                    </div>
                </div>

                <!-- Single Service Area -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-service-area mb-80 wow fadeInUp" data-wow-delay="600ms">
                        <img src="img/core-img/s3.png" alt="">
                        <h5>Plateforme de routage</h5>
                        <p>Notre plateforme vous offre tous les avantages pour vos campagnes emailing et analysez leurs performances en temps réel !</p>
                    </div>
                </div>

                <!-- Single Service Area -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-service-area mb-80 wow fadeInUp" data-wow-delay="800ms">
                        <img src="img/core-img/s4.png" alt="">
                        <h5>Insertions publicitaires</h5>
                        <p>Bénéficiez d’une exposition qui touche l’ensemble de nos abonnés, grâce à l’envoi quotidien de notre newsletter. </p>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- Our Service Area End -->

    <!-- Why Choose Us Area Start -->
    <section class="why-choose-us-area bg-gray section-padding-80-0">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-6">
                    <div class="choose-us-thumbnail mt-30 mb-80">
                        <div class="choose-us-img bg-img" style="background-image: url(img/bg-img/4.jpg);"></div>
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <!-- Section Heading -->
                    <div class="section-heading">
                         <h2>4 Raisons de nous choisir</h2>
                        <p>Faites un tour d’horizon de l’ensemble des solutions proposées par Wib Day. </p>
                    </div>
                    <!-- Choose Us Content -->
                    <div class="choose-us-content mt-30 mb-80">
                        <ul>
                            <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Forte expérience dans l’emailing sur les sujets d’acquisition, de délivrabilité et de performance.</li>
                            <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Lancer une campagne en quelques heures, relayer une offre de dernière minute. Nous garantissons que de rapides échanges avec nos représentants.</li>
                            <li><i class="fa fa-check-square-o" aria-hidden="true"></i> L’efficacité de notre produit et les meilleurs tarifs disponibles sur le marché marocain, qui correspondent à votre budget. </li>
                            <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Chaque campagne on vous envoie un reporting pour faire un point sur les différentes actions effectuées. Selon vos attentes.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Why Choose Us Area End -->

    <!-- Portfolio Area Start -->
    <section class="akame-portfolio section-padding-80 clearfix">
        <div class="container">
            <div class="row">
                <!-- Section Heading -->
                <div class="col-12">
                    <div class="section-heading text-center">
                        <h2>Nos e-mailing</h2>
                        <p>Ils nous ont fait confiance </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="portfolio-menu text-center mb-30">
                     <!--     <button class="btn active" data-filter="*">Campagnes</button>
                      <button class="btn" data-filter=".haircuts">Haircuts</button>
                        <button class="btn" data-filter=".dye">Dye</button>
                        <button class="btn" data-filter=".shave">Shave</button>
                        <button class="btn" data-filter=".hairstyle">Hairstyle</button>-->
                    </div>
                </div>
            </div>

            <div class="row akame-portfolio-area">
                <!-- Single Portfolio Item -->
                <div class="col-12 col-sm-6 col-lg-3 akame-portfolio-item haircuts mb-30 wow fadeInUp" data-wow-delay="200ms">
                    <div class="akame-portfolio-single-item">
                        <img src="img/bg-img/5.jpg" alt="">

                        <!-- Overlay Content -->
                        <div class="overlay-content d-flex align-items-center justify-content-center">
                            <div class="overlay-text text-center">
                                <h4>LA BRASSERIE DU BOULEVARD</h4>
                                <p>la soirée LATINO</p>
                            </div>
                        </div>

                        <!-- Thumbnail Zoom -->
                        <a href="img/bg-img/5.jpg" class="thumbnail-zoom"><i class="icon_search"></i></a>
                    </div>
                </div>

                <!-- Single Portfolio Item -->
                <div class="col-12 col-sm-6 akame-portfolio-item dye mb-30 wow fadeInUp" data-wow-delay="300ms">
                    <div class="akame-portfolio-single-item">
                        <img src="img/bg-img/6.jpg" alt="">

                        <!-- Overlay Content -->
                        <div class="overlay-content d-flex align-items-center justify-content-center">
                            <div class="overlay-text text-center">
                                <h4>Electroplanet</h4>
                                <p>Campagne Chaabane</p>
                            </div>
                        </div>

                        <!-- Thumbnail Zoom -->
                        <a href="img/bg-img/6.jpg" class="thumbnail-zoom"><i class="icon_search"></i></a>
                    </div>
                </div>

                <!-- Single Portfolio Item -->
                <div class="col-12 col-lg-3 akame-portfolio-item shave mb-30 wow fadeInUp" data-wow-delay="400ms">
                    <div class="akame-portfolio-single-item">
                        <img src="img/bg-img/7.jpg" alt="">

                        <!-- Overlay Content -->
                        <div class="overlay-content d-flex align-items-center justify-content-center">
                            <div class="overlay-text text-center">
                                <h4>Smeia - BMW</h4>
                                <p>Les Ventes Privilèges BMW</p>
                            </div>
                        </div>

                        <!-- Thumbnail Zoom -->
                        <a href="img/bg-img/7.jpg" class="thumbnail-zoom"><i class="icon_search"></i></a>
                    </div>
                </div>

                <!-- Single Portfolio Item -->
                <div class="col-12 col-sm-6 col-lg-3 akame-portfolio-item hairstyle mb-30 wow fadeInUp" data-wow-delay="500ms">
                    <div class="akame-portfolio-single-item">
                        <img src="img/bg-img/8.jpg" alt="">

                        <!-- Overlay Content -->
                        <div class="overlay-content d-flex align-items-center justify-content-center">
                            <div class="overlay-text text-center">
                                <h4>Mazagan</h4>
                                <p>Promo Summer</p>
                            </div>
                        </div>

                        <!-- Thumbnail Zoom -->
                        <a href="img/bg-img/8.jpg" class="thumbnail-zoom"><i class="icon_search"></i></a>
                    </div>
                </div>
                
                
                  <!-- Single Portfolio Item -->
                <div class="col-12 col-sm-6 col-lg-3 akame-portfolio-item hairstyle mb-30 wow fadeInUp" data-wow-delay="500ms">
                    <div class="akame-portfolio-single-item">
                        <img src="img/bg-img/10.jpg" alt="">

                        <!-- Overlay Content -->
                        <div class="overlay-content d-flex align-items-center justify-content-center">
                            <div class="overlay-text text-center">
                                <h4>Carrefour Label'Vie</h4>
                                <p>Catalogue Ramadan</p>
                            </div>
                        </div>

                        <!-- Thumbnail Zoom -->
                        <a href="img/bg-img/10.jpg" class="thumbnail-zoom"><i class="icon_search"></i></a>
                    </div>
                </div>              
                
                
                
                
                
                
                
                

                <!-- Single Portfolio Item -->
                <div class="col-12 col-sm-6 col-lg-3 akame-portfolio-item dye mb-30 wow fadeInUp" data-wow-delay="600ms">
                    <div class="akame-portfolio-single-item">
                        <img src="img/bg-img/9.jpg" alt="">

                        <!-- Overlay Content -->
                        <div class="overlay-content d-flex align-items-center justify-content-center">
                            <div class="overlay-text text-center">
                                <h4>Kia</h4>
                                <p>Summer Edition</p>
                            </div>
                        </div>

                        <!-- Thumbnail Zoom -->
                        <a href="img/bg-img/9.jpg" class="thumbnail-zoom"><i class="icon_search"></i></a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="view-all-btn mt-30 text-center">
                        <a href="{{ route('references.view') }}" class="btn akame-btn">Nos Campagnes</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Portfolio Area End -->
<!--
    <div id="jquery-script-menu">
<div class="jquery-script-center">

<div class="jquery-script-ads"><script type="text/javascript"><!--
google_ad_client = "ca-pub-2783044520727903";
/* jQuery_demo */
google_ad_slot = "2780937993";
google_ad_width = 728;
google_ad_height = 90;
//-->
<!--</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></div>
<div class="jquery-script-clear"></div>
</div>
</div>

<div class="slider-wrap1">
<div class="slider1">
<ul class="logoclt">
<li> <img src="img/core-img/mazagan.jpg"/> </li>
<li> <img src="img/core-img/adwebmaroc-1.jpg"/></li>
<li> <img src="img/core-img/avis-1.jpg"/></li>
<li> <img src="img/core-img/bmw.jpg"/> </li>
<li> <img src="img/core-img/carrefour-1.jpg"/> </li>
<li> <img src="img/core-img/chocodiva.jpg"/> </li>
<li> <img src="img/core-img/anfaplace-1.jpg"/></li>
<li> <img src="img/core-img/electroplanet-1.jpg"/> </li>
<li> <img src="img/core-img/ford-1.jpg"/> </li>
<li> <img src="img/core-img/hilton.jpg"/> </li>
<li> <img src="img/core-img/hitradio-1.jpg"/> </li>
<li> <img src="img/core-img/hyundai.jpg"/> </li>
<li> <img src="img/core-img/iga.jpg"/> </li>
<li> <img src="img/core-img/istikbal.jpg"/> </li>
<li> <img src="img/core-img/kasbahtamadout.jpg"/> </li>
<li> <img src="img/core-img/kia.jpg"/> </li>
<li> <img src="img/core-img/Logo-hrm-1.jpg"/> </li>
<li> <img src="img/core-img/london-academy-1.jpg"/> </li>
<li> <img src="img/core-img/macroisiere-1.jpg"/> </li>
<li> <img src="img/core-img/myschool-1.jpg"/> </li>
<li> <img src="img/core-img/palmeraie-1.jpg"/> </li>
<li> <img src="img/core-img/preventica-casa.jpg"/> </li>
<li> <img src="img/core-img/vizir center.png"/> </li>
<li> <img src="img/core-img/vtc.jpg"/> </li>
<li> <img src="img/core-img/widiane-1.jpg"/> </li>
</ul>
</div>
<a href="#" class="slider-arrow1 sa-left1">&lt;</a> <a href="#" class="slider-arrow1 sa-right1">&gt;</a> </div>
<script src="http://code.jquery.com/jquery-latest.min.js"></script> 
<script src="jquery.lbslider.js"></script> 
<script>
jQuery('.slider1').lbSlider({
    leftBtn: '.sa-left1',
    rightBtn: '.sa-right1',
    visible: 5,
    autoPlay: true,
    autoPlayDelay: 4
});
-->
@endsection