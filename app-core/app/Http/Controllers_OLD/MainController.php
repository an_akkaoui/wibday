<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TCG\Voyager\Models\Post;
use App\Reference;
class MainController extends Controller
{
    public function index()
    {
    	$posts = Post::published()->get();
    	return view('frontend.blog', compact('posts'));
    }
    
    public function home()
    {
    	return view('frontend.index');
    }

    public function article($slug){

    	$post = Post::whereSlug($slug)->published()->firstOrFail();

    	return view('frontend.article', compact('post'));
    }

    public function references()
    {
        $references = Reference::orderByDesc('created_at')->get();
    	return view('frontend.references', compact('references'));
    }

    public function service()
    {
    	return view('frontend.service');
    }

    public function agence()
    {
    	return view('frontend.agence');
    }

    public function contact()
    {
    	return view('frontend.contact');
    }
}
