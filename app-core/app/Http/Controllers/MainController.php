<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TCG\Voyager\Models\Post;
use App\Reference;
use Illuminate\Support\Facades\DB;
class MainController extends Controller
{
    public function index()
    {
    	$posts = Post::published()->get();
    	return view('frontend.blog', compact('posts'));
    }
    
    public function home()
    {
    	return view('frontend.index');
    }
    public function inscription_form(){
        return view('frontend.inscription');
    }
    
    public function insert_inscrit(Request $request){
        $messages = [
        'required' => ' Le champ :attribute est obligatoire.',
        'email.unique' => ' Cette adresse email est déjà utilisée.',
        'email' => ' Cette adresse email est invalide.',
        'digits' => ' Le numéro de téléphone ne peut pas être inférieur à 10 chiffres.',
        'numeric' => ' Mauvais numéro de téléphone.',
        'telephone.regex' => ' Mauvais numéro de téléphone.',
        'email.regex' => ' Mauvais email.'
        ];

            $validatedData = $request->validate([
                
                'nom' => 'required|max:255',
                'prenom' => 'required|max:255',
                'email' => 'required|max:255|unique:inscrits|regex:/(.+)@(.+)\.(.+)/i',
                'telephone' => 'required|numeric|regex:/(0)[0-9]{9}/|digits:10',
                'fonction' => 'required|max:255',
                
                
            ],$messages);
            
        if($request->ajax() and $validatedData){
                if(DB::table('inscrits')->insert(['civilite' => $request->civilite, 'nom' => $request->nom , 'prenom' => $request->prenom, 'email' => $request->email, 'telephone' => $request->telephone, 'fonction' => $request->fonction])){
                    $data["type"]="success";
                    $data["message"]="Vous avez été inscrit avec succès!";
                    return $data;
                }else{
                    $data["type"]="error";
                    $data["message"]="Réessayez plus tard!";
                    return $data;
                }
                
            }else{
                $data["type"]="error";
                $data["message"]="Réessayez plus tard!";
                return $data;
            }
    }
    
    public function exportCsv()
    {
       $fileName = 'inscrits.csv';
       $subscribers = DB::table('inscrits')->get();

            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );

            $columns = array('Civilité', 'Nom', 'Prénom', 'Email', 'Téléphone', 'Fonction');

            $callback = function() use($subscribers, $columns) {
                $file = fopen('php://output', 'w');
                fprintf($file, chr(0xEF).chr(0xBB).chr(0xBF));
                fputcsv($file, $columns);

                foreach ($subscribers as $subscriber) {
                    $row['Civilite']  = $subscriber->civilite;
                    $row['Nom']    = $subscriber->nom;
                    $row['Prenom']    = $subscriber->prenom;
                    $row['Email']    = $subscriber->email;
                    $row['Telephone']    = $subscriber->telephone;
                    $row['Fonction']    = $subscriber->fonction;
                    

                    fputcsv($file, array($row['Civilite'],
                                            $row['Nom'],
                                            $row['Prenom'] ,
                                            $row['Email'], 
                                            $row['Telephone'],
                                            $row['Fonction']));
                }

                fclose($file);
            };

            return response()->stream($callback, 200, $headers);
        }  

    public function article($slug){

    	$post = Post::whereSlug($slug)->published()->firstOrFail();

    	return view('frontend.article', compact('post'));
    }

    public function references()
    {
        $references = Reference::orderByDesc('created_at')->get();
    	return view('frontend.references', compact('references'));
    }

    public function service()
    {
    	return view('frontend.service');
    }

    public function agence()
    {
    	return view('frontend.agence');
    }

    public function contact()
    {
    	return view('frontend.contact');
    }
}
