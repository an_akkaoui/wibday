<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'MainController@home')->name('home.view');

Route::get('/blog', 'MainController@index')->name('blog-index.view');
Route::get('/blog/{slug}', 'MainController@article')->name('blog-article.view');
Route::get('/references', 'MainController@references')->name('references.view');
Route::get('/service', 'MainController@service')->name('service.view');
Route::get('/agence', 'MainController@agence')->name('agence.view');
Route::get('/contact', 'MainController@contact')->name('contact.view');
Route::get('/references', 'MainController@references')->name('references.view');

Route::get('/inscription-form', 'MainController@inscription_form')->name('inscription_form.view');
Route::post('/inscription-post', 'MainController@insert_inscrit')->name('insert_inscrit.view');

Route::get('/export-inscrits', 'MainController@exportCsv')->name('inscription_form.view');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
