<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_model extends CI_Model {

    var $table = 'form';     
    var $column_order = array('form_id','name','created_by','status','updated_at',null); 
    //set column field database for datatable orderable     
    var $column_search = array('name','status'); 

    public $status; 
    public $roles;
    
    function __construct(){
        // Call the Model constructor
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('text');
        $this->status = $this->config->item('status');
        $this->roles = $this->config->item('roles');
        $this->banned_users = $this->config->item('banned_users');

        $config = array(
            'table' => $this->table,
            'id' => 'form_id',
            'field' => 'slug',
            'title' => 'title',
            'replacement' => 'dash' // Either dash or underscore
        );

        $this->load->library('slug', $config);

    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if(isset($_POST['search']['value']) && $_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    //get all form
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    private function create_unique_slug($string, $table, $field='slug', $key = NULL, $value = NULL)
    {
        $t =& get_instance();
        $slug = url_title($string);
        $slug = strtolower($slug);
        $i = 0;
        $params = array ();
        $params[$field] = $slug;
     
        if($key)$params["$key !="] = $value; 
     
        while ($t->db->where($params)->get($table)->num_rows())
        {   
            if (!preg_match ('/-{1}[0-9]+$/', $slug ))
                $slug .= '-' . ++$i;
            else
                $slug = preg_replace ('/[0-9]+$/', ++$i, $slug );
             
            $params [$field] = $slug;
        }   
        return $slug;   
    }

    private function generate_slugs($string){

        $table = $this->table;
        $slug = url_title(convert_accented_characters($string), 'dash', true);
        $q = $this->db->query( " SELECT slug FROM `$table` WHERE slug LIKE '%$slug%' " )->result_array();

        if($q){
            
            $numericalPrefix = 1;
    
            while(1){

                $newSlug = $slug."-".$numericalPrefix++;

                $checkSlug = $this->db->query( " SELECT slug FROM `$table` WHERE slug LIKE '%$newSlug%' " )->result_array();
    
                if(!$checkSlug){
                    $slug = $newSlug;
                    break;
                }
    
            }
    
        }else{
            $slug = $slug;
        }

        return $slug;  
    }

    private function generate_slug($string){
        
        $table = $this->table;
        $slug = url_title(convert_accented_characters($string), 'dash', true);
        $q = $this->db->query( " SELECT slug FROM `$table` WHERE slug LIKE '%$slug%' " )->result_array();

        $total_row = count($q);
        if($total_row > 0)
        {
            foreach($q as $row)
            {
                $data[] = $row['slug'];
            }
            
            if(in_array($slug, $data))
            {
                $count = 0;
                while( in_array( ($slug . '-' . ++$count ), $data) );
                $slug = $slug . '-' . $count;
            }
        }

        return $slug;   
    }

    private function slug_exists($slug)
    {
        $this->db->where('slug',$slug);
        $query = $this->db->get('form');
        if ($query->num_rows() > 0){
            $partially_matched_slug = $this->db->select('slug')->like('slug', $slug, 'after')->from('form')->get()->result_array();
            if( count($partially_matched_slug) > 1 ){
                $slug_array = array_column($partially_matched_slug, 'slug');
                
                $i = 1;
                while(true) {
                    if( ! in_array($slug.'-'.$i, $slug_array)) {
                        $slug = $slug.'-'.$i;
                        break;
                    }
                    $i++;
                }
                return $slug;
            }
            else 
                return $slug;
        }
        else{
            return $slug;
        }
    }

    public function save_upload($form_id,$image,$column){
        $data = array(
            $column => $image
        );
        $this->db->where('form_id', $form_id);
        $result = $this->db->update('form', $data); 
        return $result;
    }

    public function save_FormConfirmation($data){

        $form_id = $data['form_id'];
        $sql = " SELECT * FROM `form_confirmation` WHERE form_id = $form_id ";
		$getC = $this->db->query( $sql )->result_array();

        if( count($getC) > 0){
            if( empty($data['url']) )
                unset($data['url']);
            if( empty($data['message']) )
                unset($data['message']);

            // print_r($data);
            $this->db->where('form_id', $form_id);
            $result = $this->db->update('form_confirmation', $data);
            return true;
        }
        else {
            $q = $this->db->insert_string('form_confirmation',$data);             
            $this->db->query($q);
            return true;
        }

    }

    public function insertForm($created_by)
    {  
        // $createSlug = url_title(convert_accented_characters('Formulaire sans nom'), 'dash', true);
        // $slug = $this->slug_exists($createSlug);

        $slug = $this->slug->create_uri(array( 'title' => 'Formulaire sans nom' ));

        $string = array(
            'name' => 'Formulaire sans nom',
            'slug' => $slug,
            'status' => 0,
            'json' => '[{"type":"header","subtype":"h3","label":"Formulaire sans nom\n","access":false}]', 
            'message' => '',
            'created_by' => $created_by,
            'created_at' => date("Y-m-d H:i:s")
        );
        $q = $this->db->insert_string('form',$string);             
        $this->db->query($q);
        return $this->db->insert_id();
    }

    public function getFormInfo($id)
    {
        $q = $this->db->get_where('form', array('form_id' => $id), 1);  
        if($this->db->affected_rows() > 0){
            $row = $q->row();
            return $row;
        }else{
            error_log('no form found getUserInfo('.$id.')');
            return false;
        }
    }

    public function getFormConfirmation($id)
    {
        $q = $this->db->get_where('form_confirmation', array('form_id' => $id), 1);  
        if($this->db->affected_rows() > 0){
            $row = $q->row();
            return $row;
        }else{
            error_log('no form found getFormConfirmation('.$id.')');
            return false;
        }
    }

    // get from by slug
    public function getFormBySlug($slug)
    {
        $q = $this->db->get_where('form', array('slug' => $slug, 'status' => 1), 1);  
        if($this->db->affected_rows() > 0){
            $row = $q->row();
            return $row;
        }else{
            error_log('no form found getUserInfo('.$slug.')');
            return false;
        }
    }
    
    // delete Submission
    public function deleteSubmission($id)
    {   
        $this->db->where('id', $id);
        $this->db->delete('form_submission');
        
        if ($this->db->affected_rows() == '1') {
            return FALSE;
        }
        else {
            return TRUE;
        }
    }
    
    //delete form
    public function deleteForm($id)
    {   
        $sql = " SELECT * FROM `form_submission` WHERE form_id = $id ";
		$submissions = $this->db->query( $sql )->result_array();

        if( count($submissions) > 0) 
        return FALSE;

        $this->db->where('form_id', $id);
        $this->db->delete('form');
        
        if ($this->db->affected_rows() == '1') {
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    public function updateFormInfo($post)
    {
        // $createSlug = url_title(convert_accented_characters('Formulaire sans nom'), 'dash', true);
        // $slug = $this->slug_exists($createSlug);
        
        $slug = $this->slug->create_uri(array( 'title' => $post['name'] ), $post['form_id']);

        $data = array(
            'json' => $post['toJson'],
            'name' => $post['name'], 
            'slug' => $slug, 
            'status' => $post['status'],
            'validation' => $post['validation'],
            'total_limit' => $post['total_limit'],
            'limit_message' => str_replace("'", '’', $post['limit_message']),
            'updated_by' => $post['updated_by'],
            'message' => str_replace("'", '’', $post['message']),
            'theme_id' => $post['theme_id'],
            'updated_at' => date("Y-m-d H:i:s"),
        );

        $this->db->where('form_id', $post['form_id']);
        $this->db->update('form', $data); 
        $success = $this->db->affected_rows(); 
        
        if(!$success){
            return false;
        }
        else 
            return true; 
    }

} 