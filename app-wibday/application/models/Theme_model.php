<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Theme_model extends CI_Model {

    var $table = 'theme';     
    var $column_order = array('id','name','color','created_by','updated_at',null); 
    //set column field database for datatable orderable     
    var $column_search = array('name'); 

    public $status; 
    public $roles;
    
    function __construct(){
        // Call the Model constructor
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('text');
        $this->status = $this->config->item('status');
        $this->roles = $this->config->item('roles');
        $this->banned_users = $this->config->item('banned_users');

    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if(isset($_POST['search']['value']) && $_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    //Get All Theme
    function getAllThemes()
    {
        $this->db->where('status', 1);
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }

    //Get All Theme
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    // Insert New Theme
    public function insertTheme($created_by)
    {
        $string = array(
            'name' => 'Formulaire sans nom',
            'description' => 'Formulaire sans nom',
            'color' => '', 
            'css' => '',
            'created_by' => $created_by,
            'created_at' => date("Y-m-d H:i:s")
        );
        $q = $this->db->insert_string('form',$string);             
        $this->db->query($q);
        return $this->db->insert_id();
    }

    // Get Theme By ID
    public function getThemeInfo($id)
    {
        $q = $this->db->get_where($this->table, array('id' => $id), 1);  
        if($this->db->affected_rows() > 0){
            $row = $q->row();
            return $row;
        }else{
            error_log('no theme found getThemeInfo('.$id.')');
            return false;
        }
    }
    
    // delete Theme
    public function deleteTheme($id)
    {   
        $sql = " SELECT * FROM `form` WHERE theme_id = $id ";
		$themes = $this->db->query( $sql )->result_array();

        if( count($themes) > 0) 
        return FALSE;

        $this->db->where('id', $id);
        $this->db->delete($this->table);
        
        if ($this->db->affected_rows() == '1') {
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    //add Theme
    public function addTheme($d)
    {  
            $string = array(
                'name' => $d['name'],
                'color' => $d['color'],
                'css' => $d['css'],
                'status' => 1, 
                'created_by' => $d['idUser'], 
                'updated_by' => $d['idUser'], 
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            );
            $q = $this->db->insert_string($this->table,$string);             
            $this->db->query($q);
            return $this->db->insert_id();
    }

    // Update Theme
    public function updateTheme($post)
    {
        $data = array(
            'name' => $post['name'],
            'color' => $post['color'],
            'css' => $post['css'],
            'status' => 1, 
            'updated_by' => $post['idUser'], 
            'updated_at' => date("Y-m-d H:i:s")
        );

        $this->db->where('id', $post['theme_id']);
        $this->db->update($this->table, $data); 
        $success = $this->db->affected_rows(); 
        
        if(!$success){
            return false;
        }
        else 
            return true; 
    }

} 