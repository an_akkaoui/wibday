<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delete extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('User_Delete_Model');
    }

    public function index($uid)
    {
        $this->User_Delete_Model->deleterow($uid);
        $this->load->view('users/read');
    }
}