<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Read extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('User_Read_Model');
        $this->load->model('User_Delete_Model');
    }

    // for all records
    public function index(){
        $results=$this->User_Read_Model->getdata();
        $this->load->view('users/read', ['result'=>$results]);
    }
    // for particular recod
    public function getdetails($uid)
    {
        $reslt=$this->User_Read_Model->getuserdetail($uid);
        $this->load->view('users/update', ['row'=>$reslt]);
    }
    // delete particular recod
    public function deleterecod($uid)
    {
        $reslt=$this->User_Read_Model->getuserdetail($uid);
        // $this->load->view('users/update', ['row'=>$reslt]);
    }

}