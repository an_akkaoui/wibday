<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Insert extends CI_Controller {

	

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->helper('form');
        $this->load->model('User_Insert_Model');
    }

	// For data insertion	
	public function index(){

		$this->form_validation->set_rules('firstname','First Name','required|alpha');	
		$this->form_validation->set_rules('lastname','Last Name','required|alpha');	
		$this->form_validation->set_rules('emailid','Email id','required|valid_email');
		$this->form_validation->set_rules('contactno','Contact Number','required|numeric|exact_length[10]');
		$this->form_validation->set_rules('address','Address','required');	

		if($this->form_validation->run()){
			$fname=$this->input->post('firstname');
			$lname=$this->input->post('lastname');
			$email=$this->input->post('emailid');
			$cntno=$this->input->post('contactno');
			$adrss=$this->input->post('address');

			$this->User_Insert_Model->insertdata($fname,$lname,$email,$cntno,$adrss);
			$this->load->view('users/insert');
		}
		else {
			$this->load->view('users/insert');
		}
	}

	// For data updation
	public function updatedetails(){

		$this->form_validation->set_rules('firstname','First Name','required|alpha');	
		$this->form_validation->set_rules('lastname','Last Name','required|alpha');	
		$this->form_validation->set_rules('emailid','Email id','required|valid_email');
		$this->form_validation->set_rules('contactno','Contact Number','required|numeric|exact_length[10]');
		$this->form_validation->set_rules('address','Address','required');	

		if($this->form_validation->run()){
			$fname=$this->input->post('firstname');
			$lname=$this->input->post('lastname');
			$email=$this->input->post('emailid');
			$cntno=$this->input->post('contactno');
			$adrss=$this->input->post('address');
			$usid=$this->input->post('userid');

			$this->User_Insert_Model->updatedetails($fname,$lname,$email,$cntno,$adrss,$usid);
		}
		else {
			$this->session->set_flashdata('error', 'Somthing went worng. Try again with valid details !!');
			redirect('users/read');
		}
	}

}