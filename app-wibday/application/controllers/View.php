<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// *** Php mailer
require_once(APPPATH.'third_party/PHPMailer/src/Exception.php');
require_once(APPPATH.'third_party/PHPMailer/src/PHPMailer.php');
require_once(APPPATH.'third_party/PHPMailer/src/SMTP.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class View extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('User_model', 'user_model', TRUE);
        $this->load->model('Form_model', 'form_model', TRUE);
        $this->load->model('Theme_model', 'theme_model', TRUE);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->load->library('session');
        $this->load->helper('captcha');

        $this->data['dataSession'] = $this->session->userdata;
        // print_r($this->data['dataSession']);
    }

	public function index( $slug = NULL )
	{
        redirect('https://wibday.com/');
    }

    private function validateEmail($email) {
        if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        else {
            return false;
        }
    }

	public function share()
	{
        if( (isset($_POST['invalidCheck']) && $_POST['invalidCheck'] == 'on' ) && isset($_POST['emails']) ){
            // print_r($_POST);
            $date = strtotime(date("Y-m-d H:i:s"));

            $post = $this->input->post(NULL, TRUE);
            $cleanPost = $this->security->xss_clean($post);

            $emails = implode(',', $this->input->post('emails'));
            $cleanPost['emails'] = $emails;
            $cleanPost['emails'] = $emails;
            $cleanPost['created_at'] = $date;
            $cleanPost['id_form'] = $this->input->post('id_form');
            $cleanPost['id_submission'] = $this->input->post('id_submission');
            // uset($cleanPost['invalidCheck']);
            if( $this->db->insert('submission_share', $cleanPost) )
                echo $this->db->insert_id();
            else
                echo 'error';
        }
        else
            echo 'error';
    }

	function getRandomWord($len = 10) {
        //$word = array_merge(range('a', 'z'), range('A', 'Z'));
        $word = array_merge(range('A', 'Z'), range(1, 9));
        //$word = array_merge($word, range(0, 9));
        shuffle($word);
        return substr(implode($word), 0, $len);
    }

    function captchaCode(){
    
        $config = array(
            'word'          => $this->getRandomWord(6),
            'img_path'      => FCPATH . 'assets/captcha_imgs/',
            'img_url'       => base_url() . 'assets/captcha_imgs/',
            'font_path'     => FCPATH . 'assets/fonts/SIXTY.TTF',
            'img_width'     => '250',
            'img_height'    => 80,
            //'expiration'    => 7200,
            'word_length'   => 10,
            'font_size'     => 30,
            //'img_id'        => 'Imageid',
            //'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
    
            // White background and border, black text and red grid
            'colors'        => array(
                    'background' => array(255, 255, 255),
                    'border' => array(54, 136, 185),
                    'text' => array(0, 0, 0),
                    'grid' => array(54, 136, 185)
            )
        );
        
        $captcha = create_captcha($config);
        
        $this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode', $captcha['word']);    
        
        return $captcha;
    }

    private function encrypt_decrypt($string, $action = 'encrypt')
    {
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
        $secret_iv = '5fgf5HJ5g27'; // user define secret key
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }

    private function sendMail($to, $form_id)
	{
        $to = $to;
        $settings = $this->user_model->getAllSettings();
        $host = $settings->host;
        $port = $settings->port;
        $encryption = $settings->encryption;
        $username = $settings->username;
        $password = $this->encrypt_decrypt($settings->password, 'decrypt');

        $dataFormConfirmation = $this->form_model->getFormConfirmation( (int) $form_id );
        
        $message = '';
        $url = '';
        $mail_from = 'mail@wib.me';
        $mail_from_name = 'Wibday.com';
        $mail_subject = 'New submission #ID : ' . $form_id;
        $mail_message = 'Merci ! votre message a bien été envoyé.';
            
        if( $dataFormConfirmation != '' ){

            $message = $dataFormConfirmation->message;
            $url = $dataFormConfirmation->url;

            if( $dataFormConfirmation->send_email == 1 ){
                if( $dataFormConfirmation->mail_from != '' )
                    $mail_from = $dataFormConfirmation->mail_from;
                else 
                    $mail_from = 'mail@wib.me';
                if( $dataFormConfirmation->mail_from_name != '' )
                    $mail_from_name = $dataFormConfirmation->mail_from_name;
                else 
                    $mail_from_name = 'Wibday.com';
                if( $dataFormConfirmation->mail_subject != '' )
                    $mail_subject = $dataFormConfirmation->mail_subject;
                else 
                    $mail_subject = 'New submission #ID : ' . $form_id;
                if( $dataFormConfirmation->mail_message != '' )
                    $mail_message = $dataFormConfirmation->mail_message;
                else 
                    $mail_message = 'Merci ! votre message a bien été envoyé.';
            }
        }

	    $mail = new PHPMailer(true);
	    try {
	        // $message = 'Mail send';
	        //Server settings
			// $mail->SMTPDebug = SMTP::DEBUG_SERVER;
			$mail->isSMTP();
			$mail->Host       = $host;
			$mail->SMTPAuth   = true;
			$mail->Username   = $username;
			$mail->Password   = $password;
			$mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
			$mail->Port       = $port;

			$mail->isHTML(true);   
			$mail->CharSet = "utf-8";
			$mail->setFrom('infos@wibday.com',  'Wibday.com');
			$mail->addReplyTo($mail_from, $mail_from_name);
			$mail->addAddress($to);
			$mail->addBCC('ysf.elagy@gmail.com', 'Youssef');
			$mail->Subject = $mail_subject;
			$mail->Body    = $mail_message;
			$mail->send();
			// if( $mail->send() )
			    // echo true;

		} catch (Exception $e) {
			// echo false;
			// echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
		}
	    
	}
    
    private static function jsonToDebug($jsonText = '')
    {
        $arr = json_decode($jsonText, true);
        $html = "";
        if ($arr && is_array($arr)) {
            $html .= self::_arrayToHtmlTableRecursive($arr);
        }
        return $html;
    }

    private static function _arrayToHtmlTableRecursive($arr) {
        $str = "<table><tbody>";
        foreach ($arr as $key => $val) {
            $str .= "<tr>";
            // $str .= "<td>$key</td>";
            $str .= "<td>";
            if (is_array($val)) {
                if (!empty($val)) {
                    $str .= self::_arrayToHtmlTableRecursive($val);
                }
            } else {
                $str .= "<strong>$val</strong>";
            }
            $str .= "</td></tr>";
        }
        $str .= "</tbody></table>";

        return $str;
    }
    
    private function newSubmission($to, $form_id, $json)
	{
        $to = $to;
        $settings = $this->user_model->getAllSettings();
        $host = $settings->host;
        $port = $settings->port;
        $encryption = $settings->encryption;
        $username = $settings->username;
        $password = $this->encrypt_decrypt($settings->password, 'decrypt');

        $dataFormInfo = $this->form_model->getFormInfo( (int) $form_id );
        
        $message = '';
        $url = '';
        $mail_from = 'mail@wib.me';
        $mail_from_name = 'Wibday.com';
        $mail_subject = 'New submission #ID Formulaire : ' . $form_id;
        
        $tableHtml = $this->jsonToDebug($json);
        $mail_message = 'Vous avez reçu un nouveau Lead Nom du formulaire : <b>' . $dataFormInfo->name . '</b>' . '<br>';
        $mail_message .= $tableHtml;

	    $mail = new PHPMailer(true);
	    try {
	        // $message = 'Mail send';
	        //Server settings
			// $mail->SMTPDebug = SMTP::DEBUG_SERVER;
			$mail->isSMTP();
			$mail->Host       = $host;
			$mail->SMTPAuth   = true;
			$mail->Username   = $username;
			$mail->Password   = $password;
			$mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
			$mail->Port       = $port;

			$mail->isHTML(true);   
			$mail->CharSet = "utf-8";
			$mail->setFrom('infos@wibday.com',  'Wibday.com');
			$mail->addReplyTo($mail_from, $mail_from_name);
			$mail->addAddress($to);
			$mail->addBCC('ysf.elagy@gmail.com', 'Youssef');
			$mail->Subject = $mail_subject;
			$mail->Body    = $mail_message;
			$mail->send();
			// if( $mail->send() )
			    // echo true;

		} catch (Exception $e) {
			// echo false;
			// echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
		}
	    
	}
    
    private function confirm_email($to, $data)
	{
        $to = $to;
        $settings = $this->user_model->getAllSettings();
        $host = $settings->host;
        $port = $settings->port;
        $encryption = $settings->encryption;
        $username = $settings->username;
        $password = $this->encrypt_decrypt($settings->password, 'decrypt');
		
        $mail_from = 'mail@wib.me';
        $mail_from_name = 'Wibday.com';
        $mail_subject = 'Wibday.com | Veuillez confirmer l\'abonnement';
		
        $mail_message = $this->load->view('email/confirm-email', $data, true);

	    $mail = new PHPMailer(true);
	    try {
	        //Server settings
			// $mail->SMTPDebug = SMTP::DEBUG_SERVER;
			$mail->isSMTP();
			$mail->Host       = $host;
			$mail->SMTPAuth   = true;
			$mail->Username   = $username;
			$mail->Password   = $password;
			$mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
			$mail->Port       = $port;

			$mail->isHTML(true);   
			$mail->CharSet = "utf-8";
			$mail->setFrom('infos@wibday.com',  'Wibday.com');
			$mail->addReplyTo($mail_from, $mail_from_name);
			$mail->addAddress($to);
			$mail->addBCC('ysf.elagy@gmail.com', 'Youssef');
			$mail->Subject = $mail_subject;
			$mail->Body    = $mail_message;
			$mail->send();
			// if( $mail->send() )
			    // echo true;

		} catch (Exception $e) {
			// echo false;
			// echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
		}
	    
	}

	public function send()
	{
        if( isset($_POST) ){

            $form_id = $_POST['form_id'];
            unset($_POST['form_id']);

            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            
            $dataForm = $this->form_model->getFormInfo( (int) $form_id );
            $dataFormConfirmation = $this->form_model->getFormConfirmation( (int) $form_id );
            
			//generate simple random code
			$set = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$code = substr(str_shuffle($set), 0, 12);
			
            $date = strtotime(date("Y-m-d H:i:s"));
            $email = '';
            $insert['form_id'] = $form_id;
            $insert['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
            $insert['ip'] = $ip;
            $insert['created_by'] = 1;
            $insert['updated_by'] = 1;
            $insert['created_at'] = $date;
            $insert['updated_at'] = $date;
			$insert['code'] = $code;
			$insert['active'] = false;
            $data = [];
            foreach( $_POST as $key => $val ){

                if( $this->validateEmail($val) == true )
                    $email = $val;

                if( is_array($val) ){
                    $n_val = implode(', ', $val);
                    $data[$key] = $n_val;
                }
                else {
                    $n_val = $val;
                    $data[$key] = $n_val;
                }
            }
			unset($data['captcha']);
            $dataJson = json_encode($data);
			$insert['data'] = $dataJson;
			
			$queryIps = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
			$queryIapsJson = json_encode($queryIps); 
            
			if($queryIps)
				$insert['dataInfoIp'] = $queryIpsJson;
			else 
				$insert['dataInfoIp'] = '';
			
            // print_r($insert);
            $inputCaptcha = $this->input->post('captcha');
            $sessCaptcha = $this->session->userdata('captchaCode');

            $sql = " SELECT * FROM `form_submission` WHERE form_id = $form_id ";
		    $getC = $this->db->query( $sql )->result_array();
			// echo count($getC);
			
            if($inputCaptcha === $sessCaptcha){

                if( $dataForm->total_limit == 0 ){
                    if( $this->db->insert('form_submission', $insert) ){
                        $insert_id = $this->db->insert_id();
                        $this->newSubmission('youssef@wib.me', $form_id, $dataJson);
                        
						$dataView['url'] = base_url()."view/activate/".$insert_id."/".$code;
                        if( $dataFormConfirmation->send_email == 1 ){
                            if( $email != '' ){
								$this->sendMail($email, $form_id);
								$dataView['url'] = base_url()."view/activate/".$insert_id."/".$code;
								if( $dataForm->validation == 1 )
									$this->confirm_email($email, $dataView);
							}
                        }
						if( $email != '' ){
							if( $dataForm->validation == 1 )
								$this->confirm_email($email, $dataView);
						}
							
                        echo $insert_id;
                    }
                    else
                        echo 'error';
                }
                else {
                    if( count($getC) < $dataForm->total_limit ){
						// print_r($insert);
                        if( $this->db->insert('form_submission', $insert) ){
                            $insert_id = $this->db->insert_id();
                            $this->newSubmission('youssef@wib.me', $form_id, $dataJson);
                            
							$dataView['url'] = base_url()."view/activate/".$insert_id."/".$code;
                            if( $dataFormConfirmation->send_email == 1 ){
                                if( $email != '' ){
									$this->sendMail($email, $form_id);
									$dataView['url'] = base_url()."view/activate/".$insert_id."/".$code;
									if( $dataForm->validation == 1 )
										$this->confirm_email($email, $dataView);
								}
                            }
							if( $email != '' ){
								if( $dataForm->validation == 1 )
									$this->confirm_email($email, $dataView);
							}
                            echo $insert_id;
                        }
                        else
                            echo 'error';
                    }
                    else 
                        echo 'maxlimit';
                }

            }
            else 
                echo 'captcha';
            // echo $email . '<br>';
            // echo json_encode($data);
        }
        else
            echo 'error';

        // redirect('https://wibday.com/');
    }
	
	public function activate(){
		$id =  $this->uri->segment(3);
		$code = $this->uri->segment(4);
		
		$return_message = "";
		//if code matches
		if( isset($id) && isset($code) ){
			
			//fetch submission details
			
			$sql = " SELECT * FROM `form_submission` WHERE id = $id and active = '0' ";
			$submission = $this->db->query( $sql )->result_array();
			
			if( count($submission) > 0 || !isset($id) ){
				if($submission[0]['code'] == $code){
					//update submission active status
					$data['active'] = true;
					
					$this->db->where('id', $id);
					$query = $this->db->update('form_submission', $data);
					if($query){
						
						$data['msg'] = 'Email activated successfully';
						$return_message = $this->load->view('email/return-email', $data, true);
						echo $return_message;
					}
					else{
						$data['msg'] = 'Something went wrong in activating email';
						$return_message = $this->load->view('email/return-email', $data, true);
						echo $return_message;
					}
				}
				else{
					$data['msg'] = 'Cannot activate email. Code didnt match';
					$return_message = $this->load->view('email/return-email', $data, true);
					echo $return_message;
				}
			}
			else{
				$data['msg'] = 'No items found';
				$return_message = $this->load->view('email/return-email', $data, true);
				echo $return_message;
			}
			// redirect('register');
		}
		else {
			$data['msg'] = 'Error 404';
			$return_message = $this->load->view('email/return-email', $data, true);
			echo $return_message;
		}
	}
	
    public function refresh(){
        
        $captcha = $this->captchaCode();
        
        echo $captcha['image'];
    }

    public function form( $slug = NULL )
	{
        
        $this->load->helper('captcha');

        if( $slug == NULL )
            redirect('https://wibday.com/');

        $this->data['dataForm'] = $datafrom = $this->form_model->getFormBySlug( $slug );

        if( $datafrom == false )
            redirect('https://wibday.com/');

        $theme = $this->db->query( " SELECT * FROM `theme` WHERE id = $datafrom->theme_id " )->result_array();
        if( count($theme) > 0 )
            $this->data['dataTheme'] = $theme;
        else 
            $this->data['dataTheme'] = '';
            
        $form_confirmation = $this->db->query( " SELECT * FROM `form_confirmation` WHERE form_id = $datafrom->form_id " )->result_array();
        if( count($form_confirmation) > 0 ){
            
            if( !empty($form_confirmation[0]['message']) ){
                $this->data['rMessage'] = $form_confirmation[0]['message'];
            }
            else {
                $this->data['rMessage'] = '<b>Merci !</b> votre message a bien été envoyé.';
            }
            
            if( !empty($form_confirmation[0]['url']) ){
                $this->data['rUrl'] = $form_confirmation[0]['url'];
            }
            else {
                $this->data['rUrl'] = '';
            }

        }
        else {
            $this->data['rMessage'] = '<b>Merci !</b> votre message a bien été envoyé.';
            $this->data['rUrl'] = '';
        }


        $this->data['actual_link'] = $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        $captcha = $this->captchaCode();
        
        $this->data['captchaImg'] = $captcha['image'];

        $this->load->view('form/index', $this->data);

    }

}