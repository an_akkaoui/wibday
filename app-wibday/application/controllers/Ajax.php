<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
class Ajax extends MY_Controller {     
    
    public function __construct(){         
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('User_model', 'user_model', TRUE);
        $this->load->model('Form_model', 'form_model', TRUE);
        $this->load->model('Theme_model', 'theme_model', TRUE);
    }
 
    public function index()
    {
        die('404');
    }
    
    function slug(){
        $slug = 'Formulaire sans nom';
        echo $this->create_unique_slug($slug, 'form');
    }
    private function create_unique_slug($string, $table, $field='slug', $key = NULL, $value = NULL)
    {
        $t =& get_instance();
        $slug = url_title($string);
        $slug = strtolower($slug);
        $i = 0;
        $params = array ();
        $params[$field] = $slug;
     
        if($key)$params["$key !="] = $value; 
     
        while ($t->db->where($params)->get($table)->num_rows())
        {   
            if (!preg_match ('/-{1}[0-9]+$/', $slug ))
                $slug .= '-' . ++$i;
            else
                $slug = preg_replace ('/[0-9]+$/', ++$i, $slug );
             
            $params [$field] = $slug;
        }   
        return $slug;   
    }
    
    function getCityByIp(){
        $ip = '41.251.115.57'; 
        $query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
        $query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
        print_r($query);
        die($query['city']);
        if($query && $query['status'] == 'success') {
        echo 'My IP: '.$query['query'].', '.$query['isp'].', '.$query['org'].', '.$query ['country'].', '.$query['regionName'].', '.$query['city'].'!';
        } else {
        echo 'Unable to get location';
        }
    }
    
    public function deletCheckedSubmissions(){
        // print_r($_POST); die();
        if( isset($_POST['deleteinput']) && $_POST['deleteinput'] != '' ){
            $ids = $_POST['deleteinput'];
            if( $this->db->query(" DELETE FROM `form_submission` WHERE `id` IN ($ids) ") )
                echo true;
            else 
                echo false;
        }
        else 
            echo false;
        // 
    }
    
    public function form_update()
    {
        echo $this->form_model->updateFormInfo($_POST);
    }

    private function _do_upload()
    {
        $config['upload_path']          = './assets/images';
        $config['allowed_types']        = 'jpg|jpeg|png|gif';
        $config['max_size']             = 100;
        $config['max_width']            = 1000;
        $config['max_height']           = 1000;
        $config['file_name']            = round(microtime(true) * 1000);
 
        $this->load->library('upload', $config);
 
        if(!$this->upload->do_upload('file')) //upload and validate
        {
            $data['inputerror'][] = 'file';
            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
            $data['status'] = FALSE;
            echo json_encode($data);
            exit();
        }
        return $this->upload->data('file_name');
    }

    public function upload_image()  
    {  
        if(!empty($_FILES["file"]["name"]))
        {
            
            // if($this->input->post('remove_photo')) // if remove photo checked
            // {
                if(file_exists('./assets/images/'.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
                    unlink('./assets/images/'.$this->input->post('remove_photo'));
            // }

            $upload = $this->_do_upload();

            $column = $this->input->post('column');
            $form_id = (int) $this->input->post('form_id');
            $image = $upload;

            $result= $this->form_model->save_upload($form_id,$image,$column);
            echo '<img src="'.base_url().'assets/images/'.$image.'" width="300" height="225" class="img-thumbnail" /> <input type="hidden" class="remove_photo" name="remove_photo" value="'.$image.'"> <br>L\'ancienne image sera supprimée définitivement lors du nouveau téléchargement';
        }
    }

    public function updateFormConfirmation()  
    {  
        $data = [];
        if( isset($_POST) )
        {
            // print_r($_POST); die();
            if( empty($this->input->post('message')) )
                $message = '';
            else 
                $message = str_replace("'", '’', $this->input->post('message'));
            if( empty($this->input->post('url')) )
                $url = '';
            else 
                $url = $this->input->post('url');
            if($this->input->post('send_email') == 'on'){
                $sendMail = true;
                $mail_from = $this->input->post('mail_from');
                $mail_from_name = $this->input->post('mail_from_name');
                $mail_subject = $this->input->post('mail_subject');
                $mail_message = $this->input->post('mail_message');
            }
               
            else {
                $sendMail = false;
                $mail_from = NULL;
                $mail_from_name = NULL;
                $mail_subject = NULL;
                $mail_message = NULL;
            }
                

            $data['message'] = $message;
            $data['url'] = $url;
            $data['form_id'] = $this->input->post('form_id');
            $data['send_email'] = $sendMail;
            $data['mail_from'] = $mail_from;
            $data['mail_from_name'] = $mail_from_name;
            $data['mail_subject'] = $mail_subject;
            $data['mail_message'] = $mail_message;

            $result = $this->form_model->save_FormConfirmation($data);
            if($result) 
                echo 1;
            else 
                echo 0;
        }
        else 
            echo 0;
    }

    private function calculate_time_span($seconds)
	{  
		$year = floor($seconds / 31556926);
		$months = floor($seconds / 2629743);
		$week = floor($seconds / 604800);
		$day = floor($seconds / 86400); 
		$hours = floor($seconds / 3600);
		$mins = floor(($seconds - ($hours*3600)) / 60); 
		$secs = floor($seconds % 60);
		if($seconds < 60) $time = $secs." seconds";
		else if($seconds < 3600 ) $time = ($mins == 1) ? $mins."min" : $mins." mins";
		else if($seconds < 86400) $time = ($hours == 1) ? $hours." heure" : $hours." heures";
		else if($seconds < 604800) $time = ($day == 1) ? $day." jour" : $day." jours";
		else if($seconds < 2629743) $time = ($week == 1) ? $week." semaine" : $week." semaines";
		else if($seconds < 31556926) $time = ($months == 1) ? $months." mois" : $months." mois";
		else $time = ($year==1)? $year." an":$year." ans";
		return $time; 
	}

    public function getRuleVariables($form_id = null)
    {
        if( $form_id != null ){
            $ID = (int) $form_id;
            $posts = $this->db->query( " SELECT * FROM `form` WHERE form_id = $ID " )->result_array();

            $placeholder = '';

            $data = [];
            if (!empty($posts[0]['json'])) {
                if (is_string($posts[0]['json'])) {
                    $data = json_decode($posts[0]['json'], true);
                    
                    $dataForm = [];
                    $dataForms = [];
                    
                    foreach($data as $keys){
                        $label = '';
                        $name = '';
                        foreach($keys as $k => $v){

                            if( !in_array($keys['type'], array('header','hidden','paragraph')) && $keys['type'] != '' && !empty($keys['type']) ){
                                
                                if( $k == 'label' )
                                    $label = $v;
                                    
                                if( $k == 'name' )
                                    $name = $v;

                                if( $label == '')
                                    $label = $name;
                                
                                $dataForm['name'] = $name;
                                $dataForm['label'] = $label;
                            }
                        }
                        $dataForms[$name] = $dataForm;
                    }
                } else {
                    $data = $posts[0]['json'];
                }
            }

            $submissions = $this->db->query( " SELECT * FROM `form_submission` WHERE form_id = $ID " )->result_array();
            
            $allDataSubmission = [];
            foreach ($submissions as $submission) {
                
                $dataSubmission = [];

                if (!empty($submission['data'])) {
                    if (is_string($submission['data'])) {
                        $dataSubmission = json_decode($submission['data'], true);
                        $dataSubmission['ID'] = $submission['id'];
                    } else {
                        $dataSubmission = $submission['data'];
                    }
                }
                $allDataSubmission[] = $dataSubmission;
            }
            
            print_r($dataForms);
            print_r($allDataSubmission);
            
            $newAllDataSubmission = [];
            
            for( $s = 0; $s < count($allDataSubmission); $s++){
                
                $tab = [];
                
                foreach($dataForms as $d => $t){
                    $allnewdatasub = [];
                    if( isset($allDataSubmission[$s][$d]) &&  !empty($d) ){
                        $tab[$d] = $allDataSubmission[$s][$d];
                    }
                    else{
                        $tab[$d] = '';
                    }
                }

                $newAllDataSubmission[$s] = $tab;
            }

            $dataForms['submission'] = $newAllDataSubmission;
            //print_r($dataForms);
        }
        else 
            die('noIdFind');
    }

    public function dataform()
    {
		$submissions = $this->db->query( " SELECT * FROM `form_submission` WHERE form_id = 1 " )->result_array();
        
        $allDataSubmission = [];
        foreach ($submissions as $submission) {
            
            $dataSubmission = [];

            if (!empty($submission['data'])) {
                if (is_string($submission['data'])) {
                    $dataSubmission = json_decode($submission['data'], true);
                } else {
                    $dataSubmission = $submission['data'];
                }
            }
            $allDataSubmission[] = $dataSubmission;
        }
        
        print_r($allDataSubmission);
    }

    public function forms_list()
    {
        $list = $this->form_model->get_datatables();
        // print_r($list);  die('vdvd');
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $form) {
            $no++;
            $row = array();
            
            if( $form->status == 1 ){
                $status = '<td><span class="badge badge-success">Actif</span></td>';
				$lvf = '<a class="dropdown-item" href="'.site_url().$form->slug.'.html" title="Supprimer" target="_blank"><i class="fa fa-dribbble fa-lg"></i> Lien vers votre formulaire</a>';
            }
			else{
                $status = '<td><span class="badge badge-danger">Inactif</span></td>';
				$lvf = '';
			}
			
            $users = $this->user_model->getUserInfo($form->created_by);
            if($users)
                $userName = $users->first_name. ' ' .$users->last_name;
            else 
                $userName = 'NULL';

            if($form->updated_at == '0000-00-00 00:00:00')
                $date = time() - strtotime($form->created_at);
            else 
                $date = time() - strtotime($form->updated_at);

            $row[] = $form->form_id;
            $row[] = $form->name;
            $row[] = $userName;
            $row[] = $status;
            $row[] = $this->calculate_time_span($date);
            
            //add html for action 
            $row[] = ' <a class="btn" href="'.site_url().'forms/create/'.$form->form_id.'" title="Modifier"><i class="fa fa-pencil-square-o fa-lg"></i></a>
			<div class="btn-group">
            <button class="btn btn-success btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Action
            </button>
            <div class="dropdown-menu">
				<a class="dropdown-item" href="'.site_url().'forms/submissions/'.$form->form_id.'" title="Listes des inscriptions"><i class="fa fa-list fa-lg"></i> Inscriptions</a>
				
				<a class="dropdown-item" href="'.site_url().'forms/deleteform/'.$form->form_id.'" title="Supprimer" onclick="return confirm(\'Etes-vous sûr que vous voulez supprimer?\');"><i class="fa fa-trash-o fa-lg"></i> Supprimer</a>
				'.$lvf.'
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="'.site_url().'forms/duplicate/'.$form->form_id.'" title="Dupliquer" onclick="return confirm(\'Etes-vous sûr?\');"><i class="fa fa-copy fa-lg"></i> Dupliquer</a>
            </div>
          </div>';
         
            $data[] = $row;
        }
 
        $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->form_model->count_all(),
                "recordsFiltered" => $this->form_model->count_filtered(),
                "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function themes_list()
    {
        $list = $this->theme_model->get_datatables();
        // print_r($list);  die('vdvd');
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $form) {
            $no++;
            $row = array();
            
            if( $form->status == 1 )
                $status = '<td><span class="badge badge-success">Actif</span></td>';
            else 
                $status = '<td><span class="badge badge-danger">Inactif</span></td>';

            $users = $this->user_model->getUserInfo($form->created_by);
            if($users)
                $userName = $users->first_name. ' ' .$users->last_name;
            else 
                $userName = 'NULL';

            if($form->updated_at == '0000-00-00 00:00:00')
                $date = time() - strtotime($form->created_at);
            else 
                $date = time() - strtotime($form->updated_at);

            $row[] = $form->id;
            $row[] = $form->name;
            $row[] = '<span class="badge" style="background-color: '.$form->color.'">&nbsp;</span> <code>' . $form->color . '</code>';
            $row[] = $userName;
            $row[] = $status;
            $row[] = $this->calculate_time_span($date);
            
            //add html for action 
            $row[] = '
            <a class="btn btn-sm btn-primary" href="'.site_url().'themes/update/'.$form->id.'" title="Modifier"><i class="fa fa-pencil-square-o fa-lg"></i></a>
                  <a class="btn btn-sm btn-danger" href="'.site_url().'themes/deletetheme/'.$form->id.'" title="Supprimer" onclick="return confirm(\'Etes-vous sûr que vous voulez supprimer?\');"><i class="fa fa-trash-o fa-lg"></i></a>';
         
            $data[] = $row;
        }
 
        $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->form_model->count_all(),
                "recordsFiltered" => $this->form_model->count_filtered(),
                "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    private function encrypt_decrypt($string, $action = 'encrypt')
    {
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
        $secret_iv = '5fgf5HJ5g27'; // user define secret key
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }
    function encrypt_decrypto(){
        $pwd = $this->encrypt_decrypt('spaceo', 'encrypt');
        echo "Your Encrypted password is = ". $pwd . '<br>';
        echo "Your Decrypted password is = ". $this->encrypt_decrypt('NlpuVmZCSjZ4b1JuQVpPakVGdzk4QT09', 'decrypt');
    }
    public function users_list()
    {
        
        $rolename = array('1'=>'Admin','2'=>'Editor','3'=>'Author','4'=>'Subscriber');
        $list = $this->user_model->get_datatables();
        // print_r($list);  die('vdvd');
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $person) {
            $no++;
            $row = array();
            $row[] = $person->last_name;
            $row[] = $person->first_name;
            $row[] = $person->email;
            $row[] = $person->last_login;
            $row[] = $rolename[$person->role];
            if( $person->status == 'approved' )
                $status = '<td><span class="badge badge-success">Actif</span></td>';
            else 
                $status = '<td><span class="badge badge-warning">En attente</span></td>';
            $row[] = $status;
            
            if( $person->banned_users == 'ban' ){

                $ban = '<a class="btn btn-sm btn-success" href="'.site_url().'main/banuser/'.$person->id.'/unban" title="unban" onclick="return confirm(\'Etes-vous sûr?\');"><i class="fa fa-check fa-lg"></i></a>';
            }
            else{
                $ban = '<a class="btn btn-sm btn-warning" href="'.site_url().'main/banuser/'.$person->id.'/ban" title="ban" onclick="return confirm(\'Etes-vous sûr?\');"><i class="fa fa-ban fa-lg"></i></a>';
            }
            //add html for action 
            $row[] = '<a class="btn btn-sm btn-primary" href="'.site_url().'main/updateuser/'.$person->id.'" title="Modifier"><i class="fa fa-pencil-square-o fa-lg"></i></a>
                  <a class="btn btn-sm btn-danger" href="'.site_url().'main/deleteuser/'.$person->id.'" title="Supprimer" onclick="return confirm(\'Etes-vous sûr que vous voulez supprimer?\');"><i class="fa fa-trash-o fa-lg"></i></a>
                  '.$ban.'';
         
            $data[] = $row;
        }
 
        $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->user_model->count_all(),
                "recordsFiltered" => $this->user_model->count_filtered(),
                "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
 
    public function ajax_edit($id)
    {
        $data = $this->blog->get_by_id($id);
        //$data->dob = ($data->dob == '0000-00-00') ? '' : $data->dob; // if 0000-00-00 set tu empty for datepicker compatibility
        echo json_encode($data);
    }
 
    public function ajax_add()
    {
        $this->load->helper('url');
        $this->load->helper('text');
        $this->_validate();
        
        $slug = url_title(convert_accented_characters($this->input->post('titre')), 'dash', true);
        
        $data = array(
                'titre' => $this->input->post('titre'),
                'slug_titre' => $slug,
                'description' => $this->input->post('description'),
        );
 
        if(!empty($_FILES['photo']['name']))
        {
            $upload = $this->_do_upload();
            $data['photo'] = $upload;
        }
 
        $insert = $this->blog->save($data);
 
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_update()
    {
        $this->load->helper('url');
        $this->load->helper('text');
        $this->_validate();
        
        // print_r($this->input);
        $slug = url_title(convert_accented_characters($this->input->post('titre')), 'dash', true);
        
        $datenow = date("Y-m-d H:i:s");
        $data = array(
                'titre' => $this->input->post('titre'),
                'slug_titre' => $slug,
                'description' => $this->input->post('description'),
                'date_update' => $datenow,
        );
 
        if($this->input->post('remove_photo')) // if remove photo checked
        {
            if(file_exists('uploads/'.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
                unlink('uploads/'.$this->input->post('remove_photo'));
            $data['photo'] = '';
        }
 
        if(!empty($_FILES['photo']['name']))
        {
            $upload = $this->_do_upload();
             
            //delete file
            $person = $this->blog->get_by_id($this->input->post('id'));
            if(file_exists('uploads/'.$person->photo) && $person->photo)
                unlink('uploads/'.$person->photo);
 
            $data['photo'] = $upload;
        }
 
        $this->blog->update(array('id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }
    
    public function ajax_delete($id)
    {
        //delete file
        $person = $this->blog->get_by_id($id);
        $data = array('etat' => 0);
        $this->blog->disabled_by_id(array('id' => $id), $data);
        echo json_encode(array("status" => TRUE));
    }
    
    /*public function ajax_delete($id)
    {
        //delete file
        $person = $this->blog->get_by_id($id);
        if(file_exists('uploads/'.$person->photo) && $person->photo)
            unlink('uploads/'.$person->photo);
         
        $this->blog->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }*/
 
    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('titre') == '')
        {
            $data['inputerror'][] = 'titre';
            $data['error_string'][] = 'Titre is required';
            $data['status'] = FALSE;
        }
 
        if($this->input->post('description') == '')
        {
            $data['inputerror'][] = 'description';
            $data['error_string'][] = 'Description is required';
            $data['status'] = FALSE;
        }
 
        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
 
}