<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Themes extends MY_Controller {

    public $status;
    public $roles;

    function __construct(){
        parent::__construct();
        $this->load->model('User_model', 'user_model', TRUE);
        $this->load->model('Form_model', 'form_model', TRUE);
        $this->load->model('Theme_model', 'theme_model', TRUE);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->status = $this->config->item('status');
        $this->roles = $this->config->item('roles');
        $this->load->library('userlevel');

        $this->data['dataSession'] = $this->session->userdata;
        // print_r($this->data['dataSession']);
    }
	
    public function index()
	{

        $this->data['css'] = array(
            site_url(). 'app-assets/css/dataTables.bootstrap4.min.css',
        );

        $this->data['scripts'] = array(
            site_url(). 'app-assets/js/jquery.dataTables.min.js',
            site_url(). 'app-assets/js/dataTables.bootstrap4.min.js',
            site_url(). 'app-assets/js/jquery-ui.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.4.0/highlight.min.js',
            '//cdn.jsdelivr.net/npm/sweetalert2@11',
        );

	    $data = $this->session->userdata;
        
        $this->data['themes_list'] = "themes_list";
	    $data['groups'] = $this->user_model->getUserData();

	    //check user level
	    if(empty($data['role'])){
	        redirect(site_url().'main/login/');
	    }
	    
        $this->data['role'] = $data['role'];
	    $data['dataLevel'] = $dataLevel = $this->userlevel->checkLevel($data['role']);
	    //check user level

	    //check is admin or not
	    if($dataLevel == "is_admin" || $dataLevel == "is_editor"){
            $this->layout("themes", "Listes des thèmes");
	    }else{
	        redirect(site_url().'main/');
	    }
    }

    // get submissions list by FORM ID
    private function getSubmissions($form_id = null)
    {
        if( $form_id != null ){
            $ID = (int) $form_id;
            $posts = $this->db->query( " SELECT * FROM `form` WHERE form_id = $ID " )->result_array();

            $data = [];
            if (!empty($posts[0]['json'])) {
                if (is_string($posts[0]['json'])) {
                    $data = json_decode($posts[0]['json'], true);
                    $dataForm = [];
                    $dataForms = [];
                    foreach($data as $keys){
                        $name = '';
                        $label = '';
                        $placeholder = '';
                        foreach($keys as $k => $v){
                            if( !in_array($keys['type'], array('header','hidden','paragraph')) && $keys['type'] != '' && !empty($keys['type']) ){
                                if( $k == 'label' )
                                    $label = $v;
                                    
                                if( $k == 'name' )
                                    $name = $v;

                                if( $k == 'placeholder' )
                                    $placeholder = $v;

                                if( $label == '')
                                    $label = $placeholder;
                                if( $label == '')
                                    $label = $name;
                                
                                $dataForm['name'] = $name;
                                $dataForm['label'] = $label;
                            }
                        }
                        $dataForms['header'][$name] = $dataForm;
                    }
                } else {
                    $data = $posts[0]['json'];
                }
            }

            //$dataForms['header'] = $dataForms;

            $submissions = $this->db->query( " SELECT * FROM `form_submission` WHERE form_id = $ID " )->result_array();
            
            $allDataSubmission = [];
            foreach ($submissions as $submission) {
                
                $ID = $submission['id'];
                $created_at = date("d/m/Y H:i:s",$submission['created_at']);
                $dataSubmission = [];

                if (!empty($submission['data'])) {
                    if (is_string($submission['data'])) {
                        $dataSubmission = json_decode($submission['data'], true);
                        $dataSubmission['ID'] = $ID;
                        $dataSubmission['created_at'] = $created_at;
                    } else {
                        $dataSubmission = $submission['data'];
                    }
                }
                $allDataSubmission[] = $dataSubmission;
            }
            
            // print_r($dataForms);
            //print_r($allDataSubmission);
            
            $newAllDataSubmission = [];
            
            for( $s = 0; $s < count($allDataSubmission); $s++){
                
                $tab['ID'] = $allDataSubmission[$s]['ID'];
                
                foreach($dataForms['header'] as $d => $t){
                    if( isset($allDataSubmission[$s][$d]) && $d != ''){
                        $tab[$d] = $allDataSubmission[$s][$d];
                    }
                    else{
                        $tab[$d] = '';
                    }
                }
                $tab['created_at'] = $allDataSubmission[$s]['created_at'];
                $newAllDataSubmission[$s] = $tab;
            }

            $dataForms['submission'] = $newAllDataSubmission;
            
            return $dataForms;
        }
        else 
            die('noIdFind');
    }

    // list submissions by ID
    public function submissions( $form_id = NULL ){

        $this->data['css'] = array(
            site_url(). 'app-assets/css/dataTables.bootstrap4.min.css',
            'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css',
            'https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css',
            'https://cdn.datatables.net/buttons/2.0.1/css/buttons.bootstrap4.min.css',
        );

        $this->data['scripts'] = array(
            site_url(). 'app-assets/js/jquery.dataTables.min.js',
            site_url(). 'app-assets/js/dataTables.bootstrap4.min.js',
            site_url(). 'app-assets/js/jquery-ui.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.4.0/highlight.min.js',
            'https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js',
            'https://cdn.datatables.net/buttons/2.0.1/js/buttons.bootstrap4.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js',
            'https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js',
            'https://cdn.datatables.net/buttons/2.0.1/js/buttons.colVis.min.js',
        );

        if($form_id == NULL)
            redirect(site_url().'forms/');

	    $data = $this->session->userdata;
        
        $this->data['submissions_list'] = "submissions_list";
        $this->data['dataForm'] = $this->form_model->getFormInfo( (int) $form_id );
	    $this->data['dataForms'] = $this->getSubmissions( (int) $form_id );
	    //check user level
	    if(empty($data['role'])){
	        redirect(site_url().'main/login/');
	    }
	    
	    
        $this->data['role'] = $data['role'];
        $this->data['form_id'] = $form_id;
	    $dataLevel = $this->userlevel->checkLevel($data['role']);
	    //check user level

	    //check is admin or not
	    if($dataLevel == "is_admin"){
            $this->layout("submissions", "Listes des inscriptions");
	    }else{
	        redirect(site_url().'main/');
	    }

    }

    
    // get submissions list by FORM ID & Submissions ID
    private function getSubmissionView( $submission_id = NULL, $form_id = null )
    {
        if( $form_id != null ){
            $ID = (int) $form_id;
            $posts = $this->db->query( " SELECT * FROM `form` WHERE form_id = $ID " )->result_array();

            $data = [];
            if (!empty($posts[0]['json'])) {
                if (is_string($posts[0]['json'])) {
                    $data = json_decode($posts[0]['json'], true);
                    $dataForm = [];
                    $dataForms = [];
                    foreach($data as $keys){
                        $name = '';
                        $label = '';
                        $placeholder = '';
                        foreach($keys as $k => $v){
                            if( !in_array($keys['type'], array('header','hidden','paragraph')) && $keys['type'] != '' && !empty($keys['type']) ){
                                if( $k == 'label' )
                                    $label = $v;
                                    
                                if( $k == 'name' )
                                    $name = $v;

                                if( $k == 'placeholder' )
                                    $placeholder = $v;

                                if( $label == '')
                                    $label = $placeholder;
                                if( $label == '')
                                    $label = $name;
                                
                                $dataForm['name'] = $name;
                                $dataForm['label'] = $label;
                            }
                        }
                        $dataForms['header'][$name] = $dataForm;
                    }
                } else {
                    $data = $posts[0]['json'];
                }
            }

            //$dataForms['header'] = $dataForms;

            $submissions = $this->db->query( " SELECT * FROM `form_submission` WHERE form_id = $ID AND id = $submission_id " )->result_array();
            
            $allDataSubmission = [];
            foreach ($submissions as $submission) {
                
                $ID = $submission['id'];
                $dataSubmission = [];

                if (!empty($submission['data'])) {
                    if (is_string($submission['data'])) {
                        $dataSubmission = json_decode($submission['data'], true);
                        $dataSubmission['ID'] = $ID;
                    } else {
                        $dataSubmission = $submission['data'];
                    }
                }
                $allDataSubmission[] = $dataSubmission;
            }
            
            // print_r($dataForms);
            //print_r($allDataSubmission);
            
            $newAllDataSubmission = [];
            
            for( $s = 0; $s < count($allDataSubmission); $s++){
                
                $tab['ID'] = $allDataSubmission[$s]['ID'];
                
                foreach($dataForms['header'] as $d => $t){
                    if( isset($allDataSubmission[$s][$d]) && $d != ''){
                        $tab[$d] = $allDataSubmission[$s][$d];
                    }
                    else{
                        $tab[$d] = '';
                    }
                }
                
                $newAllDataSubmission[$s] = $tab;
            }

            $dataForms['submission'] = $newAllDataSubmission;
            return $dataForms;
        }
        else 
            die('noIdFind');
    }

    // Get submissionview by ID
    public function submissionview( $submission_id = NULL, $form_id = null ){

        if($submission_id == NULL)
            redirect(site_url().'forms/');

	    $data = $this->session->userdata;
        
        $this->data['submissionview_list'] = "submissionview_list";
	    $this->data['submissioninfo'] = $this->getSubmissionView( (int) $submission_id, (int) $form_id );
	    $this->data['submission_id'] = $submission_id;
	    $this->data['form_id'] = $form_id;
        $dataf = $this->db->query( " SELECT * FROM `form` WHERE form_id = $form_id " )->result_array();
        $this->data['nameFrom'] = $dataf[0]['name'];
        // print_r( $this->data['submissioninfo'] ); die();
        
	    //check user level
	    if(empty($data['role'])){
	        redirect(site_url().'main/login/');
	    }
	    
        $this->data['role'] = $data['role'];
	    $dataLevel = $this->userlevel->checkLevel($data['role']);
	    //check user level

	    //check is admin or not
	    if($dataLevel == "is_admin"){
            $this->layout("submission-view", "Listes des inscriptions");
	    }else{
	        redirect(site_url().'main/');
	    }

    }

    // delete Submission
    public function deleteSubmission($id,$form_id) {
        $data = $this->session->userdata;
        if(empty($data['role'])){
	        redirect(site_url().'main/login/');
	    }
	    $dataLevel = $this->userlevel->checkLevel($data['role']);
	    //check user level

	    //check is admin or not
	    if($dataLevel == "is_admin"){
    		$this->form_model->deleteSubmission($id);
    		if($this->form_model->deleteSubmission($id) == FALSE )
    		{
    		    $this->session->set_flashdata('flash_message', 'Erreur, impossible de supprimer submission!');
    		}
    		else
    		{
    		    $this->session->set_flashdata('success_message', 'La suppression a réussi.');
    		}
    		redirect(site_url().'forms/submissions/'.$form_id);
	    }else{
		    redirect(site_url().'main/');
	    }
    }

    // delete form
    public function deletetheme($id) {
        $data = $this->session->userdata;
        if(empty($data['role'])){
	        redirect(site_url().'main/login/');
	    }
	    $dataLevel = $this->userlevel->checkLevel($data['role']);
	    //check user level

	    //check is admin or not
	    if($dataLevel == "is_admin" || $dataLevel == "is_editor"){
    		$this->theme_model->deleteTheme($id);
    		if($this->theme_model->deleteTheme($id) == FALSE )
    		{
    		    $this->session->set_flashdata('flash_message', 'Erreur, impossible de supprimer le thème!');
    		}
    		else
    		{
    		    $this->session->set_flashdata('success_message', 'La suppression a réussi.');
    		}
    		redirect(site_url().'themes/');
	    }else{
		    redirect(site_url().'main/');
	    }
    }

    //add new theme 
    public function add()
    {
        $this->data['scripts'] = array(
            site_url(). 'app-assets/js/lib/src-min/ace.js',
            site_url(). 'app-assets/js/lib/src-min/theme-clouds.js',
            site_url(). 'app-assets/js/lib/src-min/mode-css.js',
        );

        $data = $this->session->userdata;

        if(empty($data['role'])){
	        redirect(site_url().'main/login/');
	    }

        //check user level
	    if(empty($data['role'])){
	        redirect(site_url().'main/login/');
	    }
	    
	    
        $this->data['role'] = $data['role'];
        $this->data['idUser'] = $idUser = $data['id'];
	    $this->data['dataLevel'] = $dataLevel = $this->userlevel->checkLevel($data['role']);
	    //check user level
        $this->data['editorTheme'] = '';
	    //check is admin or not
	    if($dataLevel == "is_admin" || $dataLevel == "is_editor"){
            $this->form_validation->set_rules('name', 'Nom', 'required');
            $this->form_validation->set_rules('color', 'Couleur', 'required');

            if ($this->form_validation->run() == FALSE) {
                $this->layout("addtheme", "Ajouter un thème");
            }else{
                $post = $this->input->post(NULL, TRUE);
                $cleanPost = $this->security->xss_clean($post);
                $cleanPost['name'] = $this->input->post('name');
                $cleanPost['color'] = $this->input->post('color');
                $cleanPost['css'] = $this->input->post('css');
                $cleanPost['idUser'] = $idUser;

                //insert to database
                if(!$this->theme_model->addTheme($cleanPost)){
                    $this->session->set_flashdata('flash_message', 'Un problème est survenu lors de l\'ajout d\'un nouvel utilisateur');
                }else{
                    $this->session->set_flashdata('success_message', 'Thème a bien été ajouté.');
                }
                redirect(site_url().'themes/');

            }
	    }else{
	        redirect(site_url().'main/');
	    }
    }

    // Update Theme
	public function update($id = NULL) 
    {
        $this->data['scripts'] = array(
            site_url(). 'app-assets/js/lib/src-min/ace.js',
            site_url(). 'app-assets/js/lib/src-min/theme-clouds.js',
            site_url(). 'app-assets/js/lib/src-min/mode-css.js',
        );

        $data = $this->session->userdata;
        
        $this->data['editorTheme'] = '';

        if(empty($data['role'])){
	        redirect(site_url().'main/login/');
	    }

        $dataInfo = array(
            'firstName'=> $data['first_name'],
            'id'=>$data['id'],
        );

        if( $id == NULL) 
            redirect(site_url().'themes/');
        
        
        $this->data['role'] = $data['role'];
        $this->data['idUser'] = $idUser = $data['id'];
        $this->data['dataLevel'] = $dataLevel = $this->userlevel->checkLevel($data['role']);

        $this->form_validation->set_rules('name', 'Nom', 'required');
        $this->form_validation->set_rules('color', 'Couleur', 'required');
        $this->form_validation->set_rules('css', 'Css', 'required');

        $this->data['groups'] = $groups = $this->theme_model->getThemeInfo( (int) $id );
        if( $groups == false )
            redirect(site_url().'themes/');

        if ($this->form_validation->run() == FALSE) {
            $this->layout("updateTheme", "Modification d'un thème");
        }else{
            $post = $this->input->post(NULL, TRUE);
            
            $cleanPost = $this->security->xss_clean($post);
            $cleanPost['idUser'] = $idUser;
            $cleanPost['theme_id'] = $groups->id;
            $cleanPost['name'] = $this->input->post('name');
            $cleanPost['color'] = $this->input->post('color');
            $cleanPost['css'] = $this->input->post('css');
            // $cleanPost['dateUpdate'] = date("Y-m-d H:i:s");
            // print_r($cleanPost); die();
            if(!$this->theme_model->updateTheme($cleanPost)){
                $this->session->set_flashdata('flash_message', 'Un problème est survenu lors de la mise à jour du thème');
            }else{
                $this->session->set_flashdata('success_message', 'Le thème a été mis a jour.');
            }
            redirect(site_url().'themes/update/'.$id);
        }
    }
}