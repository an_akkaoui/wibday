<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public $data = array();

    public function __construct() {
		
        parent::__construct();
		
		$this->load->library('session');
		$this->load->library('form_validation');
		
    }
	
	
	
    public function layout($middle, $title = null, $description = null, $ogimages = null, $actual_link = null) {
        
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        
        if($actual_link == null)
            $actual_link = $actual_link;
        if($ogimages == null)
            $ogimages = 'https://wspace.ma/assets/img/subscribe-img1.png';
            
        $this->data["breadcrumb"] = $title;
        $this->data["title"] = 'Wibday - ' . $title;
        $this->data["actual_link"] = $actual_link;
        $this->data["description"] = $description;
        $this->data["ogimages"] = $ogimages;
        
        $this->template['header'] = $this->load->view('components/header', $this->data, true);
        $this->template['menu'] = $this->load->view('components/menu', $this->data, true);
        $this->template['sidebar'] = $this->load->view('components/sidebar', $this->data, true);
        $this->template['middle'] = $this->load->view("pages/".$middle, $this->data, true);
        $this->template['asidemenu'] = $this->load->view("components/aside-menu", $this->data, true);
        $this->template['footer'] = $this->load->view('components/footer', $this->data, true);

        $this->load->view('main', $this->template);
    }
	

  

}