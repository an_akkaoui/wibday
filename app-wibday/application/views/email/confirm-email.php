<div marginwidth="0" marginheight="0" style="font:14px/20px 'Helvetica',Arial,sans-serif;margin:0;padding:75px 0 0 0;text-align:center;background-color:#eeeeee">
   <center>
      <table border="0" cellpadding="20" cellspacing="0" height="100%" width="100%" style="background-color:#eeeeee">
         <tbody>
            <tr>
               <td align="center" valign="top">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;border-radius:6px;background-color:none">
                     <tbody>
                        <tr>
                           <td align="center" valign="top">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px">
                                 <tbody>
                                    <tr>
                                       <td>
                                          <h1 style="font-size:30px;line-height:110%;margin-bottom:30px;margin-top:0;padding:0">
                                             <div style="text-align:center"><img src="https://wibday.com/assets/img/logo.png" alt="" border="0" width="300" height="88"></div>
                                          </h1>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td align="center" valign="top">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;border-radius:6px;background-color:#ffffff">
                                 <tbody>
                                    <tr>
                                       <td align="left" valign="top" style="line-height:150%;font-family:Helvetica;font-size:14px;color:#333333;padding:20px">
                                          <h2 style="font-size:22px;line-height:28px;margin:0 0 12px 0">Veuillez confirmer l'abonnement</h2>
                                          <a href="<?= $url ?>" style="color:#ffffff!important;display:inline-block;font-weight:500;font-size:16px;line-height:42px;font-family:'Helvetica',Arial,sans-serif;width:auto;white-space:nowrap;height:42px;margin:12px 5px 12px 0;padding:0 22px;text-decoration:none;text-align:center;border:0;border-radius:3px;vertical-align:top;background-color:#1d90b9!important" rel="noreferrer" target="_blank"><span style="display:inline;font-family:'Helvetica',Arial,sans-serif;text-decoration:none;font-weight:500;font-style:normal;font-size:16px;line-height:42px;border:none;background-color:#1d90b9!important;color:#ffffff!important">Oui, je m'abonne à cette liste.</span></a>
                                          <br>
                                          <div>
                                             <p style="padding:0 0 10px 0">Si vous avez reçu ce message par erreur, effacez-le tout simplement. Vous n'êtes pas inscrit si vous ne cliquez pas sur le lien de confirmation ci-dessus.</p>
                                             <p style="padding:0 0 10px 0">Pour toute question concernant cette liste veuillez contacter :<br>
                                                <a href="mailto:info@wibday.com" style="color:#336699" rel="noreferrer" target="_blank">info@wibday.com</a>
                                             </p>
                                          </div>
                                          <span>
                                          <span content="We need to confirm your email address."></span>
                                          <span>
                                          <span>
                                          </span>
                                          </span>
                                          </span>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td align="center" valign="top">
                              <table border="0" cellpadding="20" cellspacing="0" width="100%" style="max-width:600px">
                                 <tbody>
                                    <tr>
                                       <td align="center" valign="top">
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
         </tbody>
      </table>
   </center>
</div>