<?php
    //check user level
    $dataLevel = $this->userlevel->checkLevel($role);

    $result = $this->user_model->getAllSettings();
    $site_title = $result->site_title;
    //check user level
?>
<div class="sidebar sidebar-pills bg-transparent">
    <nav class="sidebar-nav">
        <ul class="nav">
        <li class="nav-item"><a class="nav-link" href="<?= site_url();?>main/"><i class="nav-icon la la-lg la-dashboard"></i> Dashboard</a></li>
        <li class="nav-title">Components</li>
        
        <?php
            if($dataLevel == 'is_admin'){ //Check user level if is Admin
        ?>
        <li class="nav-item nav-dropdown">
            <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-lg la-users"></i> Utilisateurs</a>
            <ul class="nav-dropdown-items">
                <li class="nav-item">
                    <a class="nav-link" href="<?= site_url();?>main/adduser"><i class="nav-icon la la-lg la-plus"></i> Ajouter</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= site_url();?>main/users"><i class="nav-icon la la-lg la-list"></i> Listes</a>
                </li>
            </ul>
        </li>
		<?php } ?>
		<?php
            if($dataLevel == 'is_admin' || $dataLevel == 'is_editor'){ //Check user level if is Admin
        ?>
			<li class="nav-item nav-dropdown">
				<a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-lg la-th-large"></i> Formulaires</a>
				<ul class="nav-dropdown-items">
					<li class="nav-item">
						<a class="nav-link" href="<?= site_url();?>forms/create/"><i class="nav-icon la la-lg la-plus"></i> Créer</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= site_url();?>forms/"><i class="nav-icon la la-lg la-list"></i> Listes</a>
					</li>
				</ul>
			</li>
			<li class="nav-item nav-dropdown">
				<a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-lg la-ioxhost"></i> Thèmes</a>
				<ul class="nav-dropdown-items">
					<li class="nav-item">
						<a class="nav-link" href="<?= site_url();?>themes/add/"><i class="nav-icon la la-lg la-plus"></i> Ajouter</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= site_url();?>themes/"><i class="nav-icon la la-lg la-list"></i> Listes</a>
					</li>
				</ul>
			</li>
		<?php } ?>
		<?php
            if($dataLevel == 'is_admin'){ //Check user level if is Admin
        ?>
        <li class="nav-title">Paramètres</li>
        <li class="nav-item">
            <a class="nav-link" href="<?= site_url();?>main/settings"><i class="nav-icon icon-settings"></i> Paramètres</a>
        </li>
        <?php } ?>
        </ul>
    </nav>
</div>