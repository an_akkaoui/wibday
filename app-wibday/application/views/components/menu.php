
<?php
    //check user level
    $dataLevel = $this->userlevel->checkLevel($role);

    $result = $this->user_model->getAllSettings();
    $site_title = $result->site_title;
    //check user level
?>
<header class="app-header bg-light border-0 navbar">
      <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto ml-3" type="button" data-toggle="sidebar-show"><span class="navbar-toggler-icon"></span></button><a class="navbar-brand" href="<?= site_url();?>main/"><b>Wib</b><span>day</span></a>
      <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show"><span class="navbar-toggler-icon"></span></button>
      <ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item px-3"><a class="nav-link" href="<?= site_url();?>main/"> <i class="icon-graph"></i> Dashboard</a></li>
        <?php
            if($dataLevel == 'is_admin'){ //Check user level if is Admin
        ?>
        <li class="nav-item px-3"><a class="nav-link" href="<?= site_url();?>main/users"> <i class="icon-user"></i> Users</a></li>
        <li class="nav-item px-3"><a class="nav-link" href="<?= base_url().'main/settings' ?>"> <i class="icon-settings"></i> Settings</a></li>
        <?php } ?>
      </ul>
      <ul class="nav navbar-nav ml-auto">
        <!--<li class="nav-item d-md-down-none"><a class="nav-link" href="#"><i class="icon-bell"></i><span class="badge badge-pill badge-danger">5</span></a></li>
        <li class="nav-item d-md-down-none"><a class="nav-link" href="#"><i class="icon-list"></i></a></li>
        <li class="nav-item d-md-down-none"><a class="nav-link" href="#"><i class="icon-location-pin"></i></a></li>-->
        <li class="nav-item dropdown"><a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><img class="img-avatar" src="<?= base_url() ?>app-assets/img/avatars/6.jpg" alt=""></a>
          <div class="dropdown-menu dropdown-menu-right">
            <div class="dropdown-header text-center"><strong>Compte</strong></div>
              <a class="dropdown-item" href="<?= base_url().'main/updateuser/'.$dataSession['id'] ?>"><i class="fa fa-user"></i> Mon profile</a>
              <a class="dropdown-item" href="<?= base_url().'main/changepassword/'.$dataSession['id'] ?>"><i class="fa fa-lock"></i> Modifier le mot de passe</a>
            <div class="dropdown-header text-center"><strong>Settings</strong></div>
              <a class="dropdown-item" href="<?= base_url().'main/settings' ?>"><i class="fa fa-wrench"></i> Settings</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= base_url().'main/logout' ?>"><i class="fa fa-lock"></i> Logout</a>
          </div>
        </li>
      </ul>
      <!--<button class="navbar-toggler aside-menu-toggler d-md-down-none" type="button" data-toggle="aside-menu-lg-show"><span class="navbar-toggler-icon"></span></button>-->
      <button class="navbar-toggler aside-menu-toggler d-lg-none mr-3" type="button" data-toggle="aside-menu-show"><span class="navbar-toggler-icon"></span></button>
</header>