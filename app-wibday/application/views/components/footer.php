
    <footer class="app-footer">
      <div><a href="http://wibday.com">Wibday.com </a><span>&nbsp;&copy; 2021.</span></div>
      <div class="ml-auto"><span>Powered by </span><a href="https://wib.co">Wib.co</a><span>&nbsp;&&nbsp;</span><a href="https://wibday.com">Wibday.com</a></div>
    </footer>
    <!-- CoreUI and necessary plugins-->
    <script src="<?= base_url() ?>app-assets/vendors/jquery/js/jquery.min.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/popper.js/js/popper.min.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/pace-progress/js/pace.min.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/perfect-scrollbar/js/perfect-scrollbar.min.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/@coreui/coreui/js/coreui.min.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/pnotify/js/Pnotify.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/pnotify/js/PnotifyButtons.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/pnotify/js/PnotifyConfirm.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/pnotify/js/PnotifyMobile.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/pnotify/js/PnotifyNonBlock.js"></script>
    <script>
      PNotify.defaults.styling = 'bootstrap4';

      function printContent(el){
        var restorepage = $('body').html();
        var printcontent = $('#' + el).clone();
        $('body').empty().html(printcontent);
        window.print();
        $('body').html(restorepage);
      }

    </script>
    
    <!-- Plugins and scripts required by this view-->
    <?php 
        if ( isset($scripts) ){ 
            foreach($scripts as $script){
    ?>
    <script src="<?= $script ?>"></script>
    <?php 
            }
        } 
        if ( isset($users_list) ){
    ?>

      <script type="text/javascript">

        var table;
        var base_url = '<?= base_url();?>';
        
        $(document).ready(function() {
            //datatables
            table = $('#table').DataTable({ 
        
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
        
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?= site_url('ajax/'.$users_list)?>",
                    "type": "POST"
                },
        
                //Set column definition initialisation properties.
                "columnDefs": [
                    { 
                        "targets": [ -1 ], //last column
                        "orderable": false, //set not orderable
                    },
                    { 
                        "targets": [ -2 ], //2 last column (photo)
                        "orderable": false, //set not orderable
                    },
                ],
                "language": {
                  url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/French.json'
                },
        
            });
        
        });

        function reload_table()
        {
            table.ajax.reload(null,false); //reload datatable ajax 
        }
          
      </script>

    <?php
      } if ( isset($formbuilder) ) {
    ?>

    <script>
            
        jQuery(function($) {

          var lang = 'fr-FR';
          var formData = <?= $json ?>;

          var options = {
            onSave: function(evt, formData) {
              $('#toJson').val(formData);

              var toJson = formData;
              var form_id = $('#form_id').val();
              var updated_by = $('#updated_by').val();
              var theme = $('#theme').val();
              if($('#status').is(":checked")){
                var status = 1;
              }
              else {
                var status = 0;
              }
              if($('#validation').is(":checked")){
                var validation = 1;
              }
              else {
                var validation = 0;
              }
              if($('#total_limit_check').is(":checked")){
                var total_limit = $('#total_limit').val();
                var limit_message = $('#limit_message').val();
              }
              else {
                var total_limit = 0;
                var limit_message = "";
              }
              var name = $('#name').val();
              var message = $('#message').val();
              var postForm = {
                  'toJson'     : toJson,
                  'form_id'     : form_id,
                  'name'     : name,
                  'updated_by'     : updated_by,
                  'status'     : status,
                  'validation'     : validation,
                  'total_limit'     : total_limit,
                  'limit_message'     : limit_message,
                  'message'     : message,
                  'theme_id'     : theme,
              };
              
              // console.log(postForm);
              $.ajax({
                  type      : 'POST',
                  url       : '<?= site_url().'ajax/form_update' ?>',
                  data      : postForm,
                  dataType  : 'json',
                  success   : function(data) {
                    console.log(data);
                    if ( data == false ) {
                      swal('warning', 'Impossible de mettre à jour les informations du formulaire!');
                    }
                    else {
                      swal('success', 'Vos données ont été enregistrées');
                    }
                  }
              });

            },
            onClearAll: function(formData) {
              $('#toJson').val(formData);
            },
            i18n: {
              locale: lang
            }
          };

          var fbEditor = document.getElementById('fb-editor');
          var formBuilder = $(fbEditor).formBuilder(options);

          // 

          formBuilder.promise.then(function(fb) {
            formBuilder.actions.setData(formData);
          });
          
          document.getElementById('json-tab').addEventListener('click', function() {
            document.getElementById('setJson').innerHTML = formBuilder.actions.getData('json', true);
          });

        });

        jQuery($ => {
            const escapeEl = document.createElement("textarea");
            const code = document.getElementById("formrender");
            const formData = <?= $json ?>;
            const addLineBreaks = html => html.replace(new RegExp("><", "g"), ">\n<");

            // Grab markup and escape it
            const $markup = $("<div/>");
            $markup.formRender({ formData });

            // set < code > innerText with escaped markup
            code.innerHTML = addLineBreaks($markup.formRender("html"));

            hljs.highlightBlock(code);
            // console.log(code);
            // $('#setCode').val(code);
            // document.getElementById('setCode').innerHTML = code;
        });
        
        function swal(icon, message){
          Swal.fire({
            position: 'top-end',
            icon: icon,
            title: message,
            showConfirmButton: false,
            timer: 1500
          });
        }

        function upload(id, type){
          $('#' + id).submit(function(e){
              e.preventDefault(); 
              $.ajax({
                url:'<?=site_url();?>ajax/upload_image',
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
                async:false,
                success: function(data){
                  swal('success', type + ' a bien été enregistrées');
                  $('#uploaded_'+type).html(data); 
                }
              });
            });
        }

        function totalLimitChanged()
        {
            if($('#total_limit_check').is(":checked"))   
                $(".total_limit").show();
            else
                $(".total_limit").hide();
        }

        function sendMailChanged()
        {
            if($('#send_email').is(":checked"))   
                $(".show-send-mail").show();
            else
                $(".show-send-mail").hide();
        }

        $('#submitRedirection').submit(function(e){
          e.preventDefault(); 
          $.ajax({
            url:'<?=site_url();?>ajax/updateFormConfirmation',
            type:"post",
            data:new FormData(this),
            processData:false,
            contentType:false,
            cache:false,
            async:false,
            success: function(data){
              if( data === '1' )
                swal('success', 'Paramètres de confirmation a bien été enregistrées');
              else 
                swal('danger', "Erreur d'enregistrement");
            }
          });
        });

        $(document).ready(function(){
          $( '#message-btn' ).click(function(){
            $( "#message-show" ).show();
            $( "#redirection-show" ).hide();
            $( "#submitRedirection input" ).removeAttr( "required" );
            $( '#submitRedirection textarea' ).prop('required', true);
          });
          $( '#redirection' ).click(function(){
            $( "#redirection-show" ).show();
            $( "#message-show" ).hide();
            $( "#submitRedirection textarea" ).removeAttr( "required" );
            $( '#submitRedirection input' ).prop('required', true);
          });

          $('#formconfirmation-mail_message').trumbowyg();
        });
    </script>

    <?php 
      }
      if( isset($forms_list) ) {
    ?>
          <script type="text/javascript">

            var table;
            var base_url = '<?= base_url();?>';

            $(document).ready(function() {
                //datatables
                table = $('#table').DataTable({ 

                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [], //Initial no order.

                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?= site_url('ajax/'.$forms_list)?>",
                        "type": "POST"
                    },

                    //Set column definition initialisation properties.
                    "columnDefs": [
                        { 
                            "targets": [ -1 ], //last column
                            "orderable": false, //set not orderable
                        },
                        { 
                            "targets": [ -2 ], //2 last column (photo)
                            "orderable": false, //set not orderable
                        },
                    ],
                    "language": {
                      url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/French.json'
                    },

                });

            });

            function reload_table()
            {
                table.ajax.reload(null,false); //reload datatable ajax 
            }
            function isConfirmed()
            {
                Swal.fire({
                  title: 'Are you sure?',
                  text: "You won't be able to revert this!",
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                  if (result.isConfirmed) {
                    Swal.fire(
                      'Deleted!',
                      'Your file has been deleted.',
                      'success'
                    )
                  }
                })
            }
          </script>
    <?php 
      } if( isset($submissions_list) ) {
    ?>
    <script type="text/javascript">
        
        function swal(icon, message){
            Swal.fire({
                position: 'top-end',
                icon: icon,
                title: message,
                showConfirmButton: false,
                timer: 1500
            }).then(function() {
               window.location = window.location.href;
            });
        }
        
        $( ".delete" ).click(function() {
            check = $(".delete").is(":checked");
            if(check) {
                $(".deletebtn").show();
            } else {
                $(".deletebtn").hide();
            }
        });

      $(document).ready(function() {
        //datatables
        
        
        var nameform = $.trim($( ".nameform" ).val());
        $('#table').DataTable({
            dom: 'Bfrtip',
            /*columnDefs: [ {
                orderable: false,
                className: 'select-checkbox',
                targets:   0
            } ],
            select: {
                style:    'multi',
                selector: 'td:first-child'
            },*/
            buttons: [
                { extend: 'excel', className: 'btn btn-success', text: '<i class="fa fa-download"></i> Exporter en Excel', title: nameform,
                  exportOptions: {
                    columns: 'th:not(:last-child)' 
                  }
                },
                { extend: 'colvis', text: '<i class="fa fa-table"></i> Afficher/Masquer', className: 'btn btn-primary' },
                {
                    className: 'btn btn-danger deletebtn',
                    text: '<i class="fa fa-trash"></i> Supprimer',
                    action: function ( e, dt, node, config ) {
                        var selected = new Array();
                        $("input:checkbox[name=deleteitems]:checked").each(function(){
                            selected.push($(this).val());
                        });
                        var input = $('.delete').val();
                        var postForm = {
                            'deleteinput'     : selected.toString(),
                        };
                        
                        Swal.fire({
                            title: 'Êtes-vous sûr?',
                            text: "Vous ne pourrez pas revenir en arrière!",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Oui, supprimez-le!'
                        }).then((result) => {
                          if (result.isConfirmed) {
                            $.ajax({
                                type      : 'POST',
                                url       : '<?= site_url().'ajax/deletCheckedSubmissions' ?>',
                                data      : postForm,
                                dataType  : 'json',
                                success   : function(data) {
                                    // console.log(data);
                                    if ( data == false ) {
                                        // console.log(data);
                                        swal('warning', 'Impossible d exécuté la requête');
                                    }
                                    else {
                                        // console.log(data);
                                        swal('success', 'La requête a été exécuté avec succès');
                                    }
                                }
                            });
                          }
                        })
                        
                        // alert( 'Button activated' );
                    }
                }
            ],
            language: {
                url: "//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/French.json"
            },
            order: [[ 1, 'desc' ]]
        });
        
      });
        
    </script>
    <?php 
      } if( isset($themes_list) ) {
    ?>
    
    <script type="text/javascript">

      var table;
      var base_url = '<?= base_url();?>';

      $(document).ready(function() {
          //datatables
          table = $('#table').DataTable({ 

              "processing": true, //Feature control the processing indicator.
              "serverSide": true, //Feature control DataTables' server-side processing mode.
              "order": [], //Initial no order.

              // Load data for the table's content from an Ajax source
              "ajax": {
                  "url": "<?= site_url('ajax/'.$themes_list)?>",
                  "type": "POST"
              },

              //Set column definition initialisation properties.
              "columnDefs": [
                  { 
                      "targets": [ -1 ], //last column
                      "orderable": false, //set not orderable
                  },
                  { 
                      "targets": [ -2 ], //2 last column (photo)
                      "orderable": false, //set not orderable
                  },
              ],
              "language": {
                url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/French.json'
              },

          });

      });

      function reload_table()
      {
          table.ajax.reload(null,false); //reload datatable ajax 
      }
      function isConfirmed()
      {
          Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.isConfirmed) {
              Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
              )
            }
          })
      }
      </script>
    <?php 
      } if( isset($editorTheme) ) {
    ?>
      <script>
        var editor = ace.edit("editor");
        var CssMode = ace.require("ace/mode/css").Mode;
        editor.setTheme("ace/theme/clouds");
        editor.session.setMode(new CssMode());
        editor.setAutoScrollEditorIntoView(true);
        editor.session.setUseSoftTabs(true);
        editor.resize();
        
        // enable autocompletion and snippets
        // editor.setOptions({
            // enableBasicAutocompletion: true,
            // enableSnippets: true,
            // enableLiveAutocompletion: true,
            // wrapEnabled:true,
        // });
        
        
        var codeCss = $('#codeCss').val();
        
        editor.setValue(codeCss);
        // alert(code);
        $(document).ready(function() {
          $('#editor').bind('DOMSubtreeModified', function(){
            var code = editor.getValue();
            $('#codeCss').val(code);
            // console.log('changed');
          });
        });

      </script>
    <?php 
      }
    ?>