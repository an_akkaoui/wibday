
  <head>
    <base href="<?= base_url() ?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="<?= $description; ?>">
    <meta name="author" content="wibday.com">
    <meta name="keyword" content="Wibday,Form,GroupeWib">
    <title><?= $title; ?></title>
    <!-- Icons-->
    <link href="<?= base_url() ?>app-assets/vendors/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>app-assets/vendors/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>app-assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>app-assets/vendors/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
    <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="<?= base_url() ?>app-assets/css/style.css" rel="stylesheet">
    <link href="<?= base_url() ?>app-assets/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
    
    
    <!-- Css and scripts required by this view-->
    <?php 
        if ( isset($css) ){ 
            foreach($css as $cs){
    ?>
    <link href="<?= $cs ?>" rel="stylesheet">
    <?php 
            }
        } 
    ?>

  </head>