<!DOCTYPE html>
<!--
* Wibday - Form builder
* @version v0.1.0
* @link https://wibday.com
* Copyright (c) 2021 yelagy
-->

<html lang="en">
  
  <?php echo $header; ?>
  
  <body class="app aside-menu-fixed sidebar-lg-show">
  
	<?php echo $menu; ?>
	
    <div class="app-body">
	
		<?php echo $sidebar; ?>
		
    <main class="main">
      <!-- Breadcrumb-->
      <ol class="breadcrumb">
          <li class="breadcrumb-item">Dashboard</li>
          <li class="breadcrumb-item active"><?= $breadcrumb ?></li>
      </ol>
      <div class="container-fluid">

        <?php
            //for warning -> flash_message
            //for info -> success_message
            
            $arr = $this->session->flashdata();
            if(!empty($arr['flash_message'])){
                $html = '<div class="alert alert-warning alert-dismissible" role="alert">';
                $html .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                $html .= $arr['flash_message'];
                $html .= '</div>';
                echo $html;
            }else if (!empty($arr['success_message'])){
                $html = '<div class="alert alert-success alert-dismissible" role="alert">';
                $html .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                $html .= $arr['success_message'];
                $html .= '</div>';
                echo $html;
            }else if (!empty($arr['danger_message'])){
                $html = '<div class="alert alert-danger alert-dismissible" role="alert">';
                $html .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                $html .= $arr['danger_message'];
                $html .= '</div>';
                echo $html;
            }
        ?>

        <?php echo $middle; ?>
        
      </div>
    </main>
		
		<?php echo $asidemenu; ?>
		
    </div>
    
	<?php echo $footer; ?>
	
  </body>
</html>