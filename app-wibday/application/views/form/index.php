<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?= $dataForm->name ?> | Wibday</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/png" href="<?= base_url() ?>app-assets/form-view/images/icons/favicon.png">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/form-view/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/form-view/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/form-view/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/form-view/vendor/animate/animate.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/form-view/vendor/css-hamburgers/hamburgers.min.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/form-view/vendor/animsition/css/animsition.min.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/form-view/vendor/select2/select2.min.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/form-view/vendor/daterangepicker/daterangepicker.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/form-view/css/util.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/form-view/css/main.css">
		<link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
		<meta name="robots" content="noindex, follow">
		<style>
            .login100-form {
                width: 100%;
                padding: 50px 30px 40px;
            }
            .wrap-login100{
                flex-direction: unset;
            }
            .footer-copyright {
                width: 100%;
                padding: 15px;
                margin: 0 auto;
            }
            div#formrender {
                width: 100%;
            }
            .allp {
                padding: 30px;
            }
            .image{
                width: 100%;
            }
            .mt-100 {
                margin-top: 100px
            }

            .modal {
                background-image: linear-gradient(rgb(35, 79, 71) 0%, rgb(36, 121, 106) 100.2%)
            }

            .modal-title {
                font-weight: 900
            }

            .modal-content {
                border-radius: 13px
            }

            .modal-body {
                color: #3b3b3b
            }

            .img-thumbnail {
                border-radius: 33px;
                width: 60px;
                height: 60px
            }

            .fab:before {
                position: relative;
                top: 13px
            }

            .smd {
                width: 100%;
                font-size: small;
                text-align: center
            }

            .modal-footer {
                display: block
            }

            .ur {
                border: none;
                background-color: #e6e2e2;
                border-bottom-left-radius: 4px;
                border-top-left-radius: 4px
            }

            .cpy {
                position: absolute;
                border: none;
                background-color: #e6e2e2;
                border-bottom-right-radius: 4px;
                border-top-right-radius: 4px;
                cursor: pointer;
                padding-top: 10px;
                right: 25px;
            }

            button.focus,
            button:focus {
                outline: 0;
                box-shadow: none !important
            }

            .ur.focus,
            .ur:focus {
                outline: 0;
                box-shadow: none !important
            }

            .message {
                font-size: 11px;
                color: #ee5535
            }

            .input-group {
                position: relative;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-align: stretch;
                -ms-flex-align: stretch;
                align-items: stretch;
                width: 100%;
            }
            .input-group-prepend {
                border-top-right-radius: 0;
                border-bottom-right-radius: 0;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
                padding: 0.375rem 0.75rem;
                margin-bottom: 0;
                font-size: 1rem;
                font-weight: 400;
                line-height: 1.5;
                color: #495057;
                text-align: center;
                white-space: nowrap;
                background-color: #e9ecef;
                border: 1px solid #ced4da;
                border-radius: 0.25rem;
            }
            .input-group-prepend {
                margin-right: -1px;
                display: flex;
            }
            .custom-select.is-invalid~.invalid-feedback, .custom-select.is-invalid~.invalid-tooltip, .form-control.is-invalid~.invalid-feedback, .form-control.is-invalid~.invalid-tooltip, .was-validated .custom-select:invalid~.invalid-feedback, .was-validated .custom-select:invalid~.invalid-tooltip, .was-validated .form-control:invalid~.invalid-feedback, .was-validated .form-control:invalid~.invalid-tooltip {
                display: block;
            }
            .invalid-feedback {
                display: none;
                width: 100%;
                margin-top: 0.25rem;
                font-size: 80%;
                color: #dc3545;
            }
            .valid-feedback {
                display: none;
                width: 100%;
                margin-top: 0.25rem;
                font-size: 80%;
                color: #28a745;
            }
            .form-check-input {
                position: absolute;
                margin-top: 0.3rem;
                margin-left: -0.15rem;
            }
            
            img.logo {
                margin-bottom: 25px;
            }

            .select2-selection__rendered {
                line-height: 31px !important;
            }
            .select2-container .select2-selection--single {
                height: 35px !important;
            }
            .select2-selection__arrow {
                height: 34px !important;
            }
			a.refreshCaptcha {
				color: #1384c8;
			}

            <?php 
                if( $dataTheme != '' ){
                    echo 'body .container-login100, body .footer-copyright, .login100-form-btn {
                        background: '.$dataTheme[0]['color'].' !important;
                    }' . "\n";
                    echo $dataTheme[0]['css'];
                }
                else 
                    echo '';
            ?>

		</style>
	</head>
	<body>
		<div class="limiter">
			<div class="container-login100">
				<div class="row wrap-login100">
                    <?php 

                        
                        $useragent = $_SERVER['HTTP_USER_AGENT'];
                        if( preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)) || $dataForm->image == '' )
                        {
                            $col = 12;
                        }
                        else {
                            $col = 6;
                        }
                        
                    ?>
					
                    <?php if( $dataForm->image != '' ){ ?>
                        <div class="col-<?= $col ?> allp">
                            <img class="image" src="<?= base_url() ?>assets/images/<?= $dataForm->image ?>" />
                        </div>
                    <?php } ?>
                    
					<div class="col-<?= $col ?>">
						<form class="login100-form validate-form" action="<?= base_url() ?>view/send" method="post">

                            <?php if( $dataForm->logo != '' ){ ?>
                                <div class="logo_container">
                                    <img class="image logo" src="<?= base_url() ?>assets/images/<?= $dataForm->logo ?>" />
                                </div>
                            <?php } ?>

                            <div class="form-group">
                                <input type="hidden" name="form_id" id="form_id" value="<?= $dataForm->form_id ?>">
                            </div>

                            <div id="formrender"></div>

                            
                            <div class="col-lg-12 col-md-12 align-center" style="padding: 0;">
                                <div class="form-group">
                                <label>Confirmation code <span class="required">*</span></label>
                                <p id="captImg"><?php echo $captchaImg; ?></p>
                                <p>Vous ne pouvez pas lire l'image? cliquez <a href="javascript:void(0);" class="refreshCaptcha"><b>ici</b></a> pour rafraîchir.</p>
                                <input type="text" name="captcha" class="form-control" value="" required/>
                                </div>
                            </div>

						    <div class="container-login100-form-btn">
							    <button class="login100-form-btn">
							    Envoyer
							    </button>
						    </div>
						  
						</form>
					</div>
				</div>
			</div>
			<div class="row footer-copyright">
				<div class="col-12">
					<div class="text-center">
						<span class="txt1">
						© Copyright
						</span>
						<a href="http://wibday.com/" class="txt2">
						Wib Day
						</a>
						<span class="txt1">
						| Tous Les Droits Résérvés.
						</span>
					</div>
				</div>
			</div>
		</div>
		
        

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    
                        <div class="modal-header">
                            <h5 class="modal-title">Partager</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                        </div>
                        <div class="modal-body row">

                            <div class="col-12">
                                
                                <div class="alert alert-success text-center" role="alert">
                                    <?= $rMessage ?>
                                </div>

                            </div>

                            <div class="col-4">
                                <div class="icon-container1 flex-column-reverse">
                                    <!--<div class="smd"> 
                                        <i class=" img-thumbnail fab fa-twitter fa-2x" style="color:#4c6ef5;background-color: aliceblue"></i>
                                        <p>Twitter</p>
                                    </div>-->
                                    <a href="https://www.facebook.com/sharer/sharer.php?u=#<?= urlencode($actual_link) ?>" target="_blank">
                                        <div class="smd"> 
                                            <i class="img-thumbnail fab fa-facebook fa-2x" style="color: #3b5998;background-color: #eceff5;"></i>
                                            <p>Facebook</p>
                                        </div>
                                    </a>
                                    <a href="https://wa.me/?text=<?= urlencode($actual_link) ?>" target="_blank">
                                        <div class="smd"> 
                                            <i class="img-thumbnail fab fa-whatsapp fa-2x" style="color: #25D366;background-color: #cef5dc;"></i>
                                            <p>Whatsapp</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-8">
                                <!-- novalidate -->
                                <form class="needs-validation share" action="<?= base_url() ?>view/share" method="post">
                                    <div class="form-row">
                                        <div class="col-md-12 mb-3">
                                            <div class="form-group">
                                                <input type="hidden" name="id_submission" id="id_submission" value="">
                                                <input type="hidden" name="id_form" id="id_form" value="<?= $dataForm->form_id ?>">
                                            </div>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroupPrepend">@</span>
                                                </div>
                                                <input type="email" name="emails[]" class="form-control" id="validationCustomUsername" placeholder="Email 1" aria-describedby="inputGroupPrepend" required>
                                                <!--<div class="invalid-feedback">
                                                Veuillez choisir une adresse e-mail.
                                                </div>-->
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroupPrepend">@</span>
                                                </div>
                                                <input type="email" name="emails[]" class="form-control" id="validationCustomUsername" placeholder="Email 2" aria-describedby="inputGroupPrepend" required>
                                                <!--<div class="invalid-feedback">
                                                Veuillez choisir une adresse e-mail.
                                                </div>-->
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroupPrepend">@</span>
                                                </div>
                                                <input type="email" name="emails[]" class="form-control" id="validationCustomUsername" placeholder="Email 3" aria-describedby="inputGroupPrepend" required>
                                                <!--<div class="invalid-feedback">
                                                Veuillez choisir une adresse e-mail.
                                                </div>-->
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <div class="input-group">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="invalidCheck" id="invalidCheck" required>
                                                    <label class="form-check-label" for="invalidCheck">
                                                        Agree to terms and conditions
                                                    </label>
                                                    <div class="invalid-feedback">
                                                        You must agree before submitting.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <button class="btn btn-primary" type="submit">Partager</button>
                                </form>

                            </div>
                        </div>
                        <div class="modal-footer"> <label style="font-weight: 600">Lien de page: <span class="message"></span></label><br />
                            <div class="row" style="width: 100%;"> 
                                <input class="col-12 ur" type="url" value="<?= $actual_link ?>"  placeholder="<?= $actual_link ?>" id="myInput" aria-describedby="inputGroup-sizing-default" style="height: 40px;" readonly> 
                                <button class="cpy"><i class="far fa-clone"></i></button> 
                            </div>
                        </div>
                        
                </div>
            </div>
        </div>

		<!--<div id="dropDownSelect1"></div>-->

		<script src="<?= base_url() ?>app-assets/form-view/vendor/jquery/jquery-3.2.1.min.js"></script>
		<script src="<?= base_url() ?>app-assets/form-view/vendor/animsition/js/animsition.min.js"></script>
		<script src="<?= base_url() ?>app-assets/form-view/vendor/bootstrap/js/popper.js"></script>
		<script src="<?= base_url() ?>app-assets/form-view/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?= base_url() ?>app-assets/form-view/vendor/select2/select2.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.4.0/highlight.min.js"></script>
		<script src="<?= base_url() ?>app-assets/js/form-render.min.js"></script>
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

		<script>

            (function() {
            'use strict';
            window.addEventListener('load', function() {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
                });
            }, false);
            })();

            function copyToClipboard(textToCopy) {
                // navigator clipboard api needs a secure context (https)
                if (navigator.clipboard && window.isSecureContext) {
                    // navigator clipboard api method'
                    return navigator.clipboard.writeText(textToCopy);
                } else {
                    // text area method
                    let textArea = document.createElement("textarea");
                    textArea.value = textToCopy;
                    // make the textarea out of viewport
                    textArea.style.position = "fixed";
                    textArea.style.left = "-999999px";
                    textArea.style.top = "-999999px";
                    document.body.appendChild(textArea);
                    textArea.focus();
                    textArea.select();
                    return new Promise((res, rej) => {
                        // here the magic happens
                        document.execCommand('copy') ? res() : rej();
                        textArea.remove();
                    });
                }
            }

            var cpyBtn = document.querySelector('.cpy');

            cpyBtn.addEventListener('click', function(event) {
            var copyInputValue = document.querySelector('#myInput');
            copyInputValue.focus();
            copyInputValue.select();

            try {
                var successful = document.execCommand('copy');
                var msg = successful ? 'successful' : 'unsuccessful';
                $(".message").text("Lien copié");
                // console.log('Copying text command was ' + msg);
            } catch (err) {
                // console.log('Oops, unable to copy');
            }
            });
            
            <?php 
                if( $dataForm->limit_message != '' )
                    $limit_message = $dataForm->limit_message;
                else 
                    $limit_message = "Le nombre d’inscription est limité";
            ?>

            $(function() {
                $('form.validate-form').submit(function(event) {
                    event.preventDefault();
                    var form = $(this);
                    $.ajax({
                    type: form.attr('method'),
                    url: form.attr('action'),
                    data: form.serialize()
                    }).done(function(data) {
                        if( data == 'maxlimit' ){
                            Swal.fire({
                                position: 'top-end',
                                icon: 'error',
                                title: '<?= $limit_message ?>',
                                showConfirmButton: false,
                                timer: 1500
                            }) 
                        }
                        else if( data == 'captcha' ) {
                            Swal.fire({
                                position: 'top-end',
                                icon: 'warning',
                                title: 'Code de confirmation est erroné',
                                showConfirmButton: false,
                                timer: 1500
                            }) 
                        }
                        else {
                            
                            $('#id_submission').val(data);
                            $('#exampleModal').modal('show');
							// var m = $('#exampleModal').hasClass('show');
							
								
                        }
                        // console.log(data);
                    }).fail(function(data) {
                        // console.log(data);
                    });
                });
            });
            
			<?php if( $rUrl != '' ){ ?>
			$('#exampleModal').on('hidden.bs.modal', function () {
				location.replace('<?= $rUrl ?>');
			})
			<?php } ?>
			
            $(function() {
                $('form.share').submit(function(event) {
                    event.preventDefault();
                    var form = $(this);
                    $.ajax({
                    type: form.attr('method'),
                    url: form.attr('action'),
                    data: form.serialize()
                    }).done(function(data) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Merci pour ce partage!',
                            showConfirmButton: false,
                            timer: 1500
                        })
                        // console.log(data);
                    }).fail(function(data) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'error',
                            title: 'Your work has been saved',
                            showConfirmButton: false,
                            timer: 1500
                        })
                        // console.log(data);
                    });
                });
            });

            jQuery($ => {
                const escapeEl = document.createElement("textarea");
                const code = document.getElementById("formrender");
                const formData = <?= $dataForm->json ?>;
                const addLineBreaks = html => html;
                
                const $markup = $("<div/>");
                $markup.formRender({ formData });
                
                code.innerHTML = addLineBreaks($markup.formRender("html"));
                
                hljs.highlightBlock(code);
            });

            $(document).ready(function(){

                $('.refreshCaptcha').on('click', function(){
                    $.get('<?= base_url().'view/refresh'; ?>', function(data){
                        $('#captImg').html(data);
                    });
                });

                $(".selection-2").select2();
				
				$('input[type="tel"]').attr('pattern', '[0-9]{3}[0-9]{3}[0-9]{4}');
            });
            
		</script>
		<script src="<?= base_url() ?>app-assets/form-view/vendor/daterangepicker/moment.min.js"></script>
		<script src="<?= base_url() ?>app-assets/form-view/vendor/daterangepicker/daterangepicker.js"></script>
		<script src="<?= base_url() ?>app-assets/form-view/vendor/countdowntime/countdowntime.js"></script>
		<script src="<?= base_url() ?>app-assets/form-view/js/main.js"></script>
	  
	</body>
</html>