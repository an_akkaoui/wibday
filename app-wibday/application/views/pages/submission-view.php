        <?php 

            $previous = "javascript:history.go(-1)";
            if( isset($submissioninfo['submission'][0]) ){
                if(isset($_SERVER['HTTP_REFERER'])) {
                    $previous = $_SERVER['HTTP_REFERER'];
                }
                else 
                   header('Location: ' . site_url() . 'forms' );
            }
            else 
                header('Location: ' . site_url() . 'forms' );

        ?>
        <style>
            .tdr{
                width: 30%;
                border-right: 1px solid #e0e5ec;
            }
        </style>

        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12" style="margin-bottom: 10px;">
                    <a class="btn btn-success" href="<?= $previous ?>"><i class="fa fa-arrow-left"></i> Retourner </a>
                </div>
                <div class="col-lg-12" id="printSubmission">
                    <div class="card">
                            <div class="card-header"><i class="fa fa-file-text"></i> <b> <?= $nameFrom ?> </b>
                            <div class="card-header-actions">
                            <i class="fa fa-drivers-license"></i> Détails Inscriptions: <b>#<?= $submission_id ?></b>
                            </div>
                            </div>
                            <div class="card-body">
                            
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- Default box -->
                                        <div class="">
                                            <div class="card no-padding no-border">
                                                <table class="table table-striped mb-0">
                                                <tbody>

                                                    <?php 
                                                        $contentTr = '';
                                                        $ID = '';
                                                        foreach( $submissioninfo['submission'][0] as $submissionk => $submissionv ){
                                                            
                                                            if($submissionk == 'ID')
                                                                $ID = $submissionv;
                                                            if( $submissionk != '' && $submissionk != 'ID' ){
                                                                $contentTr .= '<tr> 
                                                                    <td class="tdr"> <strong>'.$submissioninfo['header'][$submissionk]['label'].':</strong> </td>
                                                                    <td> <span>'.$submissionv.'</span> </td>
                                                                </tr>';
                                                            }
                                                        }
                                                    ?>
                                                    
                                                    <tr> 
                                                        <td class="tdr"> <strong>Inscriptions #:</strong> </td>
                                                        <td> <span><?= $ID ?></span> </td>
                                                    </tr>
                                                    <?= $contentTr ?>

                                                </tbody>
                                                </table>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                        <!-- /.box -->
                                    </div>
                                </div>
                                <div class="form-actions text-center">
                                    <p style="font-size: 20px;">© Wibday <?= date('Y') ?></p>
                                </div>
                            </div>
                            
                    </div>
                </div>
                <div class="col-lg-12" style="margin-bottom: 10px;">
                    <button class="btn btn-success pull-right" type="button" onclick="printContent('printSubmission');"><i class="la la-print"></i>&nbsp;Print</button>
                </div>
                <!-- /.col-->
            </div>
            <!-- /.row-->
        </div>