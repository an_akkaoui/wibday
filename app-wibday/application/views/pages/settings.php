
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-sm-8">
          <div class="card">

            <div class="card-header"> <strong>Paramètres</strong>
                <div class="card-header-actions">
                    <a class="card-header-action btn-setting" href="<?= site_url();?>main/users"><i class="fa fa-list"></i> Liste des utilisateurs </a>
                </div>
            </div>

            <div class="card-body">
              <div class="row">

                <div class="col-sm-12">

                  <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home4" role="tab" aria-controls="home"><i class="icon-settings"></i> Paramètres </a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#profile4" role="tab" aria-controls="profile"><i class="icon-envelope-letter"></i> Serveur de messagerie </a></li>
                    <!--<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#messages4" role="tab" aria-controls="messages"><i class="icon-pie-chart"></i> Charts</a></li>-->
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane active" id="home4" role="tabpanel">
                      
                        <?php 
                              $fattr = array('class' => 'form-signin');
                              echo form_open('/main/settings', $fattr);
                              function tz_list() {
                                  $zones_array = array();
                                  $timestamp = time();
                                  foreach(timezone_identifiers_list() as $key => $zone) {
                                  date_default_timezone_set($zone);
                                  $zones_array[$key]['zone'] = $zone;
                                  }
                                  return $zones_array;
                              }
                              function encryption() {
                                $ecr = array(
                                  'tls', 'ssl', 'none'
                                );
                                return $ecr;
                              }
                        ?>
                        <div class="form-group">
                          <label for="site_title">Titre du site</label>
                        <?php echo '<input type="hidden" name="id" value="'.$data->id.'">'; ?>
                        <?php echo form_input(array('name'=>'site_title', 'id'=> 'site_title', 'placeholder'=>'Titre du site', 'class'=>'form-control', 'value' => set_value('site_title', $data->site_title))); ?>
                          <?= form_error('site_title');?>
                        </div>
                        <div class="form-group">
                          <label for="timezone">Timezone</label>
                          <select name="timezone" id="timezone" class="form-control">
                              <option value="<?= $timezonevalue; ?>"><?= $data->timezone; ?></option>
                              <?php foreach(tz_list() as $t) { ?>
                                  <option value="<?= $t['zone']; ?>"> <?= $t['zone']; ?></option>
                              <?php } ?>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="copyright">Copyright</label>
                          <?php echo form_input(array('name'=>'copyright', 'id'=> 'copyright', 'placeholder'=>'Copyright', 'class'=>'form-control', 'value' => set_value('copyright', $data->copyright))); ?>
                            <?= form_error('site_title');?>
                          </div>
                        <div class="form-actions">
                          <?php echo form_submit(array('value'=>'Enregistrer', 'name'=>'submit', 'class'=>'btn btn-primary')); ?>
                        </div>
                        
                        

                    </div>
                    <div class="tab-pane" id="profile4" role="tabpanel">
                      
                    
                      <!-- /.row-->
                      <div class="row">
                        <div class="form-group col-sm-6">
                          <label for="host">Host</label>
                          <?php echo form_input(array('name'=>'host', 'id'=> 'host', 'placeholder'=>'Host', 'class'=>'form-control', 'value' => set_value('host', $data->host))); ?>
                        </div>
                        <div class="form-group col-sm-3">
                          <label for="port">Port</label>
                          <?php echo form_input(array('type' => 'number', 'min' => 1, 'name'=>'port', 'id'=> 'port', 'placeholder'=>'Port', 'class'=>'form-control', 'value' => set_value('host', $data->port))); ?>
                        </div>
                        <div class="col-sm-3">
                          <div class="form-group">
                            <label for="encryption">Encryption</label>
                            <select id="encryption" class="form-control" name="encryption">
                              <?php foreach(encryption() as $e) { ?>
                                  <option value="<?= $e ?>" <?php if( $e == $data->encryption ) { echo 'selected'; } ?>> <?= $e ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                      </div>

                      <!-- /.row-->
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <label for="username">Identifiant</label>
                            <?php echo form_input(array('name'=>'username', 'id'=> 'username', 'placeholder'=>'Identifiant', 'class'=>'form-control', 'value' => set_value('username', $data->username))); ?>
                          </div>
                        </div>
                      </div>

                      <!-- /.row-->
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <label for="password">Mot de passe</label>
                            <?php echo form_input(array('type'=>'password', 'name'=>'password', 'id'=> 'password', 'placeholder'=>'Mot de passe', 'class'=>'form-control', 'value' => set_value('password', $data->password))); ?>
                          </div>
                        </div>
                      </div>
                      <!-- /.row-->

                      <div class="row">
                        <div class="col-sm-12">
                        <div class="form-actions">
                            <?php echo form_submit(array('value'=>'Enregistrer', 'name'=>'submit', 'class'=>'btn btn-primary')); ?>
                        </div>
                        </div>
                      </div>
                      <!-- /.row-->
                      <?php echo form_close(); ?>
                    </div>
                    <!--<div class="tab-pane" id="messages4" role="tabpanel">3. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>-->
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- /.row-->
    </div>