
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-sm-12">
          <div class="card">
            <div class="card-header"><strong>Ajouter un nouvel utilisateur</strong> <small>User form</small>
                <div class="card-header-actions">
                    <a class="card-header-action btn-setting" href="<?= site_url();?>main/users"><i class="fa fa-list"></i> Liste des utilisateurs </a>
                </div>
          </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-12">
                  <div class="col-lg-4 col-lg-offset-4">
                      <h5>Veuillez saisir votre nouveau mot de passe.</h5> <br />
                      <?php 
                          $fattr = array('class' => 'form-signin');
                          echo form_open('/main/changepassword/'.$groups->id, $fattr);
                      ?>
                      <div class="form-group">
                      <?php echo form_password(array('name'=>'password', 'id'=> 'password', 'placeholder'=>'Mot de passe', 'class'=>'form-control', 'value' => set_value('password'))); ?>
                      <?php echo form_error('password') ?>
                      </div>
                      <div class="form-group">
                      <?php echo form_password(array('name'=>'passconf', 'id'=> 'passconf', 'placeholder'=>'Confirmez le mot de passe', 'class'=>'form-control', 'value'=> set_value('passconf'))); ?>
                      <?php echo form_error('passconf') ?>
                      </div>
                      <?php echo form_submit(array('value'=>'Modifier', 'class'=>'btn btn-lg btn-primary btn-block')); ?>
                      <?php echo form_close(); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- /.row-->
    </div>