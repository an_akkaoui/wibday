
      <style>
        .badge {
            display: inline-block;
            min-width: 10px;
            padding: 3px 7px;
            font-size: 12px;
            font-weight: 700;
            line-height: 1;
            color: #fff;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: 10px;
        }
      </style>
      <div class="animated fadeIn">
         <div class="row">
            <div class="col-lg-12">
               <div class="card">
                   

                    <div class="card-header"> <i class="fa fa-align-justify"></i> Thèmes</b>
                        <div class="card-header-actions">
                            <a class="card-header-action btn-setting" href="<?= site_url();?>themes/add">
                                <i class="fa fa-plus"></i> Ajouter
                            </a>
                        </div>
                    </div>

                    <div class="card-body">
                    
                    <table id="table" class="table table-responsive-sm table-bordered table-striped table-hover table-sm" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#ID</th>
                                <th>Nom</th>
                                <th>Couleur</th>
                                <th>Créé par</th>
                                <th>Statut</th>
                                <th>Actualisé</th>
                                <th style="width:150px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
            
                        <!--<tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Nom du formulaire</th>
                            <th>Créé par</th>
                            <th>Statut</th>
                            <th>Actualisé</th>
                            <th style="width:150px;">Action</th>
                        </tr>
                        </tfoot>-->
                    </table>
                    
                    </div>
               </div>
            </div>
            <!-- /.col-->
         </div>
         <!-- /.row-->
      </div>