
      <div class="animated fadeIn">
         <div class="row">
            <div class="col-lg-12">
               <div class="card">
                   

                    <div class="card-header"> <i class="fa fa-align-justify"></i> Formulaires</b>
                        <div class="card-header-actions">
                            <a class="card-header-action btn-setting" href="<?= site_url();?>forms/create">
                                <i class="fa fa-plus"></i> Ajouter
                            </a>
                        </div>
                    </div>

                    <!--<div class="card-header"><i class="fa fa-align-justify"></i> Formulaires 
                        <div class="card-header-actions">
                            <a class="btn btn-success" href="<?= site_url();?>forms/create"><i class="fa fa-plus"></i> Ajouter un formulaire</a>
                        </div>
                    </div>-->

                    <div class="card-body">
                    
                    <table id="table" class="table table-responsive-sm table-bordered table-striped table-hover table-sm" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nom du formulaire</th>
                                <th>Créé par</th>
                                <th>Statut</th>
                                <th>Actualisé</th>
                                <th style="width: 10%;"></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
            
                        <!--<tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Nom du formulaire</th>
                            <th>Créé par</th>
                            <th>Statut</th>
                            <th>Actualisé</th>
                            <th style="width:150px;">Action</th>
                        </tr>
                        </tfoot>-->
                    </table>
                    
                    </div>
               </div>
            </div>
            <!-- /.col-->
         </div>
         <!-- /.row-->
      </div>