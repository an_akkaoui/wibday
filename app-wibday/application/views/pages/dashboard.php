        <style>
          .card.text-white.bg-dark h1 {
              text-align: center;
              font-size: 60px;
          }

          .card.text-white.bg-dark h5 {
              text-align: center;
              font-size: 20px;
          }

          .card.text-white.bg-dark {
              padding: 26px;
          }
          .btn i {
              font-size: 40px;
          }
        </style>
        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-primary">
                  <div class="card-body pb-0">
                    <button class="btn btn-transparent p-0 float-right" type="button"><i class="fa fa-podcast"></i></button>
                    <div class="text-value"><?= $Inscriptions ?></div>
                    <div>Inscriptions</div>
                  </div>
                  <div class="chart-wrapper mt-3 mx-3" style="height:70px;">
                    <canvas class="chart" id="card-chart1" height="70"></canvas>
                  </div>
                </div>
              </div>
              <!-- /.col-->
              <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-success">
                  <div class="card-body pb-0">
                    <button class="btn btn-transparent p-0 float-right" type="button"><i class="nav-icon la la-lg la-th-large"></i></button>
                    <div class="text-value"><?= $Formulaires ?></div>
                    <div>Formulaires</div>
                  </div>
                  <div class="chart-wrapper mt-3 mx-3" style="height:70px;">
                    <canvas class="chart" id="card-chart2" height="70"></canvas>
                  </div>
                </div>
              </div>
              <!-- /.col-->
              <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-warning">
                  <div class="card-body pb-0">
                    <button class="btn btn-transparent p-0 float-right" type="button"><i class="fa fa-user"></i></button>
                    <div class="text-value"><?= $Utilisateurs ?></div>
                    <div>Utilisateurs</div>
                  </div>
                  <div class="chart-wrapper mt-3" style="height:70px;">
                    <canvas class="chart" id="card-chart3" height="70"></canvas>
                  </div>
                </div>
              </div>
              <!-- /.col-->
              <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-dark">
                  <h1>
                      <a href="<?= site_url();?>forms/create/"><i class="fa fa-plus"></i></a>
                  </h1>
                  <h5>
                      <a href="<?= site_url();?>forms/create/">Créer un formulaire</a>
                  </h5>
                </div>
              </div>
              <!-- /.col-->
            </div>
            <!-- /.row-->
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">Les cinq dernières formes</div>
                  <div class="card-body p-0">
                    <table class="table table-responsive-sm table-striped mb-0">
                      <thead class="thead-light">
                        <tr>
                          <th class="text-center">#ID</th>
                          <th>Nom du formulaire</th>
                          <th class="text-center">Créé par</th>
                          <th>Statut</th>
                          <th>Actualisé</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                          if( count($lastForms) > 0 ) {
                            // setlocale(LC_TIME, 'fr_FR', 'fr_FR.utf8', 'french');
                            setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
                            foreach( $lastForms as $form ){
                              $date = new DateTime($form['created_at']);
				                      $date_crt = strftime("%d %B %Y à %H:%I", $date->getTimestamp());
                        ?>
                              <tr>
                                <td class="text-center">
                                  <b><?= $form['id'] ?></b>
                                </td>
                                <td>
                                  <div><?= $form['name'] ?></div>
                                  <div class="small text-muted"><a href="<?= site_url() ?>forms/create/<?= $form['id'] ?>"><span>Modifier</span></a> | Créé à: <?= $date_crt ?></div>
                                </td>
                                <td class="text-center"><?= $form['user'] ?></i></td>
                                <td>
                                  <?= $form['status'] ?>
                                </td>
                                <td>
                                  <div class="small text-muted">Dernière mise à jour</div><strong><?= $form['lastUpdate'] ?></strong>
                                </td>
                              </tr>
                        <?php 
                            }
                          } else {
                        ?>
                          <tr><td colspan="5" class="text-center">Désolé, aucun résultat n'a été trouvé</td></tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>