
    <style>
        #editor {
            height: 400px;
            border: 1px #000 solid;
        }
    </style>
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-sm-12">
          <div class="card">
            <div class="card-header"><strong>Ajouter un thème</strong>
                <div class="card-header-actions">
                    <a class="card-header-action btn-setting" href="<?= site_url();?>themes/">
                        <i class="fa fa-list"></i> Listes des thèmes
                    </a>
                </div>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-12 d-flex justify-content-center">
                  <div class="col-lg-8">
                      
                      <?php 
                          $fattr = array('class' => 'form-signin');
                          echo form_open('/themes/add', $fattr);
                      ?>
                      <div class="form-group">
                        <label for="nom">Nom</label>
                        <?php echo form_input(array('name'=>'name', 'id'=> 'nom', 'placeholder'=>'Nom', 'class'=>'form-control', 'value' => set_value('name'))); ?>
                        <?php echo form_error('name');?>
                      </div>
                      <div class="form-group">
                        <label for="color">Couleur</label>
                        <?php echo form_input(array('type'=>'color','name'=>'color', 'id'=> 'color', 'placeholder'=>'Color', 'class'=>'form-control', 'color'=> set_value('color'))); ?>
                        <?php echo form_error('color');?>
                      </div>
                      <div class="form-group">
                        <label for="css">Css</label>
                            <textarea name="css" id="codeCss" wrap="off" autocorrect="off" autocapitalize="off" spellcheck="false" style="opacity: 0; height: 14px; width: 6.59781px; left: 51px; top: 0px;"><?= str_replace('<br />', '', nl2br(stripcslashes('/*html > body:not(.theme-embed){*/\n\n/*   background-image: url("https://..");  */\n/*   -webkit-background-size: cover; */\n/*  -moz-background-size: cover;*/\n/*  -o-background-size: cover;*/\n/*  background-size: cover; */\n/*  background-repeat: no-repeat; */\n/*  background-position: center;*/\n\n/*}*/'))); ?></textarea>
                        <div id="editor" class="form-control"> </div>
                      </div>
                      
                      <?php echo form_submit(array('value'=>'Ajouter', 'class'=>'btn btn-primary')); ?>
                      <?php echo form_close(); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- /.row-->
    </div>