
<!DOCTYPE html>
<!--
* Backstrap - Free Bootstrap Admin Template
* @version v0.2.0
* @link https://backstrap.net
* Copyright (c) 2018 Cristian Tabacitu
* Licensed under MIT (https://coreui.io/license)
-->

<html lang="en">
  <head>
    <base href="<?= base_url() ?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="Wibday - Formbuilder">
    <meta name="author" content="Wibday.com">
    <meta name="keyword" content="Wibday,Form,Builder, Formbuilder">
    <title>Wibday - Login</title>
    <!-- Icons-->
    <link href="<?= base_url() ?>app-assets/vendors/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>app-assets/vendors/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>app-assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>app-assets/vendors/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
    <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="<?= base_url() ?>app-assets/css/style.css" rel="stylesheet">
    <link href="<?= base_url() ?>app-assets/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
	<style>
		.w-logo {
			padding: 0 0 35px 0;
		}
	</style>
  </head>
  <body class="app flex-row align-items-center">

    <div class="container">
      <div class="row justify-content-center">

        <div class="col-md-8">
			<?php
				//for warning -> flash_message
				//for info -> success_message
				
				$arr = $this->session->flashdata();
				if(!empty($arr['flash_message'])){
					$html = '<div class="card p-4"><div class="container" style="margin-top: 10px;">';
					$html .= '<div class="alert alert-warning alert-dismissible" role="alert">';
					$html .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
					$html .= $arr['flash_message'];
					$html .= '</div>';
					$html .= '</div></div>';
					echo $html;
				}else if (!empty($arr['success_message'])){
					$html = '<div class="card p-4"><div class="container" style="margin-top: 10px;">';
					$html .= '<div class="alert alert-info alert-dismissible" role="alert">';
					$html .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
					$html .= $arr['success_message'];
					$html .= '</div>';
					$html .= '</div></div>';
					echo $html;
				}else if (!empty($arr['danger_message'])){
					$html = '<div class="card p-4"><div class="container" style="margin-top: 10px;">';
					$html .= '<div class="alert alert-danger alert-dismissible" role="alert">';
					$html .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
					$html .= $arr['danger_message'];
					$html .= '</div>';
					$html .= '</div></div>';
					echo $html;
				}
			?>
        </div>
        <div class="col-md-8">
          <div class="card-group">
            <div class="card p-4">
              <div class="card-body">
				<div class="text-center w-logo"> <img src="https://wibday.com/assets/img/logo.png" /> </div>
                <h2>S'identifier</h2>
                <p class="text-muted">Connectez-vous à votre compte</p>
                <?php   
                    $fattr = array('class' => 'form-signin');
                    echo form_open(site_url().'main/login/', $fattr); 
                ?>
                <div class="input-group mb-3">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="icon-user"></i></span></div>
                  
                    <?php echo form_input(array(
                        'name'=>'email', 
                        'id'=> 'email', 
                        'placeholder'=>'Email', 
                        'class'=>'form-control', 
                        'value'=> set_value('email')));

                    if(form_error('email')){
                    ?>
                    <span class="invalid-feedback" style="display:block">
                        <strong><?= form_error('email') ?></strong>
                    </span>
                    <?php } ?>
                </div>
                <div class="input-group mb-4">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="icon-lock"></i></span></div>
                  
                    <?php echo form_password(array(
                        'name'=>'password', 
                        'id'=> 'password', 
                        'placeholder'=>'Password', 
                        'class'=>'form-control', 
                        'value'=> set_value('password')));

                    if(form_error('password')){
                    ?>
                    <span class="invalid-feedback" style="display:block">
                        <strong><?= form_error('password') ?></strong>
                    </span>
                    <?php } ?>
                </div>
                <?php 
                    echo form_submit(array('value'=>'Se connecter', 'class'=>'btn btn-lg btn-primary btn-block')); 
                    echo form_close();
                ?>
                <!--div class="row">
                  <div class="col-6">
                    <button class="btn btn-primary px-4" type="button">Login</button>
                  </div>
                  <div class="col-6 text-right">
                    <button class="btn btn-link px-0" type="button">Forgot password?</button>
                  </div>
                </div>-->
              </div>
            </div>
            <!--<div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
              <div class="card-body text-center">
                <div>
                  <h2>Sign up</h2>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                  <a href="<?php echo site_url();?>main/login" class="btn btn-primary active mt-3">Register Now!</a>
                  <button class="btn btn-primary active mt-3" type="button">Register Now!</button>
                </div>
              </div>
            </div>-->
          </div>
        </div>
      </div>
    </div>
    <!-- CoreUI and necessary plugins-->
    <script src="<?= base_url() ?>app-assets/vendors/jquery/js/jquery.min.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/popper.js/js/popper.min.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/pace-progress/js/pace.min.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/perfect-scrollbar/js/perfect-scrollbar.min.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/@coreui/coreui/js/coreui.min.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/pnotify/js/Pnotify.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/pnotify/js/PnotifyButtons.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/pnotify/js/PnotifyConfirm.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/pnotify/js/PnotifyMobile.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/pnotify/js/PnotifyNonBlock.js"></script>
    <script>
      PNotify.defaults.styling = 'bootstrap4';
    </script>
  </body>
</html>