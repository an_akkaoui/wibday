
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-sm-12">
          <div class="card">
            <div class="card-header"><strong>Ajouter un nouvel utilisateur</strong> <small>User form</small>
                <div class="card-header-actions">
                    <a class="card-header-action btn-setting" href="<?= site_url();?>main/users"><i class="fa fa-list"></i> Liste des utilisateurs </a>
                </div>
          </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-12 d-flex justify-content-center">
                  <div class="col-lg-8">
                      <h5>Veuillez saisir les informations requises ci-dessous.</h5> <br />
                      <?php 
                          $fattr = array('class' => 'form-signin');
                          echo form_open('/main/adduser', $fattr);
                      ?>
                      <div class="form-group">
                      <?php echo form_input(array('name'=>'firstname', 'id'=> 'firstname', 'placeholder'=>'Prénom', 'class'=>'form-control', 'value' => set_value('firstname'))); ?>
                      <?php echo form_error('firstname');?>
                      </div>
                      <div class="form-group">
                      <?php echo form_input(array('name'=>'lastname', 'id'=> 'lastname', 'placeholder'=>'Nom', 'class'=>'form-control', 'value'=> set_value('lastname'))); ?>
                      <?php echo form_error('lastname');?>
                      </div>
                      <div class="form-group">
                      <?php
                          echo form_input(array('name'=>'email', 'id'=> 'email', 'placeholder'=>'Email', 'class'=>'form-control', 'value'=> set_value('email'))); 
                      ?>
                      <?php echo form_error('email');?>
                      </div>
                      <div class="form-group">
                      <?php
                          $dd_list = array(
                              ''   => '-',
                              '1'   => 'Admin',
                              '2'   => 'Editor',
                          );
                          $dd_name = "role";
                          echo form_dropdown($dd_name, $dd_list, set_value($dd_name),'class = "form-control" id="role"');
                      ?>
                      <?php echo form_error('role') ?>
                      </div>
                      <div class="form-group">
                      <?php echo form_password(array('name'=>'password', 'id'=> 'password', 'placeholder'=>'Mot de passe', 'class'=>'form-control', 'value' => set_value('password'))); ?>
                      <?php echo form_error('password') ?>
                      </div>
                      <div class="form-group">
                      <?php echo form_password(array('name'=>'passconf', 'id'=> 'passconf', 'placeholder'=>'Confirmez le mot de passe', 'class'=>'form-control', 'value'=> set_value('passconf'))); ?>
                      <?php echo form_error('passconf') ?>
                      </div>
                      <?php echo form_submit(array('value'=>'Ajouter', 'class'=>'btn btn-primary')); ?>
                      <?php echo form_close(); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- /.row-->
    </div>