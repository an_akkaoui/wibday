
      <div class="animated fadeIn">
         <div class="row">
            <div class="col-lg-12">
               <div class="card">

                    <div class="card-header"> <b>Utilisateurs</b>
                        <?php if($dataLevel == 'is_admin'){ ?>
                            <div class="card-header-actions">
                                <a class="card-header-action btn-setting" href="<?= site_url();?>main/adduser"><i class="fa fa-plus"></i> Ajouter </a>
                            </div>
                        <?php } ?>
                    </div>

                    <div class="card-body">
                    
                    <table id="table" class="table table-responsive-sm table-bordered table-striped table-hover table-sm" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Prénom</th>
                                <th>Email</th>
                                <th>Dernière connexion</th>
                                <th>Rôle</th>
                                <th>Statut</th>
                                <th style="width:150px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
            
                        <tfoot>
                        <tr>
                            <th>Nom</th>
                            <th>Prénom</th>
                            <th>Email</th>
                            <th>Dernière connexion</th>
                            <th>Rôle</th>
                            <th>Statut</th>
                            <th style="width:150px;">Action</th>
                        </tr>
                        </tfoot>
                    </table>
                    
                    </div>
               </div>
            </div>
            <!-- /.col-->
         </div>
         <!-- /.row-->
      </div>