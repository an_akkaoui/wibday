<style>

.save-template {
    width: 100%;
    border-top-left-radius: 4px !important;
    border-bottom-left-radius: 4px !important;
    background: #42ba96;
}

.form-actions.btn-group {
    float: left !important;
    width: 100% !important;
}

.form-wrap.form-builder .frmb{
    float: right !important;
}
.form-wrap.form-builder .cb-wrap {
    float: left !important;
}
.bd-callout-warning {
    border-left-color: #f0ad4e !important;
}
.bd-callout {
    padding: 1.25rem;
    margin-bottom: 1.25rem;
    border: 1px solid #eee;
    border-left-width: .25rem;
    border-radius: .25rem;
}
.bd-callout h5{
    font-size: 1.25rem !important;
    font-weight: 700 !important;
}
.bd-callout p:last-child {
    margin-bottom: 0;
}
.bd-callout code {
    border-radius: .25rem;
}
li.formbuilder-icon-file.input-control.input-control-10.ui-sortable-handle, li.formbuilder-icon-button.input-control.input-control-1.ui-sortable-handle, .clear-all, .get-data {
    display: none !important;
}

.bg-tab {
    background: #f7f7f7;
}

div#uploaded_Photo, #uploaded_Background {
    font-size: 15px !important;
    padding-left: 0;
    margin: 15px 0 5px 0;
}

#uploaded_Photo input[type="checkbox"], #uploaded_Background input[type="checkbox"] {
    margin: 15px 5px 0 5px;
    transform: scale(1.5);
}
img.img-thumbnail {
    margin-bottom: 10px;
}
.form-group.name-wrap {
    display: none !important;
}
small.small {
    font-size: 11px;
}
a.card-header-action.btn-setting.success {
    color: #42ba96;
}
</style>
<div class="animated fadeIn">
<div class="row">
<div class="col-sm-12">
  <div class="card">

    <div class="card-header"> <i class="fa fa-table"></i> <b>Formulaire</b>
        <?php if($dataLevel == 'is_admin'){ ?>
            <div class="card-header-actions">
                <a class="card-header-action btn-setting" href="<?= site_url();?>forms/submissions/<?= $dataForm->form_id ?>"><i class="fa fa-list"></i> Listes des inscription </a>
				<?= $lvf ?>
            </div>
        <?php } ?>
    </div>
    
    <div class="card-body">
      <div class="row">
        <div class="col-sm-8">
            <div class="bd-callout bd-callout-warning">
                <h5>Conseil:</h5>
                <p>Il suffit de glisser les champs sur le centre pour commencer à construire votre formulaire. C'est rapide et facile.</p>
                <code>N'oubliez pas de sauvegarder le formulaire avant de quitter la page!</code>
            </div>

            <div id="fb-editor"></div>
        </div>
        <div class="col-sm-4">
            
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="options-tab" data-toggle="tab" href="#options" role="tab" aria-controls="options" aria-selected="true">Options</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="file-tab" data-toggle="tab" href="#file" role="tab" aria-controls="file" aria-selected="false">Désign</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="parametres-tab" data-toggle="tab" href="#parametres" role="tab" aria-controls="parametres" aria-selected="false">Paramètres</a>
                </li>
                <!--<li class="nav-item">
                    <a class="nav-link" id="json-tab" data-toggle="tab" href="#json" role="tab" aria-controls="json" aria-selected="false">Json</a>
                </li>-->
                <li class="nav-item">
                    <a class="nav-link" id="code-tab" data-toggle="tab" href="#code" role="tab" aria-controls="code" aria-selected="false">Vue html</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="options" role="tabpanel" aria-labelledby="options-tab">
                    <div class="form-group hidden">
                        <input type="hidden" id="toJson" value="<?= htmlspecialchars( $dataForm->json, ENT_COMPAT ) ?>" />
                        <input type="hidden" name="updated_by" id="updated_by" value="<?= $user_id ?>">
                        <input type="hidden" name="form_id" id="form_id" value="<?= $dataForm->form_id ?>">
                    </div>
                    <div class="form-group">
                        <?= form_input(array('name'=>'name', 'id'=> 'name', 'placeholder'=>'Nom du formulaire', 'class'=>'form-control', 'value' => set_value('name', $dataForm->name))); ?>
                        <?= form_error('name');?>
                    </div>
                    <div class="form-group">
                        <label for="status">Statut</label><br>
                        <?php 
                            if( $dataForm->status == 1 )
                                $status = 'checked';
                            else 
                                $status = '';
                        ?>
                        <input type="checkbox" name="status" id="status" <?= $status ?> data-toggle="toggle" data-onstyle="success" data-on="Activé" data-off="Désactivé" data-offstyle="danger"> <br> <small class="small">Désactiver le formulaire à tout moment.</small>
                    </div>
                    <div class="form-group">
                        <label for="validation">Activié la validation de l'adresse e-mail</label><br>
                        <?php 
                            if( $dataForm->validation == 1 )
                                $validation = 'checked';
                            else 
                                $validation = '';
                        ?>
                        <input type="checkbox" name="validation" id="validation" <?= $validation ?> data-toggle="toggle" data-onstyle="success" data-on="Activé" data-off="Désactivé" data-offstyle="danger">
                    </div>
                    <div class="form-group">
                        <label for="validation">Limiter le nombre total d'inscriptions</label><br>
                        <?php 
                            if( $dataForm->total_limit != 0 ){
                                $hidetotal = '';
                                $total_limit_check = 'checked';
                            }
                            else {
                                $hidetotal = 'style="display: none;"';
                                $total_limit_check = '';
                            }
                        ?>
                        <input type="checkbox" name="total_limit_check" id="total_limit_check" <?= $total_limit_check ?> data-toggle="toggle" data-onstyle="success" data-on="Activé" data-off="Désactivé" data-offstyle="danger" onchange="totalLimitChanged()">
                    </div>
                    <div class="total_limit" <?= $hidetotal ?>>
                        <div class="form-group">
                            <?php 
                                if($dataForm->total_limit == 0)
                                    $valueTotal = '';
                                else 
                                    $valueTotal = $dataForm->total_limit;
                            ?>
                            
                            <label for="total_limit"> Nomber</label>
                            <?= form_input(array('type' => 'number', 'min' => '10', 'name'=>'total_limit', 'id'=> 'total_limit', 'placeholder'=>'Entrez le nombre total ...', 'class'=>'form-control', 'value' => set_value('total_limit', $valueTotal))); ?>
                        </div>
                        <label for="limit_message"> Limit Message</label>
                        <div class="form-group">
                            <?= form_textarea(array('name'=>'limit_message', 'id'=> 'limit_message', 'placeholder'=>'Limit Message', 'class'=>'form-control', 'value' => set_value('limit_message', $dataForm->limit_message))); ?>
                            <?= form_error('message');?>
                        </div>
                    </div>
                    <div class="form-group">
                        <?= form_textarea(array('name'=>'message', 'id'=> 'message', 'placeholder'=>'Message', 'class'=>'form-control', 'value' => set_value('message', $dataForm->message))); ?>
                        <?= form_error('message');?>
                    </div>
                </div>
                <div class="tab-pane fade bg-tab" id="file" role="tabpanel" aria-labelledby="file-tab">
                    <div class="row">
                        <div class="form-group col-12">
                            <label for="theme">Sélectionnez une thème</label>
                            <select class="form-control theme" id="theme">
                                <option value=""> - </option>
                                <?php foreach($themes as $theme) { ?>
                                    <option value="<?= $theme->id ?>" <?php if( $theme->id == $dataForm->theme_id ) { echo 'selected'; } ?>> <?= $theme->name ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group col-12">
                            <div class="card">
                                <div class="card-header">Logo
                                <div class="card-header-actions"><small class="text-muted">View</small></div>
                                </div>
                                <div class="card-body">
                                    <form id="submitLogo" method="post">
                                        <div class="form-group">
                                            <input type="hidden" name="form_id" id="form_id" value="<?= $dataForm->form_id ?>">
                                            <input type="hidden" name="column" value="logo">
                                            
                                            <input type="file" name="file" style="margin-bottom: 15px;">
                                            <div class="col-sm-12 col-md-offset-12" id="uploaded_Logo"> 
                                                <?php if( !empty($dataForm->logo) ) { ?>
                                                    <img src="<?= base_url() ?>assets/images/<?= $dataForm->logo ?>" width="250" height="200" class="img-thumbnail" />
                                                    <input type="hidden" class="remove_photo" name="remove_photo" value="<?= $dataForm->logo ?>"> L'ancienne logo sera supprimée définitivement lors du nouveau téléchargement
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input class="btn btn-success" type="submit" value="Télécharger" onclick="upload('submitLogo','Logo');">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-columns cols-2">

                        <div class="card">
                            <div class="card-header">Photo
                            <div class="card-header-actions"><small class="text-muted">View</small></div>
                            </div>
                            <div class="card-body">
                                <form id="submitPhotos" method="post">
                                    <div class="form-group">
                                        <input type="hidden" name="form_id" id="form_id" value="<?= $dataForm->form_id ?>">
                                        <input type="hidden" name="column" value="image">
                                        
                                        <input type="file" name="file">
                                        <div class="col-sm-12 col-md-offset-12" id="uploaded_Photo"> 
                                            <?php if( !empty($dataForm->image) ) { ?>
                                                <img src="<?= base_url() ?>assets/images/<?= $dataForm->image ?>" width="250" height="200" class="img-thumbnail" />
                                                <input type="hidden" class="remove_photo" name="remove_photo" value="<?= $dataForm->image ?>"> L'ancienne image sera supprimée définitivement lors du nouveau téléchargement
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input class="btn btn-success" type="submit" value="Télécharger" onclick="upload('submitPhotos','Photo');">
                                    </div>
                                </form>
                            </div>
                        </div>
                        
                        <div class="card">
                            <div class="card-header">Background
                                <div class="card-header-actions"><small class="text-muted">View</small></div>
                            </div>
                            <div class="card-body">
                                <form id="submitBackground" method="post">
                                    <div class="form-group">
                                        <input type="hidden" name="form_id" id="form_id" value="<?= $dataForm->form_id ?>">
                                        <input type="hidden" name="column" value="background">
                                        <input type="hidden" id="setCode" name="setCode" value="">
                                        
                                        <input type="file" name="file">
                                        <div class="col-sm-12 col-md-offset-12" id="uploaded_Background"> 
                                            <?php if( !empty($dataForm->background) ) { ?>
                                                <img src="<?= base_url() ?>assets/images/<?= $dataForm->background ?>" width="250" height="200" class="img-thumbnail" />
                                                <input type="hidden" class="remove_photo" name="remove_photo" value="<?= $dataForm->background ?>"> <br> L'ancienne background sera supprimée définitivement lors du nouveau téléchargement
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input class="btn btn-success" type="submit" value="Télécharger" onclick="upload('submitBackground','Background');">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade bg-tab" id="json" role="tabpanel" aria-labelledby="json-tab">
                    <pre id="setJson"></pre>
                </div>
                <div class="tab-pane fade" id="code" role="tabpanel" aria-labelledby="code-tab">
                    <div id="formrender"></div>
                </div>
                <div class="tab-pane fade" id="parametres" role="tabpanel" aria-labelledby="parametres-tab">
                    <label class="control-label">Confirme que l'inscription a été un succès avec:</label>
                    <p>
                        <button class="btn btn-primary" type="button" id="message-btn">
                        Seulement message
                        </button>
                        <button class="btn btn-primary" type="button" id="redirection">
                        Redirection vers une autre page
                        </button>
                    </p>
                    <form id="submitRedirection" method="post">
                        <?php 
                            if( $dataFormConfirmation != '' ){
                                $message = $dataFormConfirmation->message;
                                $url = $dataFormConfirmation->url;
                                $mail_from = $dataFormConfirmation->mail_from;
                                $mail_from_name = $dataFormConfirmation->mail_from_name;
                                $mail_subject = $dataFormConfirmation->mail_subject;
                                $mail_message = $dataFormConfirmation->mail_message;
                            }
                            else{
                                $message = '';
                                $url = '';
                                $mail_from = '';
                                $mail_from_name = '';
                                $mail_subject = '';
                                $mail_message = '';
                            }
                        ?>
                        <input type="hidden" name="form_id" id="form_id" value="<?= $dataForm->form_id ?>">
                        <div id="message-show">
                            <div class="card card-body">
                                <div class="form-group">
                                    <label for="formconfirmation-message">Votre message</label>
                                    <textarea id="formconfirmation-message" class="form-control" name="message" placeholder="Votre message de confirmation ......" required><?= $message ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="collapse" id="redirection-show">
                            <div class="card card-body">
                                <div class="form-group">
                                    <label for="formconfirmation-url">URL de la page</label>
                                    <input type="text" id="formconfirmation-url" class="form-control" name="url" value="<?= $url ?>" placeholder="Entrer URL...">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="validation">Envoyer un e-mail de confirmation?</label><br>
                            <?php 
                                if( $dataFormConfirmation->send_email != 0 ){
                                    $hidesend_email = '';
                                    $send_email_check = 'checked';
                                }
                                else {
                                    $hidesend_email = 'style="display: none;"';
                                    $send_email_check = '';
                                }
                            ?>
                            <input type="checkbox" name="send_email" id="send_email" <?= $send_email_check ?> data-toggle="toggle" data-onstyle="success" data-on="On" data-off="Off" data-offstyle="danger" onchange="sendMailChanged()">
                        </div>
                        <div class="card card-body show-send-mail" <?= $hidesend_email ?>>
                            <div class="form-group field-formconfirmation-mail_from">
                                <label class="control-label" for="formconfirmation-mail_from">Répondre à</label>
                                <input type="email" id="formconfirmation-mail_from" class="form-control" name="mail_from" value="<?= $mail_from ?>" placeholder="Entrez votre adresse email...">
                            </div>
                            <div class="form-group field-formconfirmation-mail_from_name">
                                <label class="control-label" for="formconfirmation-mail_from_name">Nom ou Entreprise</label>
                                <input type="text" id="formconfirmation-mail_from_name" class="form-control" name="mail_from_name" value="<?= $mail_from_name ?>" placeholder="Entrez votre nom ou celle de votre entreprise...">
                            </div>
                            <div class="form-group field-formconfirmation-mail_subject">
                                <label class="control-label" for="formconfirmation-mail_subject">Sujet</label>
                                <input type="text" id="formconfirmation-mail_subject" class="form-control" name="mail_subject" value="<?= $mail_subject ?>" placeholder="Entrez un sujet ...">
                            </div>
                            <div class="form-group field-formconfirmation-mail_message">
                                <label class="control-label" for="formconfirmation-mail_message">E-Mail Message</label>
                                <textarea id="formconfirmation-mail_message" class="form-control trumbowyg-textarea" name="mail_message" placeholder="Votre message de confirmation par email ..." tabindex="-1" style="height: 0px;"><?= $mail_message ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <input class="btn btn-success" type="submit" value="Modifier">
                        </div>

                    </form>
                </div>
            </div>

        </div>
      </div>
    </div>
  </div>
</div>
<!-- /.col-->
</div>
<!-- /.row-->
</div>