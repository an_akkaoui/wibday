        <?php // echo '<pre>'; print_r($dataForms); ?>
        <style>
            /*.btn-success {
                color: #FFFFFF !important;
                background-color: #42ba96 !important;
                border-color: #42ba96 !important;
            }
            button.dt-button.buttons-collection.buttons-colvis.btn.btn-primary {
                color: #FFFFFF;
                background-color: #7c69ef;
                border-color: #7c69ef;
            }*/
            input.delete {
                width: 15px;
                height: 15px;
                margin: 0 auto;
                display: block;
            }
            .deletebtn {
                display: none;
            }
            #table td {
                vertical-align: middle;
            }
        </style>
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                <div class="card">

                        <div class="card-header"> <i class="fa fa-align-justify"></i> Formulaires: <b><?= $dataForm->name ?></b>
                            <div class="card-header-actions">
                                <a class="card-header-action btn-setting" href="<?= site_url();?>forms/create/<?= $dataForm->form_id ?>"><i class="fa fa-pencil-square-o"></i> Modier </a>
                            </div>
                        </div>
                        <div class="card-body">
                        <div class="hidden"><input type="hidden" class="nameform" value="<?= $dataForm->name ?>" /></div>
                        
                        <table id="table" class="table table-responsive-sm table-bordered table-striped table-hover table-sm" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <?php foreach( $dataForms['header'] as $k => $v ){ if( $k != 'submission' && $k != '' ){ ?>
                                        <th><?php if($v['label']) echo $v['label']; else $v['name'] ?></th>
                                    <?php }} ?>
                                    <th>Créé à</th>
                                    <th style="width: 20px;"></th>
                                    <th style="width:150px;" class="notexport">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                <?php 
                                    foreach( $dataForms['submission'] as $submissions ){ 
                                ?>
                                <tr>
                                    <?php 
                                        $ID = '';
                                        foreach( $submissions as $submissionk => $submissionv ){ 
                                            if( $submissionk != '' ){
                                                if($submissionk == 'ID')
                                                    $ID = $submissionv;
                                    ?>
                                        <td><?= $submissionv ?></td>
                                    <?php 
                                            }
                                        }
                                    ?>
                                    <!--<td>2011/04/25</td>-->
                                    <td><input type='checkbox' class="delete" value="<?= $ID ?>" name="deleteitems" ></td>
                                    <td>
                                        <a class="btn btn-sm btn-secondary" href="<?= site_url() ?>forms/submissionview/<?= $ID ?>/<?= $form_id ?>" title="Listes des inscriptions"><i class="fa fa-eye fa-lg"></i></a>
                                        <a class="btn btn-sm btn-danger" href="<?= site_url() ?>forms/deleteSubmission/<?= $ID ?>/<?= $form_id ?>" title="Supprimer" onclick="return confirm('Etes-vous sûr que vous voulez supprimer?');"><i class="fa fa-trash-o fa-lg"></i></a>
                                    </td>
                                </tr>
                                <?php 
                                    }
                                ?>
                            </tbody>
                            
                        </table>
                        
                        </div>
                </div>
                </div>
                <!-- /.col-->
            </div>
            <!-- /.row-->
        </div>