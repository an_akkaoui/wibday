
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-sm-12">
          <div class="card">
            <div class="card-header"><strong>Modifier l'utilisateur</strong> <small><?= $groups->first_name ?></small>
            <?php if($dataLevel == 'is_admin'){ ?>
            <div class="card-header-actions">
                <a class="btn btn-success" href="<?= site_url();?>main/users"><i class="fa fa-list"></i> Liste des utilisateurs</a>
            </div>
            <?php } ?>
          </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-12">
                  <div class="col-lg-4 col-lg-offset-4">
                      <h5>Veuillez modofier les informations requises ci-dessous.</h5> <br />
                      <?php 
                          $fattr = array('class' => 'form-signin');
                          echo form_open('/main/updateuser/'.$groups->id, $fattr);
                      ?>
                      <div class="form-group">
                      <?php echo form_input(array('name'=>'firstname', 'id'=> 'firstname', 'placeholder'=>'Prénom', 'class'=>'form-control', 'value' => set_value('firstname', $groups->first_name))); ?>
                      <?php echo form_error('firstname');?>
                      </div>
                      <div class="form-group">
                      <?php echo form_input(array('name'=>'lastname', 'id'=> 'lastname', 'placeholder'=>'Nom', 'class'=>'form-control', 'value'=> set_value('lastname', $groups->last_name))); ?>
                      <?php echo form_error('lastname');?>
                      </div>
                      <div class="form-group">
                      <?php
                          echo form_input(array('name'=>'email', 'id'=> 'email', 'placeholder'=>'Email', 'class'=>'form-control', 'value'=> set_value('email', $groups->email))); 
                      ?>
                      <?php echo form_error('email');?>
                      </div>
                      <div class="form-group">
                      <?php

                          $dd_list = array(
                              '1'   => 'Admin',
                              '2'   => 'Editor',
                              '3'   => 'Author',
                              '4'   => 'Subscriber',
                          );

                          if($dataLevel != 'is_admin'){
                            unset($dd_list['1']);
                          }
                          $dd_name = "role";
                          echo form_dropdown($dd_name, $dd_list, set_value($dd_name, $groups->role),'class = "form-control" id="role" disabled');
                      ?>
                      </div>
                      <?php echo form_submit(array('value'=>'Modifer', 'class'=>'btn btn-lg btn-primary btn-block')); ?>
                      <?php echo form_close(); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- /.row-->
    </div>