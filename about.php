﻿<!DOCTYPE html>

<html lang="fr">



    <head>

        

        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>WIB DAY - Agence d'email Marketing Direct</title>



        <!-- CSS -->

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,400">

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:700,400">

        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">

        <link rel="stylesheet" href="assets/elegant-font/code/style.css">

        <link rel="stylesheet" href="assets/css/animate.css">

        <link rel="stylesheet" href="assets/css/magnific-popup.css">

        <link rel="stylesheet" href="assets/flexslider/flexslider.css">

        <link rel="stylesheet" href="assets/css/form-elements.css">

        <link rel="stylesheet" href="assets/css/style.css">

        <link rel="stylesheet" href="assets/css/media-queries.css">



        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

        <!--[if lt IE 9]>

            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

        <![endif]-->



        <!-- Favicon and touch icons -->

        <link rel="shortcut icon" href="assets/ico/favicon.png">

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">

        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">

        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">

        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">



    </head>



    <body>


<?php $n=2; include("includes/header.php") ?>




        <!-- Page Title -->

        <div class="page-title-container">

            <div class="container">

                <div class="row">

                    <div class="col-sm-10 col-sm-offset-1 page-title wow fadeIn">

                        <span aria-hidden="true"></span>

                        <h1>Qui sommes-nous /</h1>

                        <p>La solution marketing Unique qui contient tous les outils dont vous avez besoin.</p>

                    </div>

                </div>

            </div>

        </div>



        <!-- About Us Text -->

        <div class="about-us-container">

        	<div class="container">

	            <div class="row">

	                <div class="col-sm-12 about-us-text wow fadeInLeft">

	                    <h3>

Email Marketing WIB ADS</h3>

	                    <p>

	                    	Wib Day est une agence d’email Marketing direct et du conseil, elle se charge de l’envoi d’email à partir d’une plateforme performante qui gère toutes les campagnes emailing. Notre agence dispose d’1 millions d’emails ultra-qualifier et performant. Grâce à nos bases de données on vous permet de cibler et promouvoir votre business sur un panel de clients avec un fort taux de conversion. En tant que professionnel de l’emailing, nous possédons une connaissance approfondie et exhaustive des fichiers pouvant répondre à vos besoins. Notre objectif est l’accessibilité pour tous à :<br/>

                            <strong>- Campagne d’Email Marketing sur mesure</strong><br/>

<strong>- Client ciblé </strong><br/>

<strong>- Taux de délivrabilité optimal</strong><br/>

Notre objectif est de vous aider à booster vos campagnes afin d’optimiser votre rentabilité, mais aussi d’offrir une meilleure visibilité aux personnes intéressées par vos services ou vos produits. Dans un contexte de plus en plus concurrentiel, l’entreprise doit mener des actions concrètes, fiables et efficaces pour développer, fidéliser et renouveler sa clientèle. 



	                    </p>

	                    <h3>Notre Equipe</h3>

	                    <p>

	                    	Que ce soit pour vous accompagner dans le design de vos emails, prendre en charge vos campagnes, analyser vos résultats et vous faire des recommandations, ou plus globalement, auditer votre stratégie Email, nos équipes d’experts sont à votre écoute pour étudier vos besoins et répondre à toutes vos questions pour que vos projets se transforment en résultats et vos objectifs en succès.

	                    </p>

	                    

	                </div>

	            </div>

	        </div>

        </div>

<div class="call-to-action-container">

        	<div class="container">

	            <div class="row">

	                <div class="col-sm-12 call-to-action-text wow fadeInLeftBig">

	                    <p>

	                    	Faites un tour d’horizon de l’ensemble des solutions proposées par <span class="colored-text">Wib Day</span>.

	                    </p>

	                    <div class="call-to-action-button">

	                        <a class="big-link-3" href="http://www.wibday.com/wibday.pdf"> Plaquette commerciale</a>

	                    </div>

	                </div>

	            </div>

	        </div>

        </div>

        <!-- Meet Our Team -->

        <div class="team-container">

        	<div class="container">

	            <div class="row">

		         <!--    <div class="col-sm-12 team-title wow fadeIn">

		                <h2>Meet Our Team</h2>

		            </div>

	            </div>

	            <div class="row">	            	

	            	<div class="col-sm-3">

		                <div class="team-box wow fadeInUp">

		                    <img src="assets/img/team/1.jpg" alt="" data-at2x="assets/img/team/1.jpg">

		                    <h3>John Doe</h3>

		                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor...</p>

		                    <div class="team-social">		                        

		                        <a class="big-link-2" href="#"><span class="social_facebook"></span></a>

		                        <a class="big-link-2" href="#"><span class="social_twitter"></span></a>

		                        <a class="big-link-2" href="#"><span class="social_linkedin"></span></a>

		                        <a class="big-link-2" href="#"><span class="icon_mail"></span></a>

		                    </div>

		                </div>

	                </div>

	                <div class="col-sm-3">

		                <div class="team-box wow fadeInDown">

		                    <img src="assets/img/team/2.jpg" alt="" data-at2x="assets/img/team/2.jpg">

		                    <h3>Jane Doe</h3>

		                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor...</p>

		                    <div class="team-social">		                        

		                        <a class="big-link-2" href="#"><span class="social_facebook"></span></a>

		                        <a class="big-link-2" href="#"><span class="social_twitter"></span></a>

		                        <a class="big-link-2" href="#"><span class="social_dribbble"></span></a>

		                        <a class="big-link-2" href="#"><span class="icon_mail"></span></a>

		                    </div>

		                </div>

	                </div>

	                <div class="col-sm-3">

		                <div class="team-box wow fadeInUp">

		                    <img src="assets/img/team/3.jpg" alt="" data-at2x="assets/img/team/3.jpg">

		                    <h3>Tim Brown</h3>

		                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor...</p>

		                    <div class="team-social">		                        

		                        <a class="big-link-2" href="#"><span class="social_facebook"></span></a>

		                        <a class="big-link-2" href="#"><span class="social_twitter"></span></a>

		                        <a class="big-link-2" href="#"><span class="social_linkedin"></span></a>

		                    </div>

		                </div>

	                </div>

	               <div class="col-sm-3">

		                <div class="team-box wow fadeInDown">

		                    <img src="assets/img/team/4.jpg" alt="" data-at2x="assets/img/team/4.jpg">

		                    <h3>Sarah Red</h3>

		                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor...</p>

		                    <div class="team-social">		                        

		                        <a class="big-link-2" href="#"><span class="social_facebook"></span></a>

		                        <a class="big-link-2" href="#"><span class="social_twitter"></span></a>

		                        <a class="big-link-2" href="#"><span class="social_linkedin"></span></a>

		                    </div>

		                </div>

	                </div>-->

	            </div>

	        </div>

        </div>





<?php include("includes/footer.php") ?>


        <!-- Javascript -->

        <script src="assets/js/jquery-1.11.1.min.js"></script>

        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

        <script src="assets/js/bootstrap-hover-dropdown.min.js"></script>

        <script src="assets/js/wow.min.js"></script>

        <script src="assets/js/retina-1.1.0.min.js"></script>

        <script src="assets/js/jquery.magnific-popup.min.js"></script>

        <script src="assets/flexslider/jquery.flexslider-min.js"></script>

        <script src="assets/js/jflickrfeed.min.js"></script>

        <script src="assets/js/masonry.pkgd.min.js"></script>

        <script src="https://maps.google.com/maps/api/js?sensor=true"></script>

        <script src="assets/js/jquery.ui.map.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js" integrity="sha256-dHf/YjH1A4tewEsKUSmNnV05DDbfGN3g7NMq86xgGh8=" crossorigin="anonymous"></script>

        <script src="assets/js/scripts.js"></script>



    </body>



</html>