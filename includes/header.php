        <!-- Top menu -->
        <nav class="navbar" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Wib Day</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="top-navbar-1">
                    <ul class="nav navbar-nav navbar-right">  
                        <li <?php if($n==1){echo 'class="active"';} ?>>
                            <a href="index.php"><span aria-hidden="true"></span><br/>Accueil</a>
                        </li>
                        <li <?php if($n==2){echo 'class="active"';} ?>>                        
                            <a href="about.php"><span aria-hidden="true"></span><br/>Agence</a>
                        </li>                        
                        <li <?php if($n==3){echo 'class="active"';} ?>>
                            <a href="services.php"><span aria-hidden="true"></span><br/>Services</a>
                        </li>
                        <!-- <li>
                            <a href="portfolio.php"><span aria-hidden="true" ></span><br>Reference</a>
                        </li> -->                      

                        <li <?php if($n==4){echo 'class="active"';} ?>>                       
                            <a href="contact.php"><span aria-hidden="true" ></span><br/>Contact</a>
                        </li>
                        <li <?php if($n==5){echo 'class="active"';} ?>>                        
                            <a href="inscription-news-letter.php"><span aria-hidden="true" ></span><br/>Inscription à la newsletter</a>
                        </li> 
                    </ul>
                </div>
            </div>
        </nav>