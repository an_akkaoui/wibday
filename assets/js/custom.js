
    tinymce.init({
      selector: 'textarea.richTextBoxOverride',
      plugins: 'paste, lists',
      paste_as_text: true,
      min_height: 600,
      convert_urls: false,
      toolbar: 'styleselect bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist outdent indent'
    });
