$(function () {

    // init the validator
    // validator files are included in the download package
    // otherwise download from http://1000hz.github.io/bootstrap-validator

    $('#contact-form').validator();


    // when the form is submitted
    $('#contact-form').on('submit', function (e) {

         $('#contact-form').find('.btn').addClass("disabled");
        // if the validator does not prevent form submit
        if (!e.isDefaultPrevented()) {
            var url = "assets/contact.php";

            // POST values in the background the the script URL
            $.ajax({
                type: "POST",
                url: url,
                data: $(this).serialize(),
                success: function (data)
                {
                    $('#contact-form').find('.btn').removeClass("disabled");
                    // data = JSON object that contact.php returns
                    // we recieve the type of the message: success x danger and apply it to the 
                    var messageAlert = 'alert-' + data.type;
                    var messageText = data.message;

                    // let's compose Bootstrap alert box HTML
                    var alertBox = '<div class="alert ' + messageAlert + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + messageText + '</div>';
                    // If we have messageAlert and messageText
                    if (messageAlert && messageText) {
                        // inject the alert to .messages div in our form
                        $('#contact-form').find('.messages').html(alertBox);

                        // empty the form

                        if(data.type=="success")
                        $('#contact-form')[0].reset();
                    }
                }
            });
            return false;
        }
    })


        $('.success-message').hide();
    $('.error-message').hide();
    
    $('.footer-box-text-subscribe form').submit(function(e) {
        e.preventDefault();
        
        var form = $(this);
        var postdata = form.serialize();
        
        $.ajax({
            type: 'POST',
            url: 'assets/contact.php',
            data: postdata,
            dataType: 'json',
            success: function(json) {
                if(json.valid == 0) {
                    $('.success-message').hide();
                    $('.error-message').hide();
                    $('.error-message').html(json.message);
                    $('.error-message').fadeIn();
                }
                else {
                    $('.error-message').hide();
                    $('.success-message').hide();
                    if(json.message=="Votre demande a été envoyé")
                    form.hide();
                    $('.success-message').html(json.message);
                    $('.success-message').fadeIn();
                }
            }
        });
    });

    
});function captcha_onclick() {
            $('#recaptchaValidator').val(1);
            $('#contact-form').validator('validate');
        }