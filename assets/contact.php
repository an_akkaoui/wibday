<?php

// Email address verification
function isEmail($email) {
    return(preg_match("/^[-_.[:alnum:]]+@((([[:alnum:]]|[[:alnum:]][[:alnum:]-]*[[:alnum:]])\.)+(ad|ae|aero|af|ag|ai|al|am|an|ao|aq|ar|arpa|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|biz|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|com|coop|cr|cs|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|edu|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gh|gi|gl|gm|gn|gov|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|in|info|int|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mil|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|museum|mv|mw|mx|my|mz|na|name|nc|ne|net|nf|ng|ni|nl|no|np|nr|nt|nu|nz|om|org|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|pro|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw|me)$|(([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5])\.){3}([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5]))$/i", $email));
}

if($_POST) {

    // Enter the email where you want to receive the message
    $emailTo = 'contact@wibday.com';

    $name = isset($_POST['name'])?"Nom : ".addslashes(trim($_POST['name'])):"";
    $email = isset($_POST['email'])?addslashes(trim($_POST['email'])):"";
    $emailfrom = isset($_POST['email'])?"Email : ".addslashes(trim($_POST['email'])):"";
    $telephone = isset($_POST['telephone'])?"Téléphone : ".addslashes(trim($_POST['telephone'])):"";
    $subject = isset($_POST['subject'])?(trim($_POST['subject'])):"";
    $message = isset($_POST['message'])?(trim($_POST['message'])):"";
    $type = isset($_POST['type'])?(trim($_POST['type'])):"";

    if($type=="unsubscribe"){
    $subject = "Se désabonner";
    $message="Merci de me désabonner de votre newsletter 
    --Message automatique";
    }
   

    $responseArray = array('type' => 'danger', 'message' => "Une erreur est survenu, Merci de réessayer plus tard");

    if($name == '' && $type!="unsubscribe") {
    $responseArray = array('type' => 'danger', 'message' => 'Merci de saisir votre nom');
    }
    if(!isEmail($email)) {
    $responseArray = array('type' => 'danger', 'message' => 'Veuillez saisir un email valide');
    }
    if($message == '') {

    $responseArray = array('type' => 'danger', 'message' => 'Merci de saisir un message');
    }
    if(isEmail($email) && $message != '') {
        // Send email
	$headers = "From: " . $name . " <contactform@wibday.com>" . "\r\n" . "Reply-To: " . $email;

    $Fmessage="Nom : $name 
Email : $emailfrom
Téléphone: $telephone
Message : $message
-- 
Cet e-mail a été envoyé via le formulaire de contact de wibday.com (https://wibday.com)";

	mail($emailTo, $subject . ' (WIbDAY)', $Fmessage, $headers);
    $msg=($type="unsubscribe")?"Votre demande a été envoyé":"Email envoyé avec succée";
    $responseArray = array('type' => 'success', 'message' =>$msg);
    }

  // if requested by AJAX request return JSON response

    

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $encoded = json_encode($responseArray);

    header('Content-Type: application/json');

    echo $encoded;
}
// else just display the message
else {
    echo $responseArray['message'];
}
}

?>
