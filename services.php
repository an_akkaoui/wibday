﻿<!DOCTYPE html>

<html lang="fr">



    <head>

        

        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>WIB DAY - Agence d'email Marketing Direct</title>



        <!-- CSS -->

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,400">

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:700,400">

        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">

        <link rel="stylesheet" href="assets/elegant-font/code/style.css">

        <link rel="stylesheet" href="assets/css/animate.css">

        <link rel="stylesheet" href="assets/css/magnific-popup.css">

        <link rel="stylesheet" href="assets/flexslider/flexslider.css">

        <link rel="stylesheet" href="assets/css/form-elements.css">

        <link rel="stylesheet" href="assets/css/style.css">

        <link rel="stylesheet" href="assets/css/media-queries.css">



        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

        <!--[if lt IE 9]>

            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

        <![endif]-->



        <!-- Favicon and touch icons -->

        <link rel="shortcut icon" href="assets/ico/favicon.png">

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">

        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">

        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">

        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">



    </head>



    <body>

<?php $n=3;include("includes/header.php") ?>



        <!-- Page Title -->

        <div class="page-title-container">

            <div class="container">

                <div class="row">

                    <div class="col-sm-10 col-sm-offset-1 page-title wow fadeIn">

                        <span aria-hidden="true"></span>

                        <h1>Services /</h1>

                        <p>Nous vous proposons des solutions d’Email Marketing évolutif</p>

                    </div>

                </div>

            </div>

        </div>







        <!-- Services -->

        <div class="services-container">

            <div class="container">

             <h3>Nos prestations</h3>

                <div class="row">

                

                    <div class="col-sm-3">

                        <div class="service wow fadeInUp">

                            <div ><span aria-hidden="true"><img src="assets/img/icon/bdd.png"></span></div>

                            <h3>Location bases de données</h3>

                            <p class="prestation">Nos bases de données emails sont opt-in récents, Plus d'1 millions de contacts, segmenter par critère et par le comportemental.</p>

                        </div>

                    </div>

                    <div class="col-sm-3">

                        <div class="service wow fadeInDown">

                            <div class="service-icon"><span aria-hidden="true"><img src="assets/img/icon/collect.png"></span></div>

                            <h3>Collecte de leads</h3>

                            <p class="prestation">Vous souhaitez collecter des leads qualifiés en BtoB ou en BtoC ?  Notre approche vous garantie des prospects chauds et qualifiés selon vos critères pour un taux de conversion final optimal.</p> 

                        </div>

                    </div>

                    <div class="col-sm-3">

                        <div class="service wow fadeInUp">

                            <div class="service-icon"><span aria-hidden="true"><img src="assets/img/icon/plateforme.png"></span></div>

                            <h3>Plateforme d'envoi</h3>

                            <p class="prestation">La location de notre plateforme Marketing  vous offre tous les avantages d’un grand emailer avec une interface simple et facile, et à faible coût, pour créer vos campagnes emailing et analysez leurs performances en temps réel !</p>

                      

                        </div>

                    </div>

                    <div class="col-sm-3">

                        <div class="service wow fadeInDown">

                            <div class="service-icon"><span aria-hidden="true"><img src="assets/img/icon/20h.png"></span></div>

                            <h3>Insertions publicitaires</h3>

                            <p class="prestation">Bénéficiez d’une exposition qui touche l’ensemble de nos abonnés, grâce à l’envoi quotidien de notre newsletter. Dès l’ouverture de la newsletter votre publicité est visible à nos lecteurs.</p>

                   

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <!-- Services Full Width Text -->

        <div class="services-full-width-container">

            <div class="container">

                <div class="row">

                    <div class="col-sm-12 services-full-width-text wow fadeInLeft">

                       

                        <p>

 Notre objective est d'assurer une <span class="colored-text">bonne satisfaction client</span>,  c'est pour cela que toutes nos prestations  sont proposées  à notre clientèle résultent de notre réflexion collective. 

                        </p>

                    </div>

                </div>

            </div>

        </div>

        <!-- Services Half Width Text -->

        <div class="services-half-width-container">

            <div class="container">

                <div class="row">

                  <!--  <div class="col-sm-6 services-half-width-text wow fadeInLeft" id="ancre">

                        <h3>Lorem Ipsum</h3>

                        <p>

                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et. 

                            Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper <span class="colored-text">suscipit lobortis</span> 

                            nisl ut aliquip ex ea commodo consequat. Lorem ipsum <strong>dolor sit amet</strong>, consectetur adipisicing elit, 

                            sed do eiusmod tempor incididunt ut labore et. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper 

                            suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, 

                            sed do <strong>eiusmod tempor</strong> incididunt.

                        </p>

                    </div>-->

                 <!--   <div class="col-sm-6 services-half-width-text wow fadeInUp">

                        <h3>Dolor Sit Amet</h3>

                        <p >

                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et. 

                            Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper <span class="colored-text">suscipit lobortis</span> 

                            nisl ut aliquip ex ea commodo consequat. Lorem ipsum <strong>dolor sit amet</strong>, consectetur adipisicing elit, 

                            sed do eiusmod tempor incididunt ut labore et. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper 

                            suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, 

                            sed do <strong>eiusmod tempor</strong> incididunt.

                        </p>

                    </div>-->

                </div>

            </div>

        </div>



        <!-- Call To Action -->

        <div class="call-to-action-container">

            <div class="container">

                <div class="row">

                    <div class="col-sm-12 call-to-action-text wow fadeInLeftBig">

                        <p>

                            Faites un tour d’horizon de l’ensemble des solutions proposées par <span class="colored-text">Wib Day</span>.

                        </p>

                        <div class="call-to-action-button">

                            <a class="big-link-3" href="contact.php">

Demandez votre devis</a>

                        </div>

                    </div>

                </div>

            </div>

        </div>



<?php include("includes/footer.php") ?>



        <!-- Javascript -->

        <script src="assets/js/jquery-1.11.1.min.js"></script>

        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

        <script src="assets/js/bootstrap-hover-dropdown.min.js"></script>

        <script src="assets/js/wow.min.js"></script>

        <script src="assets/js/retina-1.1.0.min.js"></script>

        <script src="assets/js/jquery.magnific-popup.min.js"></script>

        <script src="assets/flexslider/jquery.flexslider-min.js"></script>

        <script src="assets/js/jflickrfeed.min.js"></script>

        <script src="assets/js/masonry.pkgd.min.js"></script>

        <script src="https://maps.google.com/maps/api/js?sensor=true"></script>

        <script src="assets/js/jquery.ui.map.min.js"></script>

        <script src="assets/js/scripts.js"></script>



    </body>



</html>